<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Account Dashboard</title>
  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header-postlogin.php' ?>

  <!-- main -->
  <main class="subpage usersubpage">
    <!--user container -->
    <div class="container">
         <!-- row -->
         <div class="row taskrow">
            <!-- left col -->
            <div class="col-lg-4">               
                <!-- left task -->
                <div class="left-task">
                   <h6 class="h6 small text-uppercase fbold">Draft Tasks</h6>
                    <!-- request col -->
                    <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Proof Reading</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 3 December, 2019</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker01.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2 expired-request">
                        <h2 class="d-flex justify-content-between">
                            <span>Elk Stack Designer</span>
                            <span class="fbold fred price">$ 25</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 10 Feb</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker02.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 fbold">
                        <span class="small fbold">Closed</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>I need drafting</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Sat, 15 Feb</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker03.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>I need help writing a management report</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Sun, 2 Feb</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker04.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                    <h6 class="h6 small text-uppercase fbold py-2">Completed Tasks</h6>

                     <!-- request col -->
                     <div class="request-col mb-2 expired-request">
                        <h2 class="d-flex justify-content-between">
                            <span>Need to broadcast a post to FB and Insta</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 3 December, 2019</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker05.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fred small fbold">Closed</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>I need lawn mowing services</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Thu, 6 Feb</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker06.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Deck sanded</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Sun, 1 Mar</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker07.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Remove strobing from mov files</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 3 December, 2019</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker08.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fblue small fbold">Assigned .</span>     
                            <span class="small">11 Offers</span>                      
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Graphic Designer - Logo</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 3 December, 2019</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker09.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Proof Reading</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 3 December, 2019</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker10.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fblue small fbold">Assigned .</span>     
                            <span class="small">11 Offers</span>                      
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Airtable - Hlep and databse Build</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 3 December, 2019</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker01.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Proof Reading</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 3 December, 2019</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker02.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                    <!-- div buttons -->
                    <div class="pb-4">
                        <p class="text-center pb-3">To See more taks</p>
                        <p class="text-center">
                            <a href="javascript:void(0)" class="bluebtnlg">Join Laratasker</a>
                            <span>Or</span>
                            <a href="javascript:void(0)" class="pinkbtnlg">Log in</a>
                        </p>
                    </div>
                    <!--/ div buttons -->

                </div>
                <!--/ left task -->
            </div>
            <!--/ left col -->

            
            <!-- right col -->
            <div class="col-lg-8 task-detail">
                <!-- primary details row -->
                <div class="row">
                    <!-- primarty details left col -->
                    <div class="col-lg-8">
                        <p class="status">
                            <span class="active">Open</span>
                            <span>Assigned</span>
                            <span>Completed</span>
                        </p>
                        <h1 class="h4 task-title">
                            MS WORD formatting (layout) help!
                        </h1>
                        <ul class="list-group">
                            <li class="list-group-item d-flex justify-content-between">
                                <div class="d-flex">
                                    <img src="img/data/tasker01.jpg" class="sm-thumb" alt="">
                                    <div class="pl-3 align-items-center">
                                        <span class="text-uppercase flgray small">Posted By </span>
                                        <p class="fblue ">Andrew B.</p>
                                    </div>
                                </div>
                                <span class="small d-inline-block align-self-center"> 29 mins ago </span>                            
                            </li>
                            <li class="list-group-item">
                                <div class="d-flex">
                                    <span class="icon-pin icomoon align-self-center"></span>
                                    <div class="pl-3 align-items-center">
                                        <span class="text-uppercase flgray small">LOCATION </span>
                                        <p class="">Remote</p>
                                    </div>
                                </div>                                                 
                            </li>
                            <li class="list-group-item">
                                <div class="d-flex">
                                    <span class="icon-calendar icomoon align-self-center"></span>
                                    <div class="pl-3 align-items-center">
                                        <span class="text-uppercase flgray small">Due Date </span>
                                        <p class="">Monday, 3rd Feb 2020</p>
                                    </div>
                                </div>                                                 
                            </li>
                        </ul>
                    </div>
                    <!--/ primarty details left col -->

                    <!-- primary details right col -->
                    <div class="col-lg-4 primaryrt">
                        <div class="primary-details-rt text-center">
                            <p class="text-uppercase fgray text-center">TASK BUDGET</p>
                            <p class="pt-3 text-center pb-0"><span class="small">Approx. 3hrs</span></p>
                            <h2 class="h2">$25</h2>
                            <button class="w-100">Make an Offer</button>
                        </div>
                        <div class="dropdown my-2 ">
                            <button class="btn btn-outline-dark btn-sm dropdown-toggle w-100 border" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               More Options
                            </button>
                            <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Post Similar Task</a>
                                <a class="dropdown-item" href="#">Set up Alerts</a>                                
                            </div>
                        </div>
                        <p class="text-center">Share</p>
                        <ul class="share-task nav text-center justify-content-center border">
                           
                            <li class="nav-item">    
                                <a class="nav-link" href="javascript:void(0)" target="_blank">
                                    <span class="icon-facebook-official icomoon"></span>
                                </a>
                            </li>
                            <li class="nav-item">    
                                <a class="nav-link" href="javascript:void(0)" target="_blank">
                                    <span class="icon-twitter-square icomoon"></span>
                                </a>
                            </li>
                            <li class="nav-item">    
                                <a class="nav-link" href="javascript:void(0)" target="_blank">
                                    <span class="icon-linkedin-square icomoon"></span>
                                </a>
                            </li>
                        </ul>

                        <p class="pt-3">
                            <input type="file" title="Add Attachments" class="pinkbtn w-100">
                        </p>
                       
                    </div>
                    <!-- /primary details right col -->
                    
                </div>
                <!--/ primary details row -->

                <!-- secondary row details -->
                <div class="row py-3">
                    <div class="col-lg-12">
                        <h6 class="fblue h6 text-uppercase">Details</h6>
                       
                        <!-- description task -->
                        <div class="description-task">
                        <p>I have been teaching for years and am doing my Masters Education to become a teacher librarian. I dont have much experience in libraries but have loads in teaching and in middle leadership positions in education. I will be applying to International Schools.</p>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus imperdiet, nulla et dictum interdum, nisi lorem egestas vitae scel<span id="dots">...</span><span id="more">erisque enim ligula venenatis dolor. Maecenas nisl est, ultrices nec congue eget, auctor vitae massa. Fusce luctus vestibulum augue ut aliquet. Nunc sagittis dictum nisi, sed ullamcorper ipsum dignissim ac. In at libero sed nunc venenatis imperdiet sed ornare turpis. Donec vitae dui eget tellus gravida venenatis. Integer fringilla congue eros non fermentum. Sed dapibus pulvinar nibh tempor porta.</span></p>                      

                            <a class="fpink" href="javascript:void(0)" onclick="myFunction()" id="showBtn">Read more</a>

                        </div>
                        <!-- description task -->
                          <!-- attachmnent images -->
                          <section class="gallery-block grid-gallery images-attachment">     
                              
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="h6 pb-2">My Attachments</h6>
                                    </div>
                                </div>
                               
                               <div class="row">
                                   <div class="col-md-3  col-sm-6 item">
                                       <a class="lightbox" href="img/data/tasker01.jpg">
                                           <img class="img-fluid image scale-on-hover" src="img/data/tasker01.jpg">
                                           <a class="trashicomoon" href="javascript:void(0)"><span class="icon-trash-o icomoon"></span></a>
                                       </a>
                                   </div>
                                   <div class="col-md-3  col-sm-6 item">
                                       <a class="lightbox" href="img/data/tasker02.jpg">
                                           <img class="img-fluid image scale-on-hover" src="img/data/tasker02.jpg">
                                           <a class="trashicomoon" href="javascript:void(0)"><span class="icon-trash-o icomoon"></span></a>
                                       </a>
                                   </div>
                                   <div class="col-md-3  col-sm-6 item">
                                       <a class="lightbox" href="img/data/tasker03.jpg">
                                           <img class="img-fluid image scale-on-hover" src="img/data/tasker03.jpg">
                                           <a class="trashicomoon" href="javascript:void(0)"><span class="icon-trash-o icomoon"></span></a>
                                       </a>
                                   </div>
                                   <div class="col-md-3 col-sm-6 item">
                                       <a class="lightbox" href="img/data/tasker04.jpg">
                                           <img class="img-fluid image scale-on-hover" src="img/data/tasker04.jpg">
                                           <a class="trashicomoon" href="javascript:void(0)"><span class="icon-trash-o icomoon"></span></a>
                                       </a>
                                   </div>                                   
                               </div>
                              
                           </section>
                           
                           <!--/ attachment images -->
                    </div>
                </div>
                <!--/ seconeary row details -->

                <!-- row offers -->
                <div class="row-offers row">
                    <div class="col-lg-12">
                        <h6 class="fblue h6 text-uppercase">Offers (5)</h6>

                        <!-- offer item -->
                       <div class="offer-list-item">
                           <!--tasker-->
                            <div class="d-flex py-3">
                                <img class="md-thumb" src="img/data/tasker02.jpg" alt="">
                                <article class="align-self-center pl-3">
                                <h6 class="h6 fblue">Melissa P.</h6>
                                <ul class="rating-tasker mb-0">
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star-half icomoon"></span>
                                    </li>
                                    <li class="pl-2">
                                        <span class="small">4.5 (50)</span>
                                    </li>
                                </ul>
                                <p>98% Completion rate 
                                    <a href="javascript:void(0)" data-toggle="tooltip" data-placement="right" title="The Percentage of Tasker has completed the Tasks"><span class="icon-info-circle flgray icomoon"></span></a>
                                </p>
                                </article>
                            </div>
                            <!--/ taskder -->

                            <!-- description-->
                            <article class="tasker-desc">
                                <p>I’d be happy to help you with this.</p>
                                <p>I am a degree qualified communications adviser and recruitment consultant with over 12 years of experience.</p>
                                <p>My specialties include resume writing and editing, tailored cover letters, LinkedIn profile optimisation, and ATS compliant applications.</p>
                                <p>I work with my clients to create resumes and cover letters that capture the most pertinent aspects of their experience relevant to the roles for which they’re applying.</p>
                                <p>My offer includes an initial consultation and several revisions until you’re happy. It also includes editable documents for your ongoing use.</p>
                            </article>
                            <!-- /description-->
                       </div>
                       <!-- offer item -->

                        <!-- offer item -->
                        <div class="offer-list-item">
                           <!--tasker-->
                            <div class="d-flex py-3">
                                <img class="md-thumb" src="img/data/tasker03.jpg" alt="">
                                <article class="align-self-center pl-3">
                                <h6 class="h6 fblue">Melissa P.</h6>
                                <ul class="rating-tasker mb-0">
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star-half icomoon"></span>
                                    </li>
                                    <li class="pl-2">
                                        <span class="small">4.5 (50)</span>
                                    </li>
                                </ul>
                                <p>98% Completion rate 
                                    <a href="javascript:void(0)" data-toggle="tooltip" data-placement="right" title="The Percentage of Tasker has completed the Tasks"><span class="icon-info-circle flgray icomoon"></span></a>
                                </p>
                                </article>
                            </div>
                            <!--/ taskder -->

                            <!-- description-->
                            <article class="tasker-desc">
                                <p>I am strategic , goal oriented, and always work with an end goal in mind. I have been creating unique, interactive, and beautiful resumes and cover letters for over 3 years and have worked for clients and companies around the world. Additionally, my proven interview-landing resume services will illuminate your professionalism and fast-track your application through HR and ATS processes with
outstanding credentials-based content that leaves lasting impressions.</p>
                                <p>Let me help you get that job you want; all I need is your current resume or draft. If you don't have a current resume, I can help you build a new one from scratch.</p>
                            </article>
                            <!-- /description-->
                       </div>
                       <!-- offer item -->


                        <!-- offer item -->
                        <div class="offer-list-item">
                           <!--tasker-->
                            <div class="d-flex py-3">
                                <img class="md-thumb" src="img/data/tasker05.jpg" alt="">
                                <article class="align-self-center pl-3">
                                <h6 class="h6 fblue">Melissa P.</h6>
                                <ul class="rating-tasker mb-0">
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star-half icomoon"></span>
                                    </li>
                                    <li class="pl-2">
                                        <span class="small">4.5 (50)</span>
                                    </li>
                                </ul>
                                <p>98% Completion rate 
                                    <a href="javascript:void(0)" data-toggle="tooltip" data-placement="right" title="The Percentage of Tasker has completed the Tasks"><span class="icon-info-circle flgray icomoon"></span></a>
                                </p>
                                </article>
                            </div>
                            <!--/ taskder -->

                            <!-- description-->
                            <article class="tasker-desc">
                                <p>I’d be happy to help you with this.</p>
                                <p>I am a degree qualified communications adviser and recruitment consultant with over 12 years of experience.</p>
                                <p>My specialties include resume writing and editing, tailored cover letters, LinkedIn profile optimisation, and ATS compliant applications.</p>                               
                                
                            </article>
                            <!-- /description-->
                       </div>
                       <!-- offer item -->
                    </div>                    
                </div>
                <!--/ row offers -->

                <!-- row questiions -->
                 <div class="row py-3">
                    <div class="col-lg-12">
                        <h6 class="fblue h6 text-uppercase">Questions (10)</h6>
                       <p>Please don't share personal info – insurance won't apply to tasks not done through Airtasker!</p>

                       <!-- div buttons -->
                        <div class="pb-4">
                            <p class="text-center pb-3 h5">To join the conversation</p>
                            <p class="text-center">
                                <a href="javascript:void(0)" class="bluebtnlg">Join Laratasker</a>
                                <span>Or</span>
                                <a href="javascript:void(0)" class="pinkbtnlg">Log in</a>
                            </p>
                        </div>
                        <!--/ div buttons -->

                        <!-- questioin -->
                        <div class="question">
                            <div class="d-flex">
                                <img src="img/data/tasker01.jpg" class="sm-thumb" alt="">
                                <div class="pl-3 align-items-center">                               
                                    <a href="javascript:void(0)" class="fblue h6">Andrew B.</a>
                                    <p>Hi Please see my offer and assign me for this task. I will create you a resume and a cover letter that will highlight your skills, experience, and education. So feel free to assign me.</p>

                                    <p>
                                        <span class="small fgray">4 hours ago</span>
                                        <span class="small fblue d-inline-block pl-3">
                                        <a href="javascript:void(0)" class="fblue">
                                            <span class="icon-mail-reply icomoon"></span> Reply
                                        </a>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--/ question -->

                        <!-- questioin -->
                        <div class="question">
                            <div class="d-flex">
                                <img src="img/data/tasker02.jpg" class="sm-thumb" alt="">
                                <div class="pl-3 align-items-center">                               
                                    <a href="javascript:void(0)" class="fblue h6">Faith T.</a>
                                    <p>Hi Please see my offer and assign me for this task. I will create you a resume and a cover letter that will highlight your skills, experience, and education. So feel free to assign me.</p>

                                    <p>
                                        <span class="small fgray">4 hours ago</span>
                                        <span class="small fblue d-inline-block pl-3">
                                        <a href="javascript:void(0)" class="fblue">
                                            <span class="icon-mail-reply icomoon"></span> Reply
                                        </a>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--/ question -->

                          <!-- questioin -->
                          <div class="question">
                            <div class="d-flex">
                                <img src="img/data/tasker04.jpg" class="sm-thumb" alt="">
                                <div class="pl-3 align-items-center">                               
                                    <a href="javascript:void(0)" class="fblue h6">Faith T.</a>
                                    <p>Hi Please see my offer and assign me for this task. I will create you a resume and a cover letter that will highlight your skills, experience, and education. So feel free to assign me.</p>

                                    <p>
                                        <span class="small fgray">4 hours ago</span>
                                        <span class="small fblue d-inline-block pl-3">
                                        <a href="javascript:void(0)" class="fblue">
                                            <span class="icon-mail-reply icomoon"></span> Reply
                                        </a>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--/ question -->

                    </div>
                </div> 
                <!--/ row questions -->
            </div>
            <!--/ right col -->
        </div>
        <!--/ row -->
    </div>
    <!--/ user container -->
  </main>
  <!--/ main -->

  <?php include 'scripts.php' ?> 
</body>
</html>