<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Notifications Settings</title>
  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header-postlogin.php' ?>

  <!-- main -->
  <main class="subpage usersubpage">
    <!--user container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- left navigation -->
            <div class="col-lg-3 leftnavigation">
              <?php include 'user-leftnav.php' ?>
            </div>
            <!--/ left navigatin -->

            <!-- right profile -->
            <div class="col-lg-9">
                <!-- right user panel-->
                <div class="right-user-panel">
                    <h1 class="h5 title-page">Notifications Settings</h1>

                    <p>Your notifications can be updated at any time via the options below or the Airtasker App.</p>

                    <p>
                      <span class="fbold">Email:</span> praveennandipati@gmail.com <a class="fblue " href="javascript:void(0)" data-toggle="modal" data-target="#edit-mail">Edit</a>
                    </p>

                    <p>
                      <span class="fbold">Mobile:</span> +91 9642123254 <a class="fblue " href="javascript:void(0)" data-toggle="modal" data-target="#edit-phone">Edit</a>
                    </p>

                    <!-- gray box -->
                    <div class="graybox">
                        <!-- row -->
                        <div class="row justify-content-between">
                            <!-- col -->
                            <div class="col-lg-7">
                                <h6 class="h6 text-uppercase fnormal fblue ">TRANSACTIONAL</h6>
                                <p class="text-left pb-0">You will always receive important notifications about any payments, cancellations and your account.</p>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-lg-5 align-self-center text-lg-right">

                             <form class="custom-form">
                                 <label class="container-form mx-2 d-inline-block">Email
                                      <input type="checkbox">
                                      <span class="checkmark"></span>
                                  </label>
                                  <label class="container-form mx-2  d-inline-block">SMS
                                      <input type="checkbox" checked="checked">
                                      <span class="checkmark"></span>
                                  </label>       
                                  <label class="container-form mx-2  d-inline-block">Push
                                      <input type="checkbox" checked="">
                                      <span class="checkmark"></span>
                                  </label>    
                                  
                              </form>

                             </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ bray box -->

                      <!-- gray box -->
                      <div class="graybox">
                        <!-- row -->
                        <div class="row justify-content-between">
                            <!-- col -->
                            <div class="col-lg-7">
                                <h6 class="h6 text-uppercase fnormal fblue ">TASK UPDATES</h6>
                                <p class="text-left pb-0">Receive updates on any new comments, private messages, offers and reviews.</p>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-lg-5 align-self-center text-lg-right">

                             <form class="custom-form">
                                 <label class="container-form mx-2 d-inline-block">Email
                                      <input type="checkbox">
                                      <span class="checkmark"></span>
                                  </label>
                                  <label class="container-form mx-2  d-inline-block">SMS
                                      <input type="checkbox" checked="checked">
                                      <span class="checkmark"></span>
                                  </label>       
                                  <label class="container-form mx-2  d-inline-block">Push
                                      <input type="checkbox" checked="">
                                      <span class="checkmark"></span>
                                  </label>    
                                  
                              </form>

                             </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ bray box -->

                     <!-- gray box -->
                     <div class="graybox">
                        <!-- row -->
                        <div class="row justify-content-between">
                            <!-- col -->
                            <div class="col-lg-7">
                                <h6 class="h6 text-uppercase fnormal fblue ">TASK REMINDERS</h6>
                                <p class="text-left pb-0">Friendly reminders if you’ve forgotten to accept an offer, release a payment or leave a review.</p>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-lg-5 align-self-center text-lg-right">

                             <form class="custom-form">
                                 <label class="container-form mx-2 d-inline-block">Email
                                      <input type="checkbox">
                                      <span class="checkmark"></span>
                                  </label>
                                  <label class="container-form mx-2  d-inline-block">SMS
                                      <input type="checkbox" checked="checked">
                                      <span class="checkmark"></span>
                                  </label>       
                                  <label class="container-form mx-2  d-inline-block">Push
                                      <input type="checkbox" checked="">
                                      <span class="checkmark"></span>
                                  </label>    
                                  
                              </form>

                             </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ bray box -->

                    
                     <!-- gray box -->
                     <div class="graybox">
                        <!-- row -->
                        <div class="row justify-content-between">
                            <!-- col -->
                            <div class="col-lg-7">
                                <h6 class="h6 text-uppercase fnormal fblue ">AIRTASKER ALERTS</h6>
                                <p class="text-left pb-0">Once you’ve set up your Airtasker Alerts, you’ll be instantly notified when a task is posted that matches your requirements.</p>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-lg-5 align-self-center text-lg-right">

                             <form class="custom-form">
                                 <label class="container-form mx-2 d-inline-block">Email
                                      <input type="checkbox">
                                      <span class="checkmark"></span>
                                  </label>                                
                                  <label class="container-form mx-2  d-inline-block">Push
                                      <input type="checkbox" checked="">
                                      <span class="checkmark"></span>
                                  </label>    
                                  
                              </form>

                             </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ bray box -->

                     <!-- gray box -->
                     <div class="graybox">
                        <!-- row -->
                        <div class="row justify-content-between">
                            <!-- col -->
                            <div class="col-lg-7">
                                <h6 class="h6 text-uppercase fnormal fblue ">TASK RECOMMENDATIONS</h6>
                                <p class="text-left pb-0">Receive recommendations and be inspired by tasks close to you.</p>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-lg-5 align-self-center text-lg-right">

                             <form class="custom-form">
                                 <label class="container-form mx-2 d-inline-block">Email
                                      <input type="checkbox">
                                      <span class="checkmark"></span>
                                  </label>
                                  <label class="container-form mx-2  d-inline-block">SMS
                                      <input type="checkbox" checked="checked">
                                      <span class="checkmark"></span>
                                  </label>       
                                  <label class="container-form mx-2  d-inline-block">Push
                                      <input type="checkbox" checked="">
                                      <span class="checkmark"></span>
                                  </label>    
                                  
                              </form>

                             </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ bray box -->

                         <!-- gray box -->
                         <div class="graybox">
                        <!-- row -->
                        <div class="row justify-content-between">
                            <!-- col -->
                            <div class="col-lg-7">
                                <h6 class="h6 text-uppercase fnormal fblue ">HELPFUL INFORMATION</h6>
                                <p class="text-left pb-0">Learn about how to earn more and find the right people for your tasks with helpful tips and advice.</p>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-lg-5 align-self-center text-lg-right">

                             <form class="custom-form">
                                 <label class="container-form mx-2 d-inline-block">Email
                                      <input type="checkbox">
                                      <span class="checkmark"></span>
                                  </label>
                                  <label class="container-form mx-2  d-inline-block">SMS
                                      <input type="checkbox" checked="checked">
                                      <span class="checkmark"></span>
                                  </label>       
                                  <label class="container-form mx-2  d-inline-block">Push
                                      <input type="checkbox" checked="">
                                      <span class="checkmark"></span>
                                  </label>    
                                  
                              </form>

                             </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ bray box -->

                       <!-- gray box -->
                       <div class="graybox">
                        <!-- row -->
                        <div class="row justify-content-between">
                            <!-- col -->
                            <div class="col-lg-7">
                                <h6 class="h6 text-uppercase fnormal fblue ">UPDATES & NEWSLETTERS</h6>
                                <p class="text-left pb-0">Be the first to hear about new features and exciting updates on Airtasker.</p>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-lg-5 align-self-center text-lg-right">

                             <form class="custom-form">
                                 <label class="container-form mx-2 d-inline-block">Email
                                      <input type="checkbox">
                                      <span class="checkmark"></span>
                                  </label>
                                  <label class="container-form mx-2  d-inline-block">SMS
                                      <input type="checkbox" checked="checked">
                                      <span class="checkmark"></span>
                                  </label>       
                                  <label class="container-form mx-2  d-inline-block">Push
                                      <input type="checkbox" checked="">
                                      <span class="checkmark"></span>
                                  </label>    
                                  
                              </form>

                             </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ bray box -->




                  
      
                     
                </div>
                <!--/ right user panel -->
            </div>
            <!--/ right profile -->
        </div>
        <!--/ row -->
    </div>
    <!--/ user container -->
  </main>
  <!--/ main -->

  <!-- Chagne Mail -->

<div class="modal fade" id="edit-mail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-dialog-centered modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Change / Edit Mail </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- body-->
      <div class="modal-body">
        <p>Change your email address to keep in touch with Taskers and posters, get notifications on tasks and alerts that you've set up</p>

        <form class="custom-form py-3">          

            <div class="form-group">
                <label>Enter your email address</label>
                <div class="input-group">
                    <input type="text" value="praveennandipati@gmail.com" placeholder="Enter your Email" class="form-control">
                </div>
            </div>           
        </form>

        <p>You can choose what types of emails you receive from us. We may still contact you about your account if it's urgent.</p>
      </div>
      <!--/ body -->
      <div class="modal-footer text-center">        
        <button type="button" class="bluebtnlg">Save</button>
      </div>
    </div>
  </div>
</div>
  <!--/ Chagne Mail -->

   <!-- Chagne Phone -->

<div class="modal fade" id="edit-phone" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-dialog-centered modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Verify Mobile for free SMS alerts </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- body-->
      <div class="modal-body">
        <p>We noticed you want to receive SMS alerts, but you don't have a verified mobile listed on your account. Please add one to enable free alerts.</p>

        <form class="custom-form py-3">          

            <div class="form-group">
                <label>Mobile number</label>
                <div class="input-group">
                    <input type="text" value="+91 9642123254" placeholder="Phone Number" class="form-control">
                </div>
            </div>           
        </form>
        <p>We will send you a verification code</p>
      </div>
      <!--/ body -->
      <div class="modal-footer text-center">        
        <button type="button" class="bluebtnlg">Send</button>
      </div>
    </div>
  </div>
</div>
  <!--/ Chagne Phone -->

  <?php include 'scripts.php' ?> 
</body>
</html>