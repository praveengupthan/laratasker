<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Help</title>

  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header.php' ?>

  <!-- main -->
  <main class="subpage">

    <!-- help header -->
    <div class="help-header">

    <!-- contatiner -->
    <div class="container">
        <!--  row -->
        <div class="row text-center justify-content-center">
            <!-- col -->
            <div class="col-lg-6">
                <div class="search-section d-flex">
                    <span class="icon-search icomoon"></span>
                    <input type="text" placeholder="Search">
                </div>
            </div>
            <!--/ col -->
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
        
    </div>
    <!--/ heelp header -->

    <!-- help body -->
    <div class="helpbody py-3">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-4">
                    <a href="javascript:void(0)" class="faqlink">Understanding Laratasker</a>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <a href="javascript:void(0)" class="faqlink">Account & Profile</a>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <a href="javascript:void(0)" class="faqlink">Post A Task</a>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <a href="javascript:void(0)" class="faqlink">Make An Offer</a>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <a href="javascript:void(0)" class="faqlink">Community Guidelines</a>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <a href="javascript:void(0)" class="faqlink">Payments</a>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <a href="javascript:void(0)" class="faqlink">Changes & Cancellations</a>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <a href="javascript:void(0)" class="faqlink">Trust, Safety & Disputes</a>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <a href="javascript:void(0)" class="faqlink">Communication</a>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <a href="javascript:void(0)" class="faqlink">Ratings & Reviews</a>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <a href="javascript:void(0)" class="faqlink">Insurance & Tax</a>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <a href="javascript:void(0)" class="faqlink">Bookings, Subscriptions & Instant Claim</a>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <a href="javascript:void(0)" class="faqlink">Helpful Tips</a>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <a href="javascript:void(0)" class="faqlink">Contact Us</a>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <a href="javascript:void(0)" class="faqlink">Understanding Laratasker</a>
                </div>
                <!--/ col -->

                
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ help body -->



  <!-- container -->
  <div class="container">
   </div>
   <!--/ container -->

  
    
  </main>
  <!--/ main -->
  <?php include 'footer.php' ?>
  <?php include 'scripts.php' ?> 

</body>
</html>