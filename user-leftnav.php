  <!-- user left nav -->
  <div class="user-leftnav sticky-top">
        <!-- profile image-->
        <div class="profile-pic">
            <figure>
                <a href="javascript:void(0)">
                    <img src="img/data/tasker02.jpg" class="md-thumb">
                    <span class="icon-edit icomoon"></span>
                </a>
            </figure>
            <a href="javascript:void(0)">Praveen Guptha</a>
        </div>
        <!--/ profile iagme -->  

        <!--nav -->
        <div class="profile-nav">
            <ul class="nav flex-column flex-nowrap overflow-hidden">
                <li class="nav-item">
                    <a class="nav-link" href="account-dashboard.php"> Dashboard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="account-payment-history.php"> Payment History</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="account-payment-methods.php"> Payment Methods</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="account-notifications.php"> Notifications</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="account-referafriend.php"> Refer a Friend</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link collapsed text-truncate" href="#submenu1" data-toggle="collapse" data-target="#submenu1"><i class="fa fa-table"></i> <span class="d-none d-sm-inline">Settings</span></a>
                    <div class="collapse" id="submenu1" aria-expanded="false">
                        <ul class="flex-column pl-2 nav">
                            <li class="nav-item"><a class="nav-link" href="account-settings-profile.php">Account</a></li>
                            <li class="nav-item"><a class="nav-link" href="account-settings-skills.php">Skills</a></li>
                            <li class="nav-item"><a class="nav-link" href="account-settings-badges.php">Badges</a></li>
                            <li class="nav-item"><a class="nav-link" href="account-settings-task-alerts.php">Task Alerts</a></li>
                            <li class="nav-item"><a class="nav-link" href="account-settings-notification-settings.php">Notification Settings</a></li>                            
                            <li class="nav-item"><a class="nav-link" href="account-settings-portfolio.php">Portfolio</a></li>                           
                        </ul>
                    </div>
                </li>                            
            </ul>
        </div>
        <!--/ nav -->
    </div>
    <!--/ user left nav -->