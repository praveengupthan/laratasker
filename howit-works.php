<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Lara Tasker Tasks List</title>

  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
  <?php include 'header.php' ?>

  <!-- main -->
  <main class="subpage">
      <!-- how it works section 1-->
      <div class="howit-primary">
          <!-- container -->
          <div class="container">
              <!-- row -->
              <div class="row justify-content-center">
                  <div class="col-lg-8 text-center">
                      <h1 class="h3">The best place for people and businesses to outsource tasks</h1>
                  </div>
              </div>
              <!--/ row -->

               <!-- row -->
               <div class="row howitrow">
                 <!-- col -->
                 <div class="col-lg-4 text-center">
                     <img src="img/phase-1.webp" alt="" class="img-fluid">
                     <article class="pt-3 text-center">
                        <h2 class="h5">What do you need done?</h2>
                        <p class="text-center">Start by telling us about your task. Mention when and where (in person or online) you need it done, then suggest a fair budget for the task. Post any task you need from cleaning to web design in only two minutes – for free! There's no obligation to hire.</p>
                     </article>
                 </div>
                 <!--/ col -->
                  <!-- col -->
                  <div class="col-lg-4 text-center">
                     <img src="img/phase-2-4.png" alt="" class="img-fluid">
                     <article class="pt-3 text-center">
                        <h2 class="h5">Choose the best person for you</h2>
                        <p class="text-center">Take a look at profiles and reviews to pick the best Tasker for your task. When you accept an offer, your payment is held securely with Airtasker Pay until the task is complete. Now you can message and call the Tasker to sort out the details.</p>
                     </article>
                 </div>
                 <!--/ col -->
                  <!-- col -->
                  <div class="col-lg-4 text-center">
                     <img src="img/phase-3-1.webp" alt="" class="img-fluid">
                     <article class="pt-3 text-center">
                        <h2 class="h5">Task completed</h2>
                        <p class="text-center">With your task complete, you just need to release the payment held with Airtasker Pay. Then you’re free to leave a review for the Tasker so everyone can know what a great job they’ve done!</p>
                     </article>
                 </div>
                 <!--/ col -->
              </div>
              <!--/ row -->
          </div>
          <!-- /container -->
      </div>
      <!-- /how it works section 1-->

      <!-- post yur task -->
      <div class="container posturtask-blocks">

       <!-- row -->
       <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <h1 class="h3">Post your task</h1>
                <p class="text-center fgray">Want your home cleaned or furniture put together? Just tell us about the task you’d like done, suggest a fair budget for a job well done and you’ll start to receive offers from available Taskers.</p>
            </div>
        </div>
        <!--/ row -->

        <!-- row -->
        <div class="row taskers-category-row">
            <!-- col -3 -->
            <div class="col-lg-3">
                <div class="tasker-cat-col">
                   <a href="javascript:void(0)">
                        <img src="img/data/home-cooking.jpg" alt="" class="img-fluid">
                        <span class="title-cat">Cooking</span>
                   </a>
                </div>
            </div>
            <!--/ col -3 -->
            <!-- col 6-->
            <div class="col-lg-6">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6">
                        <div class="tasker-cat-col">
                            <a href="javascript:void(0)">
                                <img src="img/data/it-comp.jpg" alt="" class="img-fluid">
                                <span class="title-cat">Computer and IT</span>
                            </a>
                        </div>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-lg-6">
                        <div class="tasker-cat-col">
                            <a href="javascript:void(0)">
                                <img src="img/data/event.jpg" alt="" class="img-fluid">
                                <span class="title-cat">Photography</span>
                            </a>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!-- /row -->

                <!-- row -->
                <div class="row mt-4">
                    <!-- col -->
                    <div class="col-lg-12">
                        <div class="tasker-cat-col">
                            <a href="javascript:void(0)">
                                <img src="img/data/fun-quirky.jpg" alt="" class="img-fluid">
                                <span class="title-cat">Handy Man</span>
                            </a>
                        </div>
                    </div>
                    <!--/ col -->                    
                </div>
                <!-- /row -->

            </div>
            <!-- /col 6-->

              <!-- col -3 -->
              <div class="col-lg-3">
                <div class="tasker-cat-col">
                   <a href="javascript:void(0)">
                        <img src="img/data/delivery-removal.jpg" alt="" class="img-fluid">
                        <span class="title-cat">Removals</span>
                   </a>
                </div>
            </div>
            <!--/ col -3 -->

            <!-- col -3 -->
            <div class="col-lg-3 mt-4">
                <div class="tasker-cat-col">
                   <a href="javascript:void(0)">
                        <img src="img/data/marketing.jpg" alt="" class="img-fluid">
                        <span class="title-cat">Design</span>
                   </a>
                </div>
            </div>
            <!--/ col -3 -->

            <!-- col -3 -->
            <div class="col-lg-3 mt-4">
                <div class="tasker-cat-col">
                   <a href="javascript:void(0)">
                        <img src="img/data/business.jpg" alt="" class="img-fluid">
                        <span class="title-cat">Business</span>
                   </a>
                </div>
            </div>
            <!--/ col -3 -->

             <!-- col -3 -->
             <div class="col-lg-6 mt-4">
                <div class="tasker-cat-col">
                   <a href="javascript:void(0)">
                        <img src="img/data/handyman.jpg" alt="" class="img-fluid">
                        <span class="title-cat">Assembly</span>
                   </a>
                </div>
            </div>
            <!--/ col -3 -->
        </div>
        <!--/ row -->       

      </div>
      <!-- /post yur task -->

      <!-- features -->
      <div class="features-lara">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row howitrow">
                    <!-- COL -->
                    <div class="col-lg-4 text-center col-how">
                        <span class="icon-bookmark-o icomoon"></span>
                        <h4 class="h4 py-2">Top rated insurance</h4>
                        <p class="text-center">Airtasker Insurance is provided by CGU. This means Taskers on Airtasker are covered for liability to third parties when it comes to personal injury or property damage (terms and conditions apply) - so you can post or earn with peace of mind!*</p>
                    </div>
                    <!--/ col -->
                     <!-- COL -->
                     <div class="col-lg-4 text-center col-how">
                            <figure class="roundimg">
                                <img src="img/saver_beach.webp" alt="" class="img-fluid">
                            </figure>
                            <p class="small text-center pb-3">*Terms and Conditions apply. Included Task activities only. Excesses apply for
Taskers. Learn more about Airtasker Insurance</p>

                            <a href="javascript:void(0)" class="pinkbtnlg">Read More</a>
                    </div>
                    <!--/ col -->
                     <!-- COL -->
                     <div class="col-lg-4 text-center col-how">
                        <span class="icon-comments-o icomoon"></span>
                        <h4 class="h4 py-2">Complete customer support</h4>
                        <p class="text-center">Got a question? Simply search our comprehensiveHelp Centrefor your answer. If you’re still stuck then feel free to reach out to our expert Customer Support Team who are more than happy to help.</p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6">
                        <img src="img/on-boarding.webp" class="img-fluid">
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center">
                        <h4 class="h4 fwhite">Rating & Reviews</h4>
                        <p class="fwhite">eview Tasker's portfolios, skills, verifications on their profile, and see their transaction verified ratings & reviews on tasks they’ve previously completed on Airtasker. This empowers you to make sure you’re choosing the right person for your task.</p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                 <!-- row -->
                 <div class="row">
                    <!-- col -->
                    <div class="col-lg-6 order-lg-last">
                        <img src="img/chatting-conversation-pngrepo-com.png">
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center">
                        <h4 class="h4 fwhite">Communication</h4>
                        <p class="fwhite">Use Airtasker to stay in contact from the moment your task is posted until it’s completed.</p>
                        <p class="fwhite">Once you’ve accepted an offer, you can instantly reach out to the Airtasker via private messaging to discuss task details, and get your task completed.</p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
      </div>
      <!--/ features -->

      <!-- last features -->
      <div class="last-features">
          <!-- container -->
          <div class="container">
              <!-- row -->
              <div class="row">
                  <!-- col -->
                  <div class="col-lg-4 text-center">
                      <img src="img/worker-2016.webp" alt="">
                  </div>
                  <!--/ col -->
                  <!-- col -->
                  <div class="col-lg-8 align-self-center">
                      <h4 class="h4 pb-3">Earn up to $5,000 per month completing tasks</h4>

                      <div class="row py-3 border-bottom">
                          <div class="col-lg-1">
                            <span class="icon-black-tie icomoon"></span>
                          </div>
                          <div class="col-lg-11">
                              <h6>You're the boss</h6>
                              <p>With thousands of tasks posted every month on Airtasker there are lots of opportunities to earn. Choose the tasks you’d like to complete for people that you're happy to work with.</p>
                          </div>
                      </div>

                      <div class="row py-3 border-bottom">
                          <div class="col-lg-1">
                            <span class="icon-thumbs-o-up icomoon"></span>
                          </div>
                          <div class="col-lg-11">
                              <h6>Payments</h6>
                              <p>With your task payment held secure with Airtasker Pay, you're able to complete the task knowing payment will be made when you're done.</p>
                          </div>
                      </div>

                      <div class="row py-3 border-bottom mb-3">
                          <div class="col-lg-1">
                            <span class="icon-tag icomoon"></span>
                          </div>
                          <div class="col-lg-11">
                              <h6>Top rated insurance</h6>
                              <p>Airtasker Insurance is provided by CGU. This means Taskers on Airtasker are covered for liability to third parties when it comes to personal injury or property damage (terms and conditions apply) - so you can post or earn with peace of mind!*</p>
                          </div>
                      </div>

                      <div class="row py-3">
                          <div class="col-lg-12">
                            <!-- div buttons -->
                            <div class="pb-4">                       
                                <p class="text-center">
                                    <a href="javascript:void(0)" class="bluebtnlg">Join Laratasker</a>
                                    <span>Or</span>
                                    <a href="javascript:void(0)" class="pinkbtnlg">Start Earning</a>
                                </p>
                            </div>
                            <!--/ div buttons -->
                          </div>
                         
                      </div>




                     


                  </div>
                  <!--/ col -->
              </div>
              <!--/ row -->
          </div>
          <!--/ container -->
      </div>
      <!--/ last features -->
      

    
  </main>
  <!--/ main -->
  <?php include 'footer.php' ?>
  <?php include 'scripts.php' ?> 

</body>
</html>