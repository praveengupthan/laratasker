<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Account Dashboard</title>
  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header-postlogin.php' ?>

  <!-- main -->
  <main class="subpage usersubpage">
    <!--user container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- left navigation -->
            <div class="col-lg-3 leftnavigation">
              <?php include 'user-leftnav.php' ?>
            </div>
            <!--/ left navigatin -->

            <!-- right profile -->
            <div class="col-lg-9">
                <!-- right user panel-->
                <div class="right-user-panel">
                    <h1 class="h5 title-page">Dashboard</h1>

                    <h2 class="h5">Get it done today</h2>
                    <p>To-do list never getting shorter? Take the burden off and find the help you need on Laratasker.</p>

                    <div class="categories-in d-flex justify-content-lg-center">

                        <!-- col -->
                        <a class="cat-col-home" href="javascript:void(0)">
                            <span class="icon-bookmark icomoon"></span>
                            <span class="text">Home cleaning</span>
                        </a>
                        <!--/ col -->

                        <!-- col -->
                        <a class="cat-col-home" href="javascript:void(0)">
                            <span class="icon-taxi icomoon"></span>
                            <span class="text">Full House Removals</span>
                        </a>
                        <!--/ col -->
                        <!-- col -->
                        <a class="cat-col-home" href="javascript:void(0)">
                            <span class="icon-tag icomoon"></span>
                            <span class="text">Few Items Removals</span>
                        </a>
                        <!--/ col -->

                         <!-- col -->
                         <a class="cat-col-home" href="javascript:void(0)">
                            <span class="icon-tag icomoon"></span>
                            <span class="text">Business and Admin</span>
                        </a>
                        <!--/ col -->

                         <!-- col -->
                         <a class="cat-col-home" href="javascript:void(0)">
                            <span class="icon-tag icomoon"></span>
                            <span class="text">Computers &amp; IT</span>
                        </a>
                        <!--/ col -->

                        <!-- col -->
                        <a class="cat-col-home" href="javascript:void(0)">
                            <span class="icon-tow-truck icomoon"></span>
                            <span class="text">Furniture Assembly</span>
                        </a>
                        <!--/ col -->

                        <!-- col -->
                        <a class="cat-col-home" href="javascript:void(0)">
                            <span class="icon-tag icomoon"></span>
                            <span class="text">Handy Man</span>
                        </a>
                        <!--/ col -->

                        <!-- col -->
                        <a class="cat-col-home" href="javascript:void(0)">
                            <span class="icon-telephone icomoon"></span>
                            <span class="text">Marketing &amp; Design</span>
                        </a>
                        <!--/ col -->

                          <!-- col -->
                          <a class="cat-col-home" href="javascript:void(0)">
                            <span class="icon-tyre icomoon"></span>
                            <span class="text">Events  &amp; Photography</span>
                        </a>
                        <!--/ col -->

                         <!-- col -->
                         <a class="cat-col-home" href="javascript:void(0)">
                            <span class="icon-tyre icomoon"></span>
                            <span class="text">Fun  &amp; Quircky</span>
                        </a>
                        <!--/ col -->

                        <!-- col -->
                        <a class="cat-col-home" href="javascript:void(0)">
                            <span class="icon-tyre icomoon"></span>
                            <span class="text">Home &amp; Gardening</span>
                        </a>
                        <!--/ col -->

                        <!-- col -->
                        <a class="cat-col-home" href="javascript:void(0)">
                            <span class="icon-search icomoon"></span>
                            <span class="text">Anything</span>
                        </a>
                        <!--/ col -->
                    </div>
                    <div class="text-center py-3">
                        <a href="javascript:void(0)" class="pinkbtnlg">Post a Task</a>
                    </div>

                    <!-- announcemtns -->
                    <div class="py-3">
                        <h5 class="h6 pb-2">Laratasker Announcements</h5>
                        <ul class="list-group custom-list-group">
                            <li class="list-group-item">Lower fees for bigger tasks — that’s right! <a href="javascript:void(0)" class="fblue">Find out More</a></li>
                            <li class="list-group-item">Lower fees Increase the task price <a href="javascript:void(0)" class="fblue">Find out More</a></li>
                            <li class="list-group-item">Just launched: Gasfitting & Plumbing Badges <a href="javascript:void(0)" class="fblue">Add it to your profile</a></li>
                            <li class="list-group-item">Just launched: Working with Children Badge -  <a href="javascript:void(0)" class="fblue"> Add it to your profile</a></li>
                            <li class="list-group-item">Lower fees for bigger tasks — that’s right! <a href="javascript:void(0)" class="fblue">Find out More</a></li>
                        </ul>
                    </div>
                    <!--/ announcements -->

                    <!-- tab my tasks summary  -->
                    <div class="custom-tab">
                        <h5 class="h6 py-4 text-center">My Tasks Summary</h5>
                        <!-- tab -->
                        <ul class="nav justify-content-center nav-pills" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="worker-tab" data-toggle="tab" href="#worker" role="tab" aria-controls="home" aria-selected="true">As Worker</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="poster-tab" data-toggle="tab" href="#poster" role="tab" aria-controls="profile" aria-selected="false">As Poster</a>
                            </li>
                           
                        </ul>
                        <div class="tab-content pt-3" id="myTabContent">
                            <div class="tab-pane fade show active" id="worker" role="tabpanel" aria-labelledby="worker-tab">
                           
                            <!-- row -->
                            <div class="row mx-0 justify-content-center pt-3">
                                <!-- col -->
                                <div class="col-lg-2 text-center">
                                    <div class="summary-div">
                                      <a href="javascripty:void(0)">
                                            <h4 class="h4 pt-3">0</h4>
                                            <p class="text-center">Bid on</p>
                                      </a>
                                    </div>
                                </div>
                                <!--/ col -->
                                 <!-- col -->
                                 <div class="col-lg-2 text-center">
                                    <div class="summary-div">
                                      <a href="javascripty:void(0)">
                                            <h4 class="h4 pt-3">0</h4>
                                            <p class="text-center">Assigned</p>
                                      </a>
                                    </div>
                                </div>
                                <!--/ col -->
                                 <!-- col -->
                                 <div class="col-lg-2 text-center">
                                    <div class="summary-div">
                                      <a href="javascripty:void(0)">
                                            <h4 class="h4 pt-3">0</h4>
                                            <p class="text-center">Awaiting Payment</p>
                                      </a>
                                    </div>
                                </div>
                                <!--/ col -->
                                 <!-- col -->
                                 <div class="col-lg-2 text-center">
                                    <div class="summary-div">
                                      <a href="javascripty:void(0)">
                                            <h4 class="h4 pt-3">0</h4>
                                            <p class="text-center">Completed</p>
                                      </a>
                                    </div>
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ row -->

                            </div>
                             <div class="tab-pane fade" id="poster" role="tabpanel" aria-labelledby="poster-tab">
                             <!-- row -->
                             <div class="row mx-0 justify-content-center pt-3">
                                <!-- col -->
                                <div class="col-lg-2 text-center">
                                    <div class="summary-div">
                                      <a href="javascripty:void(0)">
                                            <h4 class="h4 pt-3">0</h4>
                                            <p class="text-center">Open for Offers</p>
                                      </a>
                                    </div>
                                </div>
                                <!--/ col -->
                                 <!-- col -->
                                 <div class="col-lg-2 text-center">
                                    <div class="summary-div">
                                      <a href="javascripty:void(0)">
                                            <h4 class="h4 pt-3">0</h4>
                                            <p class="text-center">Assigned</p>
                                      </a>
                                    </div>
                                </div>
                                <!--/ col -->
                                 <!-- col -->
                                 <div class="col-lg-2 text-center">
                                    <div class="summary-div">
                                      <a href="javascripty:void(0)">
                                            <h4 class="h4 pt-3">0</h4>
                                            <p class="text-center">Awaiting Payment</p>
                                      </a>
                                    </div>
                                </div>
                                <!--/ col -->
                                 <!-- col -->
                                 <div class="col-lg-2 text-center">
                                    <div class="summary-div">
                                      <a href="javascripty:void(0)">
                                            <h4 class="h4 pt-3">0</h4>
                                            <p class="text-center">Completed</p>
                                      </a>
                                    </div>
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ row -->                                 
                             </div>                           
                        </div>
                        <!--/ tab -->
                    </div>
                    <!--/ tab my tasks summary  -->
                    <!-- profile status -->
                    <div class="row justify-content-center py-5">
                        <div class="col-lg-12 text-center">
                            <h5 class="h6 pb-2">Your profile is 23% complete</h5>
                        </div>
                        <!-- col -->
                        <div class="col-lg-2 text-center">
                            <a href="javascript:void(0)" class="d-block border py-3">
                                <span class="icon-check icomoon fgreen h2"></span>
                                <span claSS="d-block fblack">Account</span>
                            </a>
                        </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-lg-2 text-center">
                            <a href="javascript:void(0)" class="d-block border py-3">
                                <span class="icon-exclamation-circle icomoon fgray h2"></span>
                                <span claSS="d-block fblack">Profile</span>
                            </a>
                        </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-lg-2 text-center">
                            <a href="javascript:void(0)" class="d-block border py-3">
                                <span class="icon-exclamation-circle icomoon fgray h2"></span>
                                <span claSS="d-block fblack">Skills</span>
                            </a>
                        </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-lg-2 text-center">
                            <a href="javascript:void(0)" class="d-block border py-3">
                                <span class="icon-exclamation-circle icomoon fgray h2"></span>
                                <span claSS="d-block fblack">Payments</span>
                            </a>
                        </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-lg-2 text-center">
                            <a href="javascript:void(0)" class="d-block border py-3">
                                <span class="icon-exclamation-circle icomoon fgray h2"></span>
                                <span claSS="d-block fblack">Badges</span>
                            </a>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ profile status --> 

                    <!-- recent activity in tasker  and notificatiions -->   
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-6">
                            <h5 class="h6 pb-2">Recent Activity on Airtasker</h5>
                            <ul class="list-group list-notify">
                                <li class="list-group-item">
                                    <img class="list-img" src="img/data/tasker01.jpg">
                                    <a href="javascript:void(0)" class="fblue">Troy.M</a>
                                    Made an offer on 
                                    <a href="javascript:void(0)" class="fblue">Stump grinding</a>
                                 </li>
                                 <li class="list-group-item">
                                    <img class="list-img" src="img/data/tasker01.jpg">
                                    <a href="javascript:void(0)" class="fblue">Jay R.</a>
                                    commented on 
                                    <a href="javascript:void(0)" class="fblue">Remove soil from existin.....</a>
                                 </li>
                                 <li class="list-group-item">
                                    <img class="list-img" src="img/data/tasker01.jpg">
                                    <a href="javascript:void(0)" class="fblue">Rodrigo  C.</a>
                                    commented on
                                    <a href="javascript:void(0)" class="fblue">Help repair my bed fram</a>
                                 </li>
                                 <li class="list-group-item">
                                    <img class="list-img" src="img/data/tasker01.jpg">
                                    <a href="javascript:void(0)" class="fblue">Troy.M</a>
                                    Made an offer on 
                                    <a href="javascript:void(0)" class="fblue">Stump grinding</a>
                                 </li>
                                 <li class="list-group-item">
                                    <img class="list-img" src="img/data/tasker01.jpg">
                                    <a href="javascript:void(0)" class="fblue">Ioana  A.</a>
                                    made an offer on 
                                    <a href="javascript:void(0)" class="fblue">Drop flowers</a>
                                 </li>
                                 <li class="list-group-item">
                                    <img class="list-img" src="img/data/tasker01.jpg">
                                    <a href="javascript:void(0)" class="fblue">Troy.M</a>
                                    Made an offer on 
                                    <a href="javascript:void(0)" class="fblue">Stump grinding</a>
                                 </li>                              
                            </ul>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-6">
                            <h5 class="h6 pb-2">Notifications</h5>
                            <ul class="list-group list-notify">
                                <li class="list-group-item">
                                    <img class="list-img" src="img/data/tasker01.jpg">
                                    <a href="javascript:void(0)" class="fblue">Mitchell B.</a>
                                     has made an offer on
                                    <a href="javascript:void(0)" class="fblue">Car Washing</a>
                                 </li>
                                 <li class="list-group-item">
                                    <img class="list-img" src="img/data/tasker02.jpg">
                                    <a href="javascript:void(0)" class="fblue">Dan S.</a>
                                    commented on 
                                    <a href="javascript:void(0)" class="fblue"> Moving of my home app..</a>
                                 </li>
                                 <li class="list-group-item">
                                    <img class="list-img" src="img/data/tasker03.jpg">
                                    <a href="javascript:void(0)" class="fblue">Abdul rahman S.</a>
                                    commented on
                                    <a href="javascript:void(0)" class="fblue"> Moving of my....</a>
                                 </li>
                                 <li class="list-group-item">
                                    <img class="list-img" src="img/data/tasker04.jpg">
                                    <a href="javascript:void(0)" class="fblue">Jay C. </a>
                                    has made an offer on
                                    <a href="javascript:void(0)" class="fblue">Car Washing</a>
                                 </li>
                                 <li class="list-group-item">
                                    <img class="list-img" src="img/data/tasker05.jpg">
                                    <a href="javascript:void(0)" class="fblue">Khyran C.</a>
                                    commented on
                                    <a href="javascript:void(0)" class="fblue">Car Washing</a>
                                 </li>
                                 <li class="list-group-item">
                                    <img class="list-img" src="img/data/tasker01.jpg">
                                    <a href="javascript:void(0)" class="fblue">G O. </a>
                                    commented on
                                    <a href="javascript:void(0)" class="fblue">Moving of my home applain</a>
                                 </li>                              
                            </ul>
                        </div>
                        <!--/ col -->


                    </div>
                    <!-- /recent activity in tasker  and notificatiions -->


                </div>
                <!--/ right user panel -->
            </div>
            <!--/ right profile -->
        </div>
        <!--/ row -->
    </div>
    <!--/ user container -->
  </main>
  <!--/ main -->

  <?php include 'scripts.php' ?> 
</body>
</html>