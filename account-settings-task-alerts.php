<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Task alerts</title>
  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header-postlogin.php' ?>

  <!-- main -->
  <main class="subpage usersubpage">
    <!--user container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- left navigation -->
            <div class="col-lg-3 leftnavigation">
              <?php include 'user-leftnav.php' ?>
            </div>
            <!--/ left navigatin -->

            <!-- right profile -->
            <div class="col-lg-9">
                <!-- right user panel-->
                <div class="right-user-panel">
                    <h1 class="h5 title-page">Task alerts</h1>

                    <p>These are emails and push notifications about tasks you may be interested in.</p>

                    <div class="graybox mb-3">
                        <form class="custom-form">
                            <div class="form-group  mb-0">
                                <label class="container-form mx-2  d-inline-block pt-1">Turn on task alerts to get notified about tasks we think you’d like.
                                    <input type="checkbox" checked="">
                                    <span class="checkmark"></span>
                                </label>  
                            </div>
                        </form>
                    </div>

                    <h6 class="h6 text-uppercase">ADD CUSTOM TASK ALERTS</h6>
                    <p class="pb-0">Got something specific in mind? Add a custom task alert and we'll let you know when a matching task pops up.</p>

                    <button class="pinkbtnlg" data-toggle="modal" data-target="#custom-task">Add Custom alert</button>                   
                     
                </div>
                <!--/ right user panel -->
            </div>
            <!--/ right profile -->
        </div>
        <!--/ row -->
    </div>
    <!--/ user container -->
  </main>
  <!--/ main -->

  <!-- custom task modal -->
  <!-- Modal -->
<div class="modal fade" id="custom-task" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-dialog-centered modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add a task alert</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- body-->
      <div class="modal-body">
        <p>What type of task are you looking for?</p>

        <form class="custom-form py-3">
            <div class="d-flex form-group">
                <label class="container-form">In Person
                    <input type="radio" checked="checked" name="radio">
                    <span class="checkmark-radio"></span>
                </label>
                <label class="container-form mx-2">Remote
                    <input type="radio" checked="checked" name="radio">
                    <span class="checkmark-radio"></span>
                </label>
            </div>

            <div class="form-group">
                <label>What specific kind of task?</label>
                <div class="input-group">
                    <input type="text" placeholder="e.g.End of lease clearning" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label>Where would you like to work?</label>
                <div class="input-group">
                    <input type="text" value="New form QLD, Australia" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label>Distance</label>
                <div class="input-group">
                    <select class="form-control">
                        <option>+5km</option>
                        <option>+10km</option>
                        <option>+15km</option>
                        <option>+20km</option>
                        <option>+25km</option>
                    </select>
                </div>
            </div>
        </form>


      </div>
      <!--/ body -->
      <div class="modal-footer text-center">        
        <button type="button" class="bluebtnlg">Add Your Task Alert</button>
      </div>
    </div>
  </div>
</div>
  <!--/ custom task modal -->

  <?php include 'scripts.php' ?> 
</body>
</html>