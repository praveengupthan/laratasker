<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Account Badges</title>
  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header-postlogin.php' ?>

  <!-- main -->
  <main class="subpage usersubpage">
    <!--user container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- left navigation -->
            <div class="col-lg-3 leftnavigation">
              <?php include 'user-leftnav.php' ?>
            </div>
            <!--/ left navigatin -->

            <!-- right profile -->
            <div class="col-lg-9">
                <!-- right user panel-->
                <div class="right-user-panel">
                    <h1 class="h5 title-page">Badges</h1>

                    <p>Badges help members be sure who you are and what you can do! The more you collect, the more Job Posters and Taskers will trust in your abilities</p>

                    <p>Badges are issued when specific requirements are met. A green tick shows that the verification is currently active</p>

                    <a href="javasript:void(0)" class="fblue">Read More</a>

                    <!-- row -->
                    <div class="row badges-row">
                        <!-- col -->
                        <div class="col-lg-6">
                            <div class="badge-col">
                                <img src="img/badge_tasker_bushfire_relief.png" alt="" class="sm-thumb">
                                <article>
                                    <h6 class="flight h6">Bushfire Recovery</h6>
                                    <p class="small">Make offers on bushfire recovery tasks for the current Australian bushfire season.</p>
                                </article>
                                <button class="whitebtn">Add</button>
                            </div>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->

                     <!-- row -->
                     <div class="row badges-row">
                        <div class="col-lg-12">
                            <h6 class="fgray text-uppercase">ID Badges</h6>
                        </div>

                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="badge-col">
                                <img src="img/badge_poster_police_check.png" alt="" class="sm-thumb">
                                <article>
                                    <h6 class="flight h6">Police Check</h6>
                                    <p class="small">Provide peace of mind to other members by successfully completing a Police Check</p>
                                </article>
                                <button class="whitebtn">Add</button>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="badge-col">
                                <img src="img/badge_poster_auspost-dbad.png" alt="" class="sm-thumb">
                                <article>
                                    <h6 class="flight h6">Digital iD</h6>
                                    <p class="small">Give members a reason to choose you - knowing that your identity is verified with Australia Post’s Digital iD.</p>
                                </article>
                                <button class="whitebtn">Add</button>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="badge-col">
                                <img src="img/badge_poster_payment_method.png" alt="" class="sm-thumb">
                                <article>
                                    <h6 class="flight h6">Payment Method</h6>
                                    <p class="small">Make payments with ease by having your payment method verified.</p>
                                </article>
                                <button class="whitebtn">Add</button>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="badge-col">
                                <img src="img/badge_poster_mobile.png" alt="" class="sm-thumb">
                                <article>
                                    <h6 class="flight h6">Mobile</h6>
                                    <p class="small">Verified when you join, you'll receive instant task notifications and can make free calls via Airtasker.</p>
                                </article>
                                <button class="whitebtn">Add</button>
                            </div>
                        </div>
                        <!--/ col -->

                          <!-- col -->
                          <div class="col-lg-6">
                            <div class="badge-col">
                                <img src="img/badge_poster_facebook.png" alt="" class="sm-thumb">
                                <article>
                                    <h6 class="flight h6">Facebook</h6>
                                    <p class="small">Connect your Facebook profile to build your online social reputation.</p>
                                </article>
                                <button class="whitebtn">Add</button>
                              </div>
                          </div>
                          <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-6">
                            <div class="badge-col">
                                <img src="img/badge_poster_twitter.png" alt="" class="sm-thumb">
                                <article>
                                    <h6 class="flight h6">Twitter</h6>
                                    <p class="small">Connect your Twitter profile to build your online social reputation.</p>
                                </article>
                                <button class="whitebtn">Add</button>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="badge-col">
                                <img src="img/badge_poster_linkedin.png" alt="" class="sm-thumb">
                                <article>
                                    <h6 class="flight h6">LinkedIn</h6>
                                    <p class="small">Connect your professional Linkedin profile to build your online social reputation.</p>
                                </article>
                                <button class="whitebtn">Add</button>
                            </div>
                        </div>
                        <!--/ col -->

                     </div>
                     <!--/ row -->

                      <!-- row -->
                      <div class="row badges-row">
                        <div class="col-lg-12">
                            <h6 class="fgray text-uppercase">Licence Badges</h6>
                        </div>

                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="badge-col">
                                <img src="img/Electric_114x114.png" alt="" class="sm-thumb">
                                <article>
                                    <h6 class="flight h6">Electrical</h6>
                                    <p class="small">Let others know you hold an electrical contractor licence with this badge.</p>
                                </article>
                                <button class="whitebtn">Add</button>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="badge-col">
                                <img src="img/Plumbing.png" alt="" class="sm-thumb">
                                <article>
                                    <h6 class="flight h6">Plumbing</h6>
                                    <p class="small">Let others know you hold a valid Plumbing licence with this badge.</p>
                                </article>
                                <button class="whitebtn">Add</button>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="badge-col">
                                <img src="img/Gas_114x114.png" alt="" class="sm-thumb">
                                <article>
                                    <h6 class="flight h6">Gasfitting</h6>
                                    <p class="small">Let others know you hold a valid Gasfitting licence with this badge.</p>
                                </article>
                                <button class="whitebtn">Add</button>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="badge-col">
                                <img src="img/Trades_insurance.png" alt="" class="sm-thumb">
                                <article>
                                    <h6 class="flight h6">Trades Insurance</h6>
                                    <p class="small">Let people know you’re covered by your own public liability insurance.</p>
                                </article>
                                <button class="whitebtn">Add</button>
                            </div>
                        </div>
                        <!--/ col -->

                          <!-- col -->
                          <div class="col-lg-6">
                            <div class="badge-col">
                                <img src="img/badge_poster_facebook.png" alt="" class="sm-thumb">
                                <article>
                                    <h6 class="flight h6">Working With Children</h6>
                                    <p class="small">Connect your Facebook profile to build your online social reputation.</p>
                                </article>
                                <button class="whitebtn">Add</button>
                              </div>
                          </div>
                          <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-6">
                            <div class="badge-col">
                                <img src="img/Childcare__1_.png" alt="" class="sm-thumb">
                                <article>
                                    <h6 class="flight h6">Twitter</h6>
                                    <p class="small">Let others know you hold a valid Working with Children Check with this badge.</p>
                                </article>
                                <button class="whitebtn">Add</button>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="badge-col">
                                <img src="img/Asbestos_removal.png" alt="" class="sm-thumb">
                                <article>
                                    <h6 class="flight h6">Asbestos Removal</h6>
                                    <p class="small">Let others know you hold a valid asbestos removal licence with this badge.</p>
                                </article>
                                <button class="whitebtn">Add</button>
                            </div>
                        </div>
                        <!--/ col -->

                     </div>
                     <!--/ row -->

                       <!-- row -->
                       <div class="row badges-row">
                        <div class="col-lg-12">
                            <h6 class="fgray text-uppercase">PARTNERSHIP BADGES</h6>
                        </div>

                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="badge-col">
                                <img src="img/badge_poster_ikea.png" alt="" class="sm-thumb">
                                <article>
                                    <h6 class="flight h6">IKEA</h6>
                                    <p class="small">Showcase your furniture assembly expertise with an IKEA Badge.</p>
                                </article>
                                <button class="whitebtn">Add</button>
                            </div>
                        </div>
                        <!--/ col -->
                     </div>
                     <!--/ row -->
                     
                </div>
                <!--/ right user panel -->
            </div>
            <!--/ right profile -->
        </div>
        <!--/ row -->
    </div>
    <!--/ user container -->
  </main>
  <!--/ main -->

  <?php include 'scripts.php' ?> 
</body>
</html>