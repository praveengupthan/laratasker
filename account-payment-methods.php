<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Account Dashboard</title>
  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header-postlogin.php' ?>

  <!-- main -->
  <main class="subpage usersubpage">
    <!--user container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- left navigation -->
            <div class="col-lg-3 leftnavigation">
              <?php include 'user-leftnav.php' ?>
            </div>
            <!--/ left navigatin -->

            <!-- right profile -->
            <div class="col-lg-9">
                <!-- right user panel-->
                <div class="right-user-panel">
                    <h1 class="h5 title-page">Payment Methods</h1>

                     <!-- tab -->
                    <div class="custom-tab">
                    
                          <ul class="nav nav-pills" id="myTab" role="tablist">
                              <li class="nav-item">
                                  <a class="nav-link active" id="make-payment-tab" data-toggle="tab" href="#makepayment" role="tab" aria-controls="home" aria-selected="true">Make Payment</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" id="receivepayment-tab" data-toggle="tab" href="#receivepayment" role="tab" aria-controls="profile" aria-selected="false">Receive Payment</a>
                              </li>                           
                          </ul>

                          <div class="tab-content pt-3" id="myTabContent">
                              <!--  Make Payment -->
                              <div class="tab-pane fade show active" id="makepayment" role="tabpanel" aria-labelledby="make-payment-tab">
                                <!-- row -->
                                <div class="row pt-3 border-top">
                                    <!-- col -->
                                    <div class="col-lg-12">
                                        <p>When you are ready to accept a Tasker's offer, you will be required to pay for the task using Airtasker Pay. Payment will be held securely until the task is complete and you release payment to the Tasker.</p>

                                        <div class="py-3">
                                            <h6 class="h6">CREDIT CARD</h6>
                                            <a href="javascript:void(0)" class="fblue" data-toggle="modal" data-target="#addcard">
                                                <span class="icon-plus-circle icomoon"></span> Add your credit card
                                            </a>
                                        </div>

                                        <div class="py-3">
                                            <h6 class="h6">AIRTASKER CARD</h6>
                                            <p>Task Credit balance:</p>
                                            <h3 class="h1 flight fblue">$0</h3>
                                            <p>The Task Credit balance from Airtasker Cards will be automatically applied when you accept an offer on a task. Each card will expire 12 months from the date of registration. Any unused amount after expiry day will not be refunded or credited.</p>                                           
                                        </div>
                                    </div>
                                    <!--/ col --> 
                                </div>
                                <!--/ row -->

                              </div>
                              <!--/ Make Payment -->

                              <!-- Received Payment -->
                              <div class="tab-pane fade" id="receivepayment" role="tabpanel" aria-labelledby="receivepayment-tab">
                                <!-- row -->
                                <div class="row pt-3 border-top"> 

                                    <div class="col-lg-12"> 
                                        <p>Once a task is completed, you will be able to request payment from the Job Poster, who will then release it to your nominated account.</p>

                                        <h6 class="h6">Billing Address</h6>
                                        <p>Your billing address will be verified before you can receive payments.</p>
                                    </div>

                                    <!-- col -->
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Address Line 1</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Address Line 1">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                     <!-- col -->
                                     <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Address Line 2 (optional)</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Address Line 2">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                     <!-- col -->
                                     <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Suburb</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Suburb">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>State</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="State">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                     <!-- col -->
                                     <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Postcode</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Postcode">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                     <!-- col -->
                                     <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Country</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Country">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-12 pt-3">
                                        <a class="bluebtnlg" href="javascript:void(0)">Add Billing Address</a>
                                       <p class="pt-2"> <small>Your address will never been shown publicly, it is only used for account verification purposes.</small></p>
                                    </div>
                                    <!--/ col -->


                                    
                                    <div class="col-lg-12">                                        

                                        <h6 class="h6">Bank Account Details</h6>
                                        <p>Please provide your bank details so you can get paid. We don't take any money from your account.</p>
                                    </div>

                                    <!-- col -->
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Account holder name</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Account holder name">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                     <!-- col -->
                                     <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Account number</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Account number">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                     <!-- col -->
                                     <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>BSB</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="000-000">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-12 pt-3">
                                        <a class="bluebtnlg" href="javascript:void(0)">Add Bank Account</a>
                                    </div>
                                    <!--/ col -->
                                </div>
                                <!--/ row -->

                              </div>
                              <!--/Received Payment -->
                          </div>

                      </div>

                      <!--/ tab -->

                </div>
                <!--/ right user panel -->
            </div>
            <!--/ right profile -->
        </div>
        <!--/ row -->
    </div>
    <!--/ user container -->
  </main>
  <!--/ main -->


<!-- Modal -->
<div class="modal fade" id="addcard" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-md">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add your credit card</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-lg-12">
                <div class="form-group">
                    <label>Card Number</label>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Enter Card Number">
                    </div>
                </div>
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6">
                <div class="form-group">
                    <label>Expiry Date</label>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="MM/YY">
                    </div>
                </div>
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-lg-6">
                <div class="form-group">
                    <label>CVC</label>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="CVC">
                    </div>
                </div>
            </div>
            <!--/ col -->

            <div class="col-lg-12">
                <p class="small">By providing your credit card details, you agree to Stripe's Terms & Conditions.</p>
            </div>

        </div>
        <!--/ row -->
      </div>
      <div class="modal-footer">
        <button type="button" class="pinkbtnlg" data-dismiss="modal">Cancel</button>
        <button type="button" class="bluebtnlg">Save Card</button>
      </div>
    </div>
  </div>
</div>

  <?php include 'scripts.php' ?> 


</body>
</html>