<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Account Dashboard</title>
  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header-postlogin.php' ?>

  <!-- main -->
  <main class="subpage usersubpage">
    <!--user container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- left navigation -->
            <div class="col-lg-3 leftnavigation">
              <?php include 'user-leftnav.php' ?>
            </div>
            <!--/ left navigatin -->

            <!-- right profile -->
            <div class="col-lg-9">
                <!-- right user panel-->
                <div class="right-user-panel">
                    <h1 class="h5 title-page">Notifications</h1>

                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-12">                       
                           
                            <ul class="list-group list-notify">
                                <li class="list-group-item">
                                    <img class="list-img" src="img/data/tasker01.jpg">
                                    <a href="javascript:void(0)" class="fblue">Troy.M</a>
                                    Made an offer on 
                                    <a href="javascript:void(0)" class="fblue">Stump grinding</a>
                                    <span class="float-right small">2 weeks ago</span>
                                 </li>
                                 <li class="list-group-item">
                                    <img class="list-img" src="img/data/tasker02.jpg">
                                    <a href="javascript:void(0)" class="fblue">Jay R.</a>
                                    commented on 
                                    <a href="javascript:void(0)" class="fblue">Remove soil from existin.....</a>
                                    <span class="float-right small">2 weeks ago</span>
                                 </li>
                                 <li class="list-group-item">
                                    <img class="list-img" src="img/data/tasker03.jpg">
                                    <a href="javascript:void(0)" class="fblue">Rodrigo  C.</a>
                                    commented on
                                    <a href="javascript:void(0)" class="fblue">Help repair my bed fram</a>
                                    <span class="float-right small">2 weeks ago</span>
                                 </li>
                                 <li class="list-group-item">
                                    <img class="list-img" src="img/data/tasker04.jpg">
                                    <a href="javascript:void(0)" class="fblue">Troy.M</a>
                                    Made an offer on 
                                    <a href="javascript:void(0)" class="fblue">Stump grinding</a>
                                    <span class="float-right small">2 weeks ago</span>
                                 </li>
                                 <li class="list-group-item">
                                    <img class="list-img" src="img/data/tasker05.jpg">
                                    <a href="javascript:void(0)" class="fblue">Ioana  A.</a>
                                    made an offer on 
                                    <a href="javascript:void(0)" class="fblue">Drop flowers</a>
                                    <span class="float-right small">2 weeks ago</span>
                                 </li>
                                 <li class="list-group-item">
                                    <img class="list-img" src="img/data/tasker06.jpg">
                                    <a href="javascript:void(0)" class="fblue">Troy.M</a>
                                    Made an offer on 
                                    <a href="javascript:void(0)" class="fblue">Stump grinding</a>
                                    <span class="float-right small">2 weeks ago</span>
                                 </li>                              
                            </ul>
                       
                        </div>
                        <!--/col -->
                    </div>


                </div>
                <!--/ right user panel -->
            </div>
            <!--/ right profile -->
        </div>
        <!--/ row -->
    </div>
    <!--/ user container -->
  </main>
  <!--/ main -->

  <?php include 'scripts.php' ?> 
</body>
</html>