<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Jobs</title>

  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header.php' ?>

  <!-- main -->
  <main class="subpage">

  <!-- container -->
  <div class="container">

  <!-- row -->
  <div id="page-wrap" class="row jobs-row">
    <!-- col -->
    <ul class="jobs-list col-lg-3">
        <li><a href="#one" class="scrollTo" >Accounting</a></li>
        <li><a href="#two" class="scrollTo" >Admin</a></li>
        <li><a href="#three" class="scrollTo" >Assembly</a></li>
        <li><a href="#four" class="scrollTo" >Bakery</a></li>
        <li><a href="#five" class="scrollTo" >Bricklayer</a></li>
        <li><a href="#six" class="scrollTo" >Carpet Cleaning</a></li>
    </ul>
    <!--/ col -->

    <!-- col -->
    <div  id="movejobs" class="col-lg-9">
        <h2 class="h3">Earn money on Airtasker</h2>
        <p>Airtasker is Australia's largest Job marketplace for all kind of Jobs from handyman to cleaners to gardeners. Sign up now and Get Hired!</p>
        <!-- 1-->            
        <div id="one" class="rt-job"> 

            <h2 class="h4" >Accounting</h2>
            <img src="img/data/business-admin.jpg" class="img-fluid" alt="">

            <p class="jobs-links">
                <a href="javascript:void(0)">Book Keeping</a>
                <a href="javascript:void(0)">Financial Modelling</a>
                <a href="javascript:void(0)">Financial Planning</a>
                <a href="javascript:void(0)">Financial Reporting</a>
                <a href="javascript:void(0)">Payroll Accountant</a>
                <a href="javascript:void(0)">Tax Accountants</a>          
            </p>
           
        </div>
        <!--/ 1-->

        <!--2-->  
        <div id="two" class="rt-job">  
                
            <div class="d-flex justify-content-between">
                <h2 class="h4" >Admin</h2>
                <p><a href="#top">Top</a></p>
            </div>

            <img src="img/data/office.jpg" class="img-fluid" alt="">

            <p class="jobs-links">
                <a href="javascript:void(0)">Book Keeping</a>
                <a href="javascript:void(0)">Financial Modelling</a>
                <a href="javascript:void(0)">Financial Planning</a>
                <a href="javascript:void(0)">Financial Reporting</a>
                <a href="javascript:void(0)">Payroll Accountant</a>
                <a href="javascript:void(0)">Tax Accountants</a>          
            </p>

            <p>quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum
            sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. </p>
        </div>
        <!--2/-->

        <!--3 -->
        <div id="three" class="rt-job">

            <div class="d-flex justify-content-between">
                <h2 class="h4" >Assembly</h2>
                <p><a href="#top">Top</a></p>
            </div>

            <img src="img/data/furniture-assembly.jpg" class="img-fluid" alt="">

            <p class="jobs-links">
                <a href="javascript:void(0)">Book Keeping</a>
                <a href="javascript:void(0)">Financial Modelling</a>
                <a href="javascript:void(0)">Financial Planning</a>
                <a href="javascript:void(0)">Financial Reporting</a>
                <a href="javascript:void(0)">Payroll Accountant</a>
                <a href="javascript:void(0)">Tax Accountants</a>          
            </p>

            <p>quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum
            sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus,
            tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
           
        </div>
        <!--/ 3 -->

         <!--4 -->
         <div id="four" class="rt-job">

        <div class="d-flex justify-content-between">
            <h2 class="h4" >Bakery</h2>
            <p><a href="#top">Top</a></p>
        </div>

        <img src="img/data/cooking.jpg" class="img-fluid" alt="">

        <p class="jobs-links">
            <a href="javascript:void(0)">Book Keeping</a>
            <a href="javascript:void(0)">Financial Modelling</a>
            <a href="javascript:void(0)">Financial Planning</a>
            <a href="javascript:void(0)">Financial Reporting</a>
            <a href="javascript:void(0)">Payroll Accountant</a>
            <a href="javascript:void(0)">Tax Accountants</a>          
        </p>

        <p>quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentumsed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. </p>

        </div>
        <!--/ 4 -->

         <!--5 -->
         <div id="five" class="rt-job">

        <div class="d-flex justify-content-between">
            <h2 class="h4" >Bricklayer</h2>
            <p><a href="#top">Top</a></p>
        </div>

        <img src="img/data/handyman (1).jpg" class="img-fluid" alt="">

        <p class="jobs-links">
            <a href="javascript:void(0)">Book Keeping</a>
            <a href="javascript:void(0)">Financial Modelling</a>
            <a href="javascript:void(0)">Financial Planning</a>
            <a href="javascript:void(0)">Financial Reporting</a>
            <a href="javascript:void(0)">Payroll Accountant</a>
            <a href="javascript:void(0)">Tax Accountants</a>          
        </p>

        <p>quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. </p>

        </div>
        <!--/ 5 -->

        
         <!--6 -->
         <div id="six" class="rt-job">

        <div class="d-flex justify-content-between">
            <h2 class="h4" >Carpet Cleaning</h2>
            <p><a href="#top">Top</a></p>
        </div>

        <img src="img/data/cleaning.jpg" class="img-fluid" alt="">

        <p class="jobs-links">
            <a href="javascript:void(0)">Book Keeping</a>
            <a href="javascript:void(0)">Financial Modelling</a>
            <a href="javascript:void(0)">Financial Planning</a>
            <a href="javascript:void(0)">Financial Reporting</a>
            <a href="javascript:void(0)">Payroll Accountant</a>
            <a href="javascript:void(0)">Tax Accountants</a>          
        </p>

        <p>quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. </p>

        </div>
        <!--/ 6 -->


    </div>
    <!-- col -->

    </div>
    <!--/ row -->      
  </div>
  <!--/ container -->

    
  </main>
  <!--/ main -->
  <?php include 'footer.php' ?>
  <?php include 'scripts.php' ?> 

</body>
</html>