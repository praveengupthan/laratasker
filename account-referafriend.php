<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Account Dashboard</title>
  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header-postlogin.php' ?>

  <!-- main -->
  <main class="subpage usersubpage">
    <!--user container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- left navigation -->
            <div class="col-lg-3 leftnavigation">
              <?php include 'user-leftnav.php' ?>
            </div>
            <!--/ left navigatin -->

            <!-- right profile -->
            <div class="col-lg-9">
                <!-- right user panel-->
                <div class="right-user-panel">
                    <h1 class="h5 title-page">Refer a Friend</h1>

                    <!-- row -->
                    <div class="row sharerow py-3">
                        <!-- col -->
                        <div class="col-lg-4">
                            <img src="img/Illu-Gift.png" alt="" class="img-fluid">
                        </div>
                        <!--/ col-->
                        <!-- col -->
                      <div class="col-lg-8 align-self-center">
                            <h5 class="h6">Get free Airtasker Credit</h5>
                            <p>Invite a friend to try Airtasker with $25 towards their first task. Once it’s completed, you’ll get $10 Airtasker Credit*.</p>

                            <!-- copy link -->
                            <div class="copylink d-flex form-group">
                                <input class="form-control" value="https://www.airtasker.com/r/nandipati-p-18423484/" >
                                <button class="bluebtnlg align-self-center mt-0">Copy link</button>
                            </div>
                            <!--/ copy link -->

                            <!-- social -->
                            <div class="socialcol">
                                <p class="pb-0">Share this on</p>
                                <a href="javascript:void(0)">
                                <span class="icon-facebook icomoon"></span>
                                </a>
                                <a href="javascript:void(0)">
                                <span class="icon-twitter icomoon"></span>
                                </a>
                                <a href="javascript:void(0)">
                                <span class="icon-linkedin icomoon"></span>
                                </a>
                                <a href="javascript:void(0)">
                                <span class="icon-instagram icomoon"></span>
                                </a>
                            </div>                            
                            <!--/ social -->
                      </div>
                      <!--/ col -->
                    </div>
                    <!-- row -->

                    <!-- row -->
                    <div class="row py-4">
                        <div class="col-lg-12">
                            <h6 class="text-center h5 flight">You’ve got <span class="fbold fblue">$0</span> Airtasker Credit right now. Time to refer some friends!</h6>

                            <h6 class="text-center h5 pt-4 pb-3">How referrals work</h6>

                            <!-- row -->
                            <div class="row howrow">
                                
                                <!-- col -->
                                <div class="col-lg-4 text-center how-col">
                                    <span class="number h6">1</span>
                                    <h6 class="h6">Invite your friends</h6>
                                    <p class="text-center">Your friend signs up using your referral code.</p>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-4 text-center how-col">
                                    <span class="number h6">2</span>
                                    <h6 class="h6">Their first task is done</h6>
                                    <p class="text-center">After joining Airtasker, they post a task and get it completed.</p>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-4 text-center how-col">
                                    <span class="number h6">3</span>
                                    <h6 class="h6">You get $10 credit</h6>
                                    <p class="text-center">For every friend you refer, you get $10 Airtasker Credit.</p>
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ row -->

                            <!-- row -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <p class="small">*Terms & Conditions apply.</p>
                                    <p class="small">
                                    *Referral Coupons may be used in the purchase of a Task on the Airtasker Platform. Referred Member must become an Airtasker User and agree to the Airtasker Terms & Conditions. Minimum Task value to receive the benefit conferred by the Referral Coupon is $100. Limit of one Referral Coupon per User & valid only for the first Task Contract created by the Referred Member. Cannot be used with any other coupon or Airtasker promotion. Referral Coupons are valid for at least 30 days from issue and Airtasker may change or cancel the terms of the Referral Coupon at any time on 30 days' notice.
                                    </p>
                                    <p class="small">
                                    The Referring Member will receive a $10 Task credit towards the purchase of tasks on the Airtasker Platform, when the Referred Member has completed their first Task Contract. The Task credit is valid for 12 months from issue, and is valid for the payment of Posted Tasks on the Airtasker Platform. Posted Tasks must comply with Terms & Conditions. Limit of 15 x $10 Task credits issued per Referring Member. Airtasker may change or cancel the terms of the Task credit at any time on 30 days' notice.
                                    </p>
                                </div>
                            </div>
                            <!--/ row -->
                        </div>
                    </div>
                    <!--/ row -->
                </div>
                <!--/ right user panel -->
            </div>
            <!--/ right profile -->
        </div>
        <!--/ row -->
    </div>
    <!--/ user container -->
  </main>
  <!--/ main -->

  <?php include 'scripts.php' ?> 
</body>
</html>