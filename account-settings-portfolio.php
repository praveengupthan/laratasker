<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Portfolio</title>
  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header-postlogin.php' ?>

  <!-- main -->
  <main class="subpage usersubpage">
    <!--user container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- left navigation -->
            <div class="col-lg-3 leftnavigation">
              <?php include 'user-leftnav.php' ?>
            </div>
            <!--/ left navigatin -->

            <!-- right profile -->
            <div class="col-lg-9">
                <!-- right user panel-->
                <div class="right-user-panel">
                    <h1 class="h5 title-page">Portfolio</h1>

                    <p>Your notifications can be updated at any time via the options below or the Airtasker App.</p>

                    <div class="py-4 border-bottom">
                        <h6 class="h6">Upload your resume</h6>
                        <p>Adding your resume can definitely help members understand your skills and qualifications.</p>
                        <p><small>File formats can be PDF/DOC/TXT/RTF and no larger than 5MB.</small></p>

                        <div class="inputfile pl-3 align-self-center">
                            <input type="file" title="Upload Resume" class="whitebtn">
                            <!-- <button class="pinkbtnlg mt-0">Upload File</button> -->
                        </div>
                    </div>

                    
                    <div class="py-4 border-bottom">
                        <h6 class="h6">Upload portfolio items</h6>
                        <p>Showcase your talents by adding items to your portfolio (visible on your profile). This is particularly great for photographers, designers and illustrators, but also great for a gallery to advertise you completing tasks.</p>
                        <p>You may upload a maximum of 30 items. File formats can be JPG/PNG/PDF/TXT and must be no larger than 5MB. For your own security and privacy, please make sure you don't upload any personal details in your attachments.</p>

                        <div class="inputfile pl-3 align-self-center">
                            <input type="file" title="Select Files" class="whitebtn">
                            <!-- <button class="pinkbtnlg mt-0">Upload File</button> -->
                        </div>
                    </div>
                     
                </div>
                <!--/ right user panel -->
            </div>
            <!--/ right profile -->
        </div>
        <!--/ row -->
    </div>
    <!--/ user container -->
  </main>
  <!--/ main -->

  <!-- Chagne Mail -->

<div class="modal fade" id="edit-mail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-dialog-centered modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Change / Edit Mail </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- body-->
      <div class="modal-body">
        <p>Change your email address to keep in touch with Taskers and posters, get notifications on tasks and alerts that you've set up</p>

        <form class="custom-form py-3">          

            <div class="form-group">
                <label>Enter your email address</label>
                <div class="input-group">
                    <input type="text" value="praveennandipati@gmail.com" placeholder="Enter your Email" class="form-control">
                </div>
            </div>           
        </form>

        <p>You can choose what types of emails you receive from us. We may still contact you about your account if it's urgent.</p>
      </div>
      <!--/ body -->
      <div class="modal-footer text-center">        
        <button type="button" class="bluebtnlg">Save</button>
      </div>
    </div>
  </div>
</div>
  <!--/ Chagne Mail -->

   <!-- Chagne Phone -->

<div class="modal fade" id="edit-phone" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-dialog-centered modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Verify Mobile for free SMS alerts </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- body-->
      <div class="modal-body">
        <p>We noticed you want to receive SMS alerts, but you don't have a verified mobile listed on your account. Please add one to enable free alerts.</p>

        <form class="custom-form py-3">          

            <div class="form-group">
                <label>Mobile number</label>
                <div class="input-group">
                    <input type="text" value="+91 9642123254" placeholder="Phone Number" class="form-control">
                </div>
            </div>           
        </form>
        <p>We will send you a verification code</p>
      </div>
      <!--/ body -->
      <div class="modal-footer text-center">        
        <button type="button" class="bluebtnlg">Send</button>
      </div>
    </div>
  </div>
</div>
  <!--/ Chagne Phone -->

  <?php include 'scripts.php' ?> 
</body>
</html>