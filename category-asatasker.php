<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Lara Tasker Tasks List</title>

  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
  <?php include 'header.php' ?>

  <!-- main -->
  <main class="subpage">
      <!-- how it works section 1-->
      <div class="howit-primary">
          <!-- container -->
          <div class="container">
              <!-- row -->
              <div class="row justify-content-center">
                  <div class="col-lg-8 align-self-center text-center">
                      <h1 class="h2">Accounting Jobs Near You</h1>
                      <p class="text-center">Browse through over 500 Accounting Jobs.</p>
                  </div>
                  <div class="col-lg-4 text-center">
                        <div class="card card-category">
                            <div class="card-heder text-center py-2 border-bottom">
                                <h6 class="h6 pt-2">SEE WHAT YOU COULD EARN</h5>
                            </div>
                            <div class="card-body text-center">
                                <div class="form-group w-75 mx-auto">
                                    <select class="form-control">
                                        <option>1-2 tasker per week</option>
                                        <option>3-5 tasker per week</option>
                                        <option>5-6 tasker per week</option>
                                        <option>6 + tasker per week</option>
                                    </select>
                                </div>
                                <h2 class="h1 fbold pb-0 mb-0">$866</h2>
                                <p class="text-center"><small class="fgray">Per Month</small></p>
                                <p class="text-center"><a href="javascript:void(0)" class="pinkbtnlg">Become a Tasker</a></p>
                                <p class="text-center pt-3">Based on average accounting task prices. Actual marketplace earnings may vary</p>
                            </div>
                        </div>
                  </div>
              </div>
              <!--/ row -->  
          </div>
          <!-- /container -->
      </div>
      <!-- /how it works section 1-->

      <!-- tasks sections -->
      <div class="tasks-section other-getdone">
            <!-- container -->
            <div class="container">
                  <!-- title row -->
              <div class="row justify-content-center title-row">
                  <div class="col-lg-6 text-center">
                      <h2 class="h3">Accounting tasks</h2>
                      <p class="text-center fgray">Check out what people want done near you right now...</p>
                  </div>
              </div>
              <!--/ title row -->

              <!-- row -->
              <div class="row">
                    <!-- col -->
                    <div class="col-lg-4">
                        <!-- card -->
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header d-flex justify-content-between">
                            <h4 class="h6">Accounting</h4>
                            <a class="fblue small fbold" href="javascript:void(0)">Get this job</a>
                          </div>
                          <!--/ card header -->
                            <a href="javascript:void(0)">
                                 <!-- card body -->
                                <div class="card-body d-flex justify-content-between p-3">
                                    <div class="d-flex thumb">
                                    <img src="img/thumbnail.png" alt="">
                                    <h5 class="h6 align-self-center">I need bookkeeping</h5>
                                    </div>
                                    <h4 class="h6 align-self-center">$20</h4>
                                </div>
                                <!--/card card body -->
                            </a>
                            <!-- card footer -->
                            <div class="card-footer">
                                    <p class="small pb-0"><span class="icon-clock-o"></span> 10 Hours ago</p>
                            </div>
                            <!--/ card footer -->
                        </div>
                        <!--/ card -->                    
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-4">
                        <!-- card -->
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header d-flex justify-content-between">
                            <h4 class="h6">Accounting</h4>
                            <a class="fblue small fbold" href="javascript:void(0)">Get this job</a>
                          </div>
                          <!--/ card header -->
                            <a href="javascript:void(0)">
                                 <!-- card body -->
                                <div class="card-body d-flex justify-content-between p-3">
                                    <div class="d-flex thumb">
                                    <img src="img/thumbnail.png" alt="">
                                    <h5 class="h6 align-self-center">Tax Returns</h5>
                                    </div>
                                    <h4 class="h6 align-self-center">$22</h4>
                                </div>
                                <!--/card card body -->
                            </a>
                            <!-- card footer -->
                            <div class="card-footer">
                                    <p class="small pb-0"><span class="icon-clock-o"></span> 10 Hours ago</p>
                            </div>
                            <!--/ card footer -->
                        </div>
                        <!--/ card -->                    
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-4">
                        <!-- card -->
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header d-flex justify-content-between">
                            <h4 class="h6">Accounting</h4>
                            <a class="fblue small fbold" href="javascript:void(0)">Get this job</a>
                          </div>
                          <!--/ card header -->
                            <a href="javascript:void(0)">
                                 <!-- card body -->
                                <div class="card-body d-flex justify-content-between p-3">
                                    <div class="d-flex thumb">
                                    <img src="img/thumbnail.png" alt="">
                                    <h5 class="h6 align-self-center">Brand Development & Sales Specialist</h5>
                                    </div>
                                    <h4 class="h6 align-self-center">$18</h4>
                                </div>
                                <!--/card card body -->
                            </a>
                            <!-- card footer -->
                            <div class="card-footer">
                                    <p class="small pb-0"><span class="icon-clock-o"></span> 10 Hours ago</p>
                            </div>
                            <!--/ card footer -->
                        </div>
                        <!--/ card -->                    
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-4">
                        <!-- card -->
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header d-flex justify-content-between">
                            <h4 class="h6">Accounting</h4>
                            <a class="fblue small fbold" href="javascript:void(0)">Get this job</a>
                          </div>
                          <!--/ card header -->
                            <a href="javascript:void(0)">
                                 <!-- card body -->
                                <div class="card-body d-flex justify-content-between p-3">
                                    <div class="d-flex thumb">
                                    <img src="img/thumbnail.png" alt="">
                                    <h5 class="h6 align-self-center">Support with BAS Account bills</h5>
                                    </div>
                                    <h4 class="h6 align-self-center">$12</h4>
                                </div>
                                <!--/card card body -->
                            </a>
                            <!-- card footer -->
                            <div class="card-footer">
                                    <p class="small pb-0"><span class="icon-clock-o"></span> 10 Hours ago</p>
                            </div>
                            <!--/ card footer -->
                        </div>
                        <!--/ card -->                    
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-4">
                        <!-- card -->
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header d-flex justify-content-between">
                            <h4 class="h6">Accounting</h4>
                            <a class="fblue small fbold" href="javascript:void(0)">Get this job</a>
                          </div>
                          <!--/ card header -->
                            <a href="javascript:void(0)">
                                 <!-- card body -->
                                <div class="card-body d-flex justify-content-between p-3">
                                    <div class="d-flex thumb">
                                    <img src="img/thumbnail.png" alt="">
                                    <h5 class="h6 align-self-center">I need accounting services</h5>
                                    </div>
                                    <h4 class="h6 align-self-center">$12</h4>
                                </div>
                                <!--/card card body -->
                            </a>
                            <!-- card footer -->
                            <div class="card-footer">
                                    <p class="small pb-0"><span class="icon-clock-o"></span> 10 Hours ago</p>
                            </div>
                            <!--/ card footer -->
                        </div>
                        <!--/ card -->                    
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-4">
                        <!-- card -->
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header d-flex justify-content-between">
                            <h4 class="h6">Accounting</h4>
                            <a class="fblue small fbold" href="javascript:void(0)">Get this job</a>
                          </div>
                          <!--/ card header -->
                            <a href="javascript:void(0)">
                                 <!-- card body -->
                                <div class="card-body d-flex justify-content-between p-3">
                                    <div class="d-flex thumb">
                                    <img src="img/thumbnail.png" alt="">
                                    <h5 class="h6 align-self-center">Tax depreciation schedule</h5>
                                    </div>
                                    <h4 class="h6 align-self-center">$20</h4>
                                </div>
                                <!--/card card body -->
                            </a>
                            <!-- card footer -->
                            <div class="card-footer">
                                    <p class="small pb-0"><span class="icon-clock-o"></span> 10 Hours ago</p>
                            </div>
                            <!--/ card footer -->
                        </div>
                        <!--/ card -->                    
                    </div>
                    <!--/ col -->

                      <!-- col -->
                      <div class="col-lg-4">
                        <!-- card -->
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header d-flex justify-content-between">
                            <h4 class="h6">Accounting</h4>
                            <a class="fblue small fbold" href="javascript:void(0)">Get this job</a>
                          </div>
                          <!--/ card header -->
                            <a href="javascript:void(0)">
                                 <!-- card body -->
                                <div class="card-body d-flex justify-content-between p-3">
                                    <div class="d-flex thumb">
                                    <img src="img/thumbnail.png" alt="">
                                    <h5 class="h6 align-self-center">Quickbooks Online (QBO) Help!</h5>
                                    </div>
                                    <h4 class="h6 align-self-center">$24</h4>
                                </div>
                                <!--/card card body -->
                            </a>
                            <!-- card footer -->
                            <div class="card-footer">
                                    <p class="small pb-0"><span class="icon-clock-o"></span> 10 Hours ago</p>
                            </div>
                            <!--/ card footer -->
                        </div>
                        <!--/ card -->                    
                    </div>
                    <!--/ col -->

                      <!-- col -->
                      <div class="col-lg-4">
                        <!-- card -->
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header d-flex justify-content-between">
                            <h4 class="h6">Accounting</h4>
                            <a class="fblue small fbold" href="javascript:void(0)">Get this job</a>
                          </div>
                          <!--/ card header -->
                            <a href="javascript:void(0)">
                                 <!-- card body -->
                                <div class="card-body d-flex justify-content-between p-3">
                                    <div class="d-flex thumb">
                                    <img src="img/thumbnail.png" alt="">
                                    <h5 class="h6 align-self-center">Help completing Final company Tax Return</h5>
                                    </div>
                                    <h4 class="h6 align-self-center">$20</h4>
                                </div>
                                <!--/card card body -->
                            </a>
                            <!-- card footer -->
                            <div class="card-footer">
                                    <p class="small pb-0"><span class="icon-clock-o"></span> 10 Hours ago</p>
                            </div>
                            <!--/ card footer -->
                        </div>
                        <!--/ card -->                    
                    </div>
                    <!--/ col -->

                      <!-- col -->
                      <div class="col-lg-4">
                        <!-- card -->
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header d-flex justify-content-between">
                            <h4 class="h6">Accounting</h4>
                            <a class="fblue small fbold" href="javascript:void(0)">Get this job</a>
                          </div>
                          <!--/ card header -->
                            <a href="javascript:void(0)">
                                 <!-- card body -->
                                <div class="card-body d-flex justify-content-between p-3">
                                    <div class="d-flex thumb">
                                    <img src="img/thumbnail.png" alt="">
                                    <h5 class="h6 align-self-center">Xero Certified Bookkeeper / Junior Accountant</h5>
                                    </div>
                                    <h4 class="h6 align-self-center">$20</h4>
                                </div>
                                <!--/card card body -->
                            </a>
                            <!-- card footer -->
                            <div class="card-footer">
                                    <p class="small pb-0"><span class="icon-clock-o"></span> 10 Hours ago</p>
                            </div>
                            <!--/ card footer -->
                        </div>
                        <!--/ card -->                    
                    </div>
                    <!--/ col -->
              </div>
              <!--/ row -->

              <!-- row -->
              <div class="row justify-content-center pb-5">
                    <div class="col-lg-4 text-center">
                        <a href="javascript:void(0)" class="bluebtnlg">Browse all Tasks</a>
                    </div>
              </div>
              <!--/ row -->
            </div>
            <!--/ container --> 
      </div>
      <!--/ tasks sections -->

          <!-- last features -->
          <div class="last-features">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-6 text-center order-lg-last">
                            <img src="img/why-become-a-tasker.jpg" alt="" class="img-fluid">
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6 align-self-center">
                            <h4 class="h4 pb-0 mb-0">Why become a Tasker?</h4>
                            <p>Here are just a few great reasons</p>

                            <div class="row py-3">
                                <div class="col-lg-1">
                                    <span class="icon-black-tie icomoon"></span>
                                </div>
                                <div class="col-lg-11">
                                    <h6>Be your own Boss</h6>
                                    <p>You choose what you do, when you do it and for how much - the power is in your hands.</p>
                                </div>
                            </div>

                            <div class="row py-3">
                                <div class="col-lg-1">
                                    <span class="icon-thumbs-o-up icomoon"></span>
                                </div>
                                <div class="col-lg-11">
                                    <h6>Secured payments</h6>
                                    <p>We secure payment before you start work so you can be sure it’s there when you’re done.</p>
                                </div>
                            </div>

                            <div class="row py-3 mb-3">
                                <div class="col-lg-1">
                                    <span class="icon-tag icomoon"></span>
                                </div>
                                <div class="col-lg-11">
                                    <h6>Every day is different</h6>
                                    <p>We get thousands of new and varied tasks that need doing every day, so no 2 days will be the same!</p>
                                </div>
                            </div>

                            <div class="row py-3">
                                <div class="col-lg-12">
                                    <!-- div buttons -->
                                    <div class="pb-4">                       
                                        <p class="text-center">
                                            <a href="javascript:void(0)" class="bluebtnlg">Become a  Tasker</a> 
                                        </p>
                                    </div>
                                    <!--/ div buttons -->
                                </div>                         
                            </div>

                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->

                     <!-- row -->
                     <div class="row">
                        <!-- col -->
                        <div class="col-lg-6 text-center">
                            <img src="img/insurance.jpg" alt="" class="img-fluid">
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6 align-self-center">
                            <h4 class="h4 pb-0 mb-0">Protection</h4>
                            <p>It’s nice to have a safety net – that’s why we’ve got insurance available to help you do tasks with peace of mind.</p>

                            <div class="row py-3">
                                <div class="col-lg-1">
                                    <span class="icon-black-tie icomoon"></span>
                                </div>
                                <div class="col-lg-11">
                                    <h6>Public Liability*</h6>
                                    <p>Certain tasks done through Airtasker are covered by third party public liability insurance (provided by CGU).</p>
                                </div>
                            </div>

                            <div class="row py-3">
                                <div class="col-lg-1">
                                    <span class="icon-thumbs-o-up icomoon"></span>
                                </div>
                                <div class="col-lg-11">
                                    <h6>Injury*</h6>
                                    <p>Just in case something happens while you’re doing a task</p>
                                </div>
                            </div>

                            <div class="row py-3 mb-3">
                                <div class="col-lg-1">
                                    <span class="icon-tag icomoon"></span>
                                </div>
                                <div class="col-lg-11">
                                    <h6>Income Protection*</h6>
                                    <p>Keep earning even when you can’t! This optional plan (provided by Roobyx) is available. Please contact Roobyx directly for further details.</p>

                                    <small>*subject to terms and conditions, excess and exclusions applies.</small>
                                </div>
                            </div>

                            <div class="row py-3">
                                <div class="col-lg-12">
                                    <!-- div buttons -->
                                    <div class="pb-4">                       
                                        <p class="text-center">
                                            <a href="javascript:void(0)" class="bluebtnlg">Learn More</a> 
                                        </p>
                                    </div>
                                    <!--/ div buttons -->
                                </div>                         
                            </div>

                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->


                </div>
                <!--/ container -->
            </div>
            <!--/ last features -->

     
    
      

    
  </main>
  <!--/ main -->
  <?php include 'footer.php' ?>
  <?php include 'scripts.php' ?> 

</body>
</html>