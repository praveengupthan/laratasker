<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Account Dashboard</title>
  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header-postlogin.php' ?>

  <!-- main -->
  <main class="subpage usersubpage">
    <!--user container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- left navigation -->
            <div class="col-lg-3 leftnavigation">
              <?php include 'user-leftnav.php' ?>
            </div>
            <!--/ left navigatin -->

            <!-- right profile -->
            <div class="col-lg-9">
                <!-- right user panel-->
                <div class="right-user-panel">
                    <h1 class="h5 title-page">Account</h1>

                    <!-- row -->
                    <div class="row justify-content-between">
                        <!-- left col -->
                        <div class="col-lg-6">
                            <!-- upload avatar -->
                            <div class="upload-avatar">
                                <h6 class="h6 flight">Upload Avatar</h6>
                                <div class="d-flex">
                                    <img src="img/data/tasker07.jpg" alt="" class="avatar-thumb">

                                    <div class="inputfile pl-3 align-self-center">
                                        <input type="file" title="Upload Photo" class="pinkbtn">
                                    </div>
                                </div>
                            </div>
                            <!--/ upload avatar -->

                            <!-- profile image -->
                            <div class="profile-headerbg">
                            <label>Profile Image</label>
                                <img src="img/header-profile-noimage.png" alt="" class="img-fluid">
                                <a href="javascript:void(0)">Jazz up your public profile with a custom header image</a>
                            </div>
                            <!--/ profile image -->

                            <!-- form -->
                            <form class="custom-form">
                              <div class="form-group">
                                <label>First name</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" value="Praveen">
                                </div>
                              </div>

                              <div class="form-group">
                                <label>Last name</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" value="Guptha">
                                </div>
                              </div>

                              <div class="form-group">
                                <label>Tag Line</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Mini Bio">
                                </div>
                              </div>

                              <div class="form-group">
                                <label>Location</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" value="New Farm QLD, Australia">
                                </div>
                              </div>

                              <div class="form-group">
                                <label>Email</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" value="Praveennandipati@gmail.com">
                                </div>
                              </div>

                              <div class="form-group">
                                <label>Birthday</label>
                                <div class="input-group">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <input type="text" class="form-control text-center" placeholder="DD">
                                        </div>
                                        <div class="col-lg-3">
                                            <input type="text" class="form-control text-center" placeholder="MM">
                                        </div>
                                        <div class="col-lg-3">
                                            <input type="text" class="form-control text-center" placeholder="YYYY">
                                        </div>
                                    </div>
                                </div>
                              </div>

                              
                              <div class="form-group">
                                <label>ABN</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="ABN">
                                </div>
                              </div>

                              <div class="form-group">
                                <label>Description</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Description">
                                </div>
                              </div>

                              
                                <p class="pb-1">On Laratasker I want to</p>
                                <label class="container-form pr-3 d-inline-block">Post Tasks
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container-form  d-inline-block">Earn Money
                                    <input type="checkbox" checked="checked">
                                    <span class="checkmark"></span>
                                </label>                               
                             

                              <div class="form-group">
                                <button class="bluebtnlg">Save Profile</button>
                                <button class="pinkbtnlg">Deactive my Account</button>
                              </div>  

                            </form>
                            <!--/ form -->                            
                        </div>
                        <!--/ left col -->

                        <!-- right col -->
                        <div class="col-lg-4">
                            <!-- status bar -->
                            <div class="status-bar">
                                <small class="fgray text-uppercase">Your Profile  is 75% Completed</small>
                                <div class="bar">
                                    <div class="barin" style="width:45%"></div>
                                </div>
                            </div>
                            <!--/ status bar -->

                            <a class="bluebtnlg" href="javascript:void(0)">
                                  View Your Public Profile
                            </a>
                        </div>
                        <!--/ right col -->
                    </div>
                    <!--/ row -->                  

                 
                </div>
                <!--/ right user panel -->
            </div>
            <!--/ right profile -->
        </div>
        <!--/ row -->
    </div>
    <!--/ user container -->
  </main>
  <!--/ main -->

  <?php include 'scripts.php' ?> 
</body>
</html>