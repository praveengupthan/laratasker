<!-- scripts -->
  <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>  
  <script src="js/bootstrap.min.js"></script>
  <script src="js/bootnavbar.js"></script>
  <script>
        $(function () {
            $('.main_navbar').bootnavbar();
        })
  </script>   
  <script src="js/jquery.easyResponsiveTabs.js"></script>

  



  <script src="js/slick.js"></script>
  <!--[if lt IE 9]>
    <script src="js/html5-shiv.js"></script>
    <![endif ]-->    

  <script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script> <!-- date picker -->
  <script src="js/bootstrap.file-input.js"></script>
  <script src="js/tagsinput.js"></script>
    <!-- jquery steps -->
    <!-- <script src="js/jquery-steps.js"></script>  -->
  <!--/ jquery steps ends -->
  <script src="js/custom.js"></script>
  <!--/ scripts -->
  <script src="js/jquery.steps.js"></script>

  <!-- grid gallery -->
  <script src="js/baguetteBox.min.js"></script>
  <script>
      baguetteBox.run('.grid-gallery', { animation: 'slideIn'});
  </script>

  
  

<script>

  //datepicker
$('.datepicker').datepicker({
    showOtherMonths: true
});

//input
$(document).ready(function(){
  $('input[type=file]').bootstrapFileInput();
});
</script>
  
<script src="js/accordian.js"></script>
