<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Help Detail</title>

  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header.php' ?>

  <!-- main -->
  <main class="subpage">
  <!-- container -->
  <div class="container">

    <!-- row -->
    <div class="row py-3">
        <!-- col -->
        <div class="col-lg-6">
            <p class="helpbrlinks">
                <a href="help.php">Laratasker Help Center </a> >
                <a href="javascript:void(0)">Payments </a> >
                <a href="javascript:void(0)">Understanding payments </a>
            </p>
        </div>
        <!--/ col -->
    </div>
    <!-- row -->

    <!-- row -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="h2">Payments</h1>

            <div class="accordion cust-accord">

                <h3 class="panel-title">What is Airtasker Pay?</h3>
                <div class="panel-content">
                    <p>Airtasker Pay is our payment system that ensures payment protection for Taskers and a seamless, secure way to get local tasks completed for Posters.</p>
                    <p>With Airtasker Pay, both Posters and Taskers can be confident that the money has been committed to the task, and that it will only be released when both sides are happy that the task is complete. </p>
                    <p>The steps below outline the Airtasker payment flow under this system:</p>
                </div>

                <h3 class="panel-title">How can I increase the price once the task is assigned?</h3>
                <div class="panel-content">
                    <p>Realised there’s more to be done or the task is more complicated than initially thought? Not to worry, increasing the task price is simple.</p>
                    <p>Once an offer has been accepted, either the Poster or Tasker can increase the task price, using the steps below.</p>
                    <p>The price can only be increased 3 times(to an overall maximum of 500 in your local currency) and the service fee is applied to all price increases.</p>
                    <p>This feature is currently only available to increase the price. Should you need to decrease the task price, first have a chat with the other party through private messaging. Once you’ve agreed on a new price, contact us to assist. For more info on decreasing the price, see here.</p>
                </div>

                <h3 class="panel-title">How do I decrease the price?</h3>
                <div class="panel-content">
                    <p>Decreasing the price can only be done if the task funds are still held in escrow. If the payment has already been released, Airtasker no longer has access to it, so Posters and Taskers will need to organise adjustments directly.</p>
                    <p>If payment hasn't yet been released yet and you would like to decrease the task price, you can do so by following these steps:</p>
                    <ol>
                        <li class="p1">Discuss with the other party about decreasing the task price and come to an agreement.</li>
                        <li class="p1"><span style="font-weight: 400;">Both parties must agree to decrease the price via Airtasker private messages and explicitly state the new amount.</span></li>
                        <li class="p1">Contact Airtasker (<a href="https://support.airtasker.com/hc/en-au/articles/213536878-How-can-I-contact-Airtasker-">click here</a>) once both parties have written that they agree to a decrease in the task price and stated what the new amount should be.</li>
                        <li class="p1">Airtasker will adjust the price on the system accordingly and <a href="/hc/en-au/articles/221269248" target="_self">return</a> the extra amount to the Poster.</li>
                    </ol>
                    <p>The price can only be increased 3 times(to an overall maximum of 500 in your local currency) and the service fee is applied to all price increases.</p>
                    <p>This feature is currently only available to increase the price. Should you need to decrease the task price, first have a chat with the other party through private messaging. Once you’ve agreed on a new price, contact us to assist. For more info on decreasing the price, see here.</p>
                    <p>Please note that we won't be able to help adjust the task price until both parties have confirmed that they're happy to lower the task price and explicitly stated the new amount. The agreed amount should be inclusive of Airtasker fees to avoid confusion and must be rounded to the nearest full number.</p>
                </div>

                <h3 class="panel-title">I don't agree with an increase price request, what do I do?</h3>
                <div class="panel-content">
                    <p>Both members have the option to increase the task price after being assigned. When a Tasker uses this feature, a request will be sent to you (the Poster) with details about the request.</p>
                    <p>If you don’t agree with the request to increase the task price, you can either decline it, or discuss the request with the Tasker. The request will not be accepted without your express permission.</p>
                    
                    <p>If you’re not sure about a Tasker’s request for an increase in payment, first send them through a private message to ask for more information. Taskers are generally friendly and open to negotiation so it’s definitely worth chatting with them to come to an agreement.</p>
                    <p>You can also choose to decline the request. This can be done in the following steps:</p>
                    <p>If you can't come to an agreement about the final price of a task, Airtasker can help. Check out our range of Dispute Resolution articles for more information here.</p>
                </div>

                
                <h3 class="panel-title">How do I check my payment history?</h3>
                <div class="panel-content">
                    <p>Your payment history will have information about all incoming and outgoing payments through Airtasker. You can easily check this through the following steps:</p>
                    <ol>
                        <li class="p1">Log into Airtasker.</li>
                        <li class="p1">Click the <em>Payment History&nbsp;</em>tab in your account. You can find this under the <em>More</em>&nbsp;<em>Options</em> menu in the app, or by clicking your profile picture in the mobile or web browser</li>
                        <li class="p1">Select on <em>Earned</em> or <em>Outgoing</em> depending on which payment history you'd like to view</li>
                    </ol>
                    
                    <p>You'll then be able to see a list of all the relevant transactions, including the date, task details and a breakdown of the price.</p>
                    <p>You can also choose to decline the request. This can be done in the following steps:</p>
                    <p>You can also download a CSV file of all of your payments by clicking on Download (which can be found below the net earned or net outgoing amount).</p>
                </div>

                <h3 class="panel-title">What are bonuses?</h3>
                <div class="panel-content">
                    <p>When a Tasker does a great job on a task, you can give them a little bit extra to say thanks!</p>
                    
                    <p>We've created the bonus feature on our platform to help you easily do this. You will see this option when you’re releasing the payment to the Tasker.</p>
                    <p>You’ll be shown the bonus options available when you click Release Payment. At the moment, bonuses can only be added in pre-set amounts, and you can't increase the total task value to over our maximum of 9,999 (in your local currency).</p>
                    <p>If you don’t want to add a bonus, that’s okay too! On the desktop site, just stay on the pre-selected option of I’d prefer not to and proceed to release payment. On the app, you can simply release payment without selecting any of the bonus amounts. </p>
                    <p>Keep in mind that all payments through Airtasker have a service fee deducted, including bonuses. </p>
                </div>

                <h3 class="panel-title">How do coupons work on Airtasker?</h3>
                <div class="panel-content">
                    <p>Coupons are unique codes that give you a discount on tasks posted on Airtasker. Coupons can be distributed in a number of ways, including as part of promotions, or as prizes for competitions on our Facebook page.</p>
                    <p>Coupons have specific requirements that need to be met so you can redeem their value. Check for the following:</p>
                    <p>If the requirements are met and your coupon is valid, you can use it for a discount on a task. You can pay any extra amount on the task with the saved payment method on your account.</p>
                    <p>Please keep in mind that you can only use one coupon per task.</p>
                    <p>Coupons are redeemed when you accept an offer on a task. On the payment screen before you assign a Tasker, there will be a button that says Add a coupon under your payment method. Enter your code here before clicking assign. Check out this process in action below:</p>
                </div>

                <h3 class="panel-title">What are referrals? How do they work?</h3>
                <div class="panel-content">
                    <p>When you become a member of Airtasker you get your own unique referral code to share with friends and family. If someone signs up using your code, they’ll get $25 towards their first task. And once their task is completed, you’ll get $10 credit yourself!</p>
                    <p>The easiest way to share the code is on your social media accounts. That way you have the best chance of spreading the Airtasker love (and credit) to people you know.</p>
                    <p>Although the code doesn't expire, it can only be used by a maximum of 15 people, so do keep that in mind. If you're having trouble with any referral matters, check out our helpful article here.</p>
                    <p>If you've signed up using someone's referral code, you'll get the $25 added to your account to use on your first task (to the value of $100 or more). This will be automatically added to your account and automatically applied when you assign your first task.</p>
                    <p>For any issues with referrals, see our helpful article here.</p>
                </div>

                <h3 class="panel-title">The task is complete, can I still increase the task price?</h3>
                <div class="panel-content">
                    <p>If the Tasker has requested payment already but you need to increase the price, you can still do so.</p>
                    <p>Just so you know, once payment has been released you're no longer able to change the price. </p>
                    <p>You can also choose to increase the payment using the bonus feature, which you can find out more about here.</p>
                </div>


            </div>


        </div>
    </div>
    <!--/ row -->



   </div>
   <!--/ container -->  
    
  </main>
  <!--/ main -->
  <?php include 'footer.php' ?>
  <?php include 'scripts.php' ?> 

</body>
</html>