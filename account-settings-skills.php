<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Account Skills</title>
  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header-postlogin.php' ?>

  <!-- main -->
  <main class="subpage usersubpage">
    <!--user container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- left navigation -->
            <div class="col-lg-3 leftnavigation">
              <?php include 'user-leftnav.php' ?>
            </div>
            <!--/ left navigatin -->

            <!-- right profile -->
            <div class="col-lg-9">
                <!-- right user panel-->
                <div class="right-user-panel">
                    <h1 class="h5 title-page">Skills</h1>
                    <!-- row -->
                    <div class="row justify-content-end">
                      <!-- right col -->
                      <div class="col-lg-4">
                            <!-- status bar -->
                            <div class="status-bar">
                                <small class="fgray text-uppercase">Your Skills  is 45% Completed</small>
                                <div class="bar">
                                    <div class="barin" style="width:45%"></div>
                                </div>
                            </div>
                            <!--/ status bar -->                           
                        </div>
                        <!--/ right col -->
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row">
                      <!-- col -->
                      <div class="col-lg-12">
                          <p>These are your skills. Keep them updated with any new skills you learn so other members can know what you can offer.</p>
                     
                          <!-- form -->
                          <form class="custom-form">

                            <div class="form-group">
                                <label>What are you good at?</label>
                                <div class="input-group">
                                    <input type="text" data-role="tagsinput" value="jQuery,Script,Net">
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="d-block">How do you get around?</label>
                                  <label class="container-form mx-2 d-inline-block">Bicycle
                                      <input type="checkbox">
                                      <span class="checkmark"></span>
                                  </label>
                                  <label class="container-form mx-2  d-inline-block">Car
                                      <input type="checkbox" checked="checked">
                                      <span class="checkmark"></span>
                                  </label>       
                                  <label class="container-form mx-2  d-inline-block">Online
                                      <input type="checkbox" checked="">
                                      <span class="checkmark"></span>
                                  </label>     
                                  <label class="container-form mx-2  d-inline-block">Schooter
                                      <input type="checkbox">
                                      <span class="checkmark"></span>
                                  </label>  
                                  <label class="container-form mx-2  d-inline-block">Truck
                                      <input type="checkbox">
                                      <span class="checkmark"></span>
                                  </label>  
                                  <label class="container-form mx-2  d-inline-block">Walk
                                      <input type="checkbox">
                                      <span class="checkmark"></span>
                                  </label>  
                              </div>

                              <div class="form-group ">
                                <label>What languages can you speak/write?</label>
                                <div class="input-group">
                                    <input type="text" data-role="tagsinput" value="English,Telugu,Hindi">
                                </div>
                              </div>

                              <div class="form-group ">
                                <label>What qualifications have you got?</label>
                                <div class="input-group">
                                    <input type="text" data-role="tagsinput" value="Graduation,Post Graduation,Phd">
                                </div>
                              </div>

                              <div class="form-group ">
                                <label>What's your work experience?</label>
                                <div class="input-group">
                                    <input type="text" data-role="tagsinput" value="" placeholder="Eg: 3years as a Barista at the Cafe">
                                </div>
                              </div>

                              <div class="form-group">
                                <button class="bluebtnlg">Save Skills</button>
                              </div>
                          </form>
                          <!--/ form -->
                        </div>
                      <!--/ col -->                     
                    </div>
                    <!--/ row --> 
                </div>
                <!--/ right user panel -->
            </div>
            <!--/ right profile -->
        </div>
        <!--/ row -->
    </div>
    <!--/ user container -->
  </main>
  <!--/ main -->

  <?php include 'scripts.php' ?> 
</body>
</html>