<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Portfolio</title>
  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header-postlogin.php' ?>

  <!-- main -->
  <main class="subpage">
    <!--user container -->
    <div class="container">
        <!-- row -->
        <div class="row">
          <!-- col -->
          <div class="col-lg-12">
              <!-- main profile row -->
              <div class="main-profilerow">
                <!-- banner image -->
                <figure class="profile-banner">
                    <a href="javascript:void(0)" class="edit-profile-icon" id="edit-profile-banner-icon">
                        <span class="icon-pencil icomoon"></span>
                    </a>

                    <!-- image upload -->
                    <div class="image-upload">
                        <label for="file-input" class="fileinput">
                            <span class="icon-upload icomoon"></span>
                        </label>
                        <input id="file-input" type="file"/>
                    </div>
                    <!--/ image upload -->

                    <img src="img/profile-bg-dummy.png" class="img-fluid w-100" alt="">
                </figure>
                <!-- /banner image -->

                <!-- profile body -->
                <div class="profile-body">
                  <!-- row user basic info -->
                  <div class="row">
                      <!-- col -->
                      <div class="col-lg-6">
                          <figure class="profile-thumb">                           
                              <img src="img/data/tasker04.jpg" alt="">

                               <!-- image upload -->
                              <div class="image-upload">
                                  <label for="file-input" class="fileinput">
                                      <span class="icon-upload icomoon"></span>
                                  </label>
                                  <input id="file-input" type="file"/>
                              </div>
                              <!--/ image upload -->
                          </figure>  
                          <article id="article-userprimary">
                              <h6 class="h6">Praveen Guptha N.</h6>
                              <p class="pb-0">Last online 22 mins ago</p>
                              <p class="pb-0"><span class="icon-pin icomoon"></span> New Farm QLD, Australia</p>
                              <p class="pb-0">Member since 31st Jan 2020</p>
                          </article>
                      </div>
                      <!--/ col -->
                      <!-- col -->
                      <div class="col-lg-12" id="edit-profile-basic">
                          <form class="custom-form">
                            <!-- row -->
                              <div class="row">
                                  <!-- col -->
                                  <div class="col-lg-6">
                                      <div class="form-group">
                                          <label>First name</label>
                                          <div class="input-group">
                                              <input type="text" value="Praveen" class="form-control">
                                          </div>
                                      </div>
                                  </div>
                                  <!--/ col -->
                                   <!-- col -->
                                   <div class="col-lg-6">
                                      <div class="form-group">
                                          <label>Last name</label>
                                          <div class="input-group">
                                              <input type="text" value="Kumar" class="form-control">
                                          </div>
                                      </div>
                                  </div>
                                  <!--/ col -->

                                  <!-- col -->
                                  <div class="col-lg-12">
                                      <a class="pinkbtnlg" href="javascript:void(0)">Cancel</a>
                                      <a class="bluebtnlg" href="javascript:void(0)" id="saveprofile-basic-data">Save</a>
                                  </div>
                                  <!--/ col -->
                              </div>
                              <!-- row -->                         
                          </form>
                      </div>
                      <!--/ col -->
                    </div>
                    <!--/ row user basic info -->

                  <!-- badges row and about section -->
                  <div class="row pt-4">
                      <!-- col for badges -->
                      <div class="col-lg-4">
                          <h5 class="h6 text-uppercase">Badges</h5>

                          <div class="badge-div py-md-3">
                              <p class="flight text-uppercase pb-3 small"><i>ISSUED</i></p>
                              <a href="javascript:void(0)" class="whitebtn">Get a Badge</a>
                          </div>

                          <div class="badge-div py-md-3">
                              <p class="flight text-uppercase pb-1 small"><i>ID Badges</i></p>
                              <p>Reassure members you are who you say.</p>
                              <a href="javascript:void(0)" class="whitebtn">Get a Badge</a>
                          </div>

                          
                          <div class="badge-div py-md-3">
                              <p class="flight text-uppercase pb-1 small"><i>LICENCE BADGES</i></p>
                              <p>Show off your hard-earned qualifications and licences</p>
                              <a href="javascript:void(0)" class="whitebtn">Get a Badge</a>
                          </div>

                          <div class="badge-div py-md-3">
                              <p class="flight text-uppercase pb-1 small"><i>PARTNERSHIP BADGES</i></p>
                              <p>Get exclusive access to partnership tasks.</p>
                              <a href="javascript:void(0)" class="whitebtn">Get a Badge</a>
                          </div>

                      </div>
                      <!--/ col for badges -->      

                      <!-- col for edit profile -->
                      <div class="col-lg-8">

                          <!-- right profile section -->
                          <div class="right-profile-section">
                              <h5 class="h6 text-uppercase">About</h5>
                              <!-- static data -->
                              <div class="static-data" id="static-data-about">
                                  <a href="javascript:void(0)" class="edit-profile-icon" id="edit-profile-icon">
                                      <span class="icon-pencil icomoon"></span>
                                  </a>
                                  <p class="flight text-uppercase pb-1 small"><i>Tag line name will be here</i></p>
                                  <p>Edit your description now.</p>
                              </div>
                              <!--/ static data -->

                              <!-- dynamic data -->
                              <div class="dynamic-data" id="edit-description">
                                  <form class="custom-form">
                                      <div class="form-group">
                                        <label>Tagline</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" value="Praveen">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label>Description</label>
                                        <div class="input-group">
                                            <textarea class="form-control" id="describe-user" placeholder="Description" style="height:120px;" ></textarea>
                                        </div>
                                      </div>

                                      <!-- col -->
                                      <div>
                                          <a class="pinkbtnlg" href="javascript:void(0)">Cancel</a>
                                          <a class="bluebtnlg" href="javascript:void(0)" id="saveprofile-about-data">Save</a>
                                      </div>
                                      <!--/ col -->

                                  </form>
                              </div>
                              <!--/ dynamic data -->
                          </div>
                          <!-- /right profile section -->


                           <!-- right profile section -->
                           <div class="right-profile-section">
                              <h5 class="h6 text-uppercase">Portfolio</h5>
                              <!-- static data -->
                              <div class="static-data" id="static-data-portfolio">
                                  <a href="javascript:void(0)" class="edit-profile-icon" id="edit-portfolio-icon">
                                      <span class="icon-pencil icomoon"></span>
                                  </a>

                                  <figure class="figure-portfolio d-flex">
                                      <div class="figure-portfolio-img">
                                          <img src="img/data/it-comp.jpg" class="img-fluid">
                                      </div>                                     
                                  </figure>
                                  
                              </div>
                              <!--/ static data -->

                              <!-- dynamic data -->
                              <div class="dynamic-data pr-3" id="dynamic-data-portfolio">
                                  <p>Add items to your portfolio. Upload a maximum of 30 items. File formats accepted include JPG/PNG/PDF/TXT and must not be larger than 5MB.</p>

                                  <figure class="figure-portfolio d-flex">
                                      <div class="figure-portfolio-img">
                                          <img src="img/data/it-comp.jpg" class="img-fluid">
                                          <a href="javascript:void(0)" class="delete-img"><span class="icon-trash-o icomoon"></span></a>
                                      </div>

                                      <div class="figure-portfolio-img upload-figure">
                                         <!-- image upload -->
                                          <div class="image-upload">
                                              <label for="file-input" class="fileinput">
                                                  <span class="icon-plus-circle icomoon"></span>
                                              </label>
                                              <input id="file-input" type="file"/>
                                          </div>
                                          <!--/ image upload -->
                                      </div>
                                  </figure>
                                  <!-- col -->
                                  <div>                                         
                                      <a class="bluebtnlg" href="javascript:void(0)" id="save-portfolio-btn">Save</a>
                                  </div>
                                  <!--/ col -->

                              </div>
                              <!--/ dynamic data -->
                          </div>
                          <!-- /right profile section -->

                           <!-- right profile section -->
                           <div class="right-profile-section">
                              <h5 class="h6 text-uppercase">Skills</h5>
                              <!-- static data -->
                              <div class="static-data" id="static-data-skills">
                                  <a href="javascript:void(0)" class="edit-profile-icon" id="edit-skills-btn">
                                      <span class="icon-pencil icomoon"></span>
                                  </a>
                                  <p>Specialities</p>
                                  <div class="tags">
                                      <span class="label label-default small">
                                          <i>Graphic Designing</i>
                                      </span>
                                      <span class="label label-default small">
                                          <i>3d Animations</i>
                                      </span>
                                      <span class="label label-default small">
                                          <i>Web Designing</i>
                                      </span>
                                      <span class="label label-default small">
                                          <i>Web Development</i>
                                      </span>
                                  </div>
                              </div>
                              <!--/ static data -->

                              <!-- dynamic data -->
                              <div class="dynamic-data" id="edit-skills-dynamic">

                                <form class="custom-form">
                                  <!-- form group -->
                                  <div class="form-group">
                                    <label>EDUCATION</label>
                                    <div class="input-group">
                                        <input type="text" data-role="tagsinput" value="MCA">
                                    </div>
                                  </div>
                                  <!--/ form group -->

                                   <!-- form group -->
                                   <div class="form-group">
                                    <label>SPECIALITIES</label>
                                    <div class="input-group">
                                        <input type="text" data-role="tagsinput" value="Web Designing">
                                    </div>
                                  </div>
                                  <!--/ form group -->

                                    <!-- form group -->
                                    <div class="form-group">
                                    <label>LANGUAGES</label>
                                    <div class="input-group">
                                        <input type="text" data-role="tagsinput" value="Telugu">
                                    </div>
                                  </div>
                                  <!--/ form group -->

                                   <!-- form group -->
                                   <div class="form-group">
                                    <label>Work</label>
                                    <div class="input-group">
                                        <input type="text" data-role="tagsinput" value="WORK">
                                    </div>
                                  </div>
                                  <!--/ form group -->

                                  <div class="form-group">
                                      <label class="d-block">TRANSPORTATION</label>
                                      <label class="container-form mx-2 d-inline-block">Bicycle
                                          <input type="checkbox">
                                          <span class="checkmark"></span>
                                      </label>
                                      <label class="container-form mx-2  d-inline-block">Car
                                          <input type="checkbox" checked="checked">
                                          <span class="checkmark"></span>
                                      </label>       
                                      <label class="container-form mx-2  d-inline-block">Online
                                          <input type="checkbox" checked="">
                                          <span class="checkmark"></span>
                                      </label>     
                                      <label class="container-form mx-2  d-inline-block">Schooter
                                          <input type="checkbox">
                                          <span class="checkmark"></span>
                                      </label>  
                                      <label class="container-form mx-2  d-inline-block">Truck
                                          <input type="checkbox">
                                          <span class="checkmark"></span>
                                      </label>  
                                      <label class="container-form mx-2  d-inline-block">Walk
                                          <input type="checkbox">
                                          <span class="checkmark"></span>
                                      </label>  
                                  </div>
                                </form>

                                 <!-- col -->
                                 <div>                                         
                                      <a class="bluebtnlg" href="javascript:void(0)" id="save-skills-btn">Save</a>
                                  </div>
                                  <!--/ col -->

                              </div>
                              <!--/ dynamic data -->
                          </div>
                          <!-- /right profile section -->

                           <!-- right profile section -->
                           <div class="right-profile-section">
                              <h5 class="h6 text-uppercase">Reviews</h5>
                              <!-- static data -->
                              <div class="static-data">

                                <!-- tab -->
                                <div class="custom-tab primarytab">                                    
                                    <ul class="nav nav-pills" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="Earned-tab" data-toggle="tab" href="#earned" role="tab" aria-controls="home" aria-selected="true">As a Tasker</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="outgoing-tab" data-toggle="tab" href="#outgoing" role="tab" aria-controls="profile" aria-selected="false">As a Poster</a>
                                        </li>                           
                                    </ul>

                                    <div class="tab-content pt-3" id="myTabContent">
                                        <!-- As a Tasker -->
                                        <div class="tab-pane fade show active" id="earned" role="tabpanel" aria-labelledby="Earned-tab">

                                            <p class="pb-0">
                                              <small>Looks like you haven’t received any reviews just yet.</small>
                                            </p>
                                            <p>
                                              <a href="javascript:void(0)" class="fblue small"><i>Let’s browse available tasks.</i></a>
                                            </p>
                                            
                                        </div>
                                        <!--/ As a Tasker -->

                                        <!-- As a Poster -->
                                        <div class="tab-pane fade" id="outgoing" role="tabpanel" aria-labelledby="outgoing-tab">
                                        
                                            <p class="pb-0">
                                              <small>Looks like you haven’t received any reviews just yet.</small>
                                            </p>
                                            <p>
                                              <a href="javascript:void(0)" class="fblue small"><i>Let’s browse available tasks.</i></a>
                                            </p>

                                        </div>
                                        <!--/ As a Poster-->
                                    </div>
                                </div>
                                <!--/ tab -->                                 
                              </div>
                              <!--/ static data -->                              
                          </div>
                          <!-- /right profile section -->

                      </div>
                      <!--/ col for edit profile -->  
                  </div>
                  <!--/ badges row and about section -->                  
                </div>
                <!-- /profile body -->
              </div>
              <!-- /main profile row -->
          </div>
          <!--/ col -->
        </div>
        <!--/ row -->
    </div>
    <!--/ user container -->
  </main>
  <!--/ main -->
  <?php include 'scripts.php' ?> 
</body>
</html>