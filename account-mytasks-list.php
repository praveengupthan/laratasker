<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Account Dashboard</title>
  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header-postlogin.php' ?>

  <!-- main -->
  <main class="subpage usersubpage">
    <!--user container -->
    <div class="container">
         <!-- row -->
         <div class="row taskrow">
            <!-- left col -->
            <div class="col-lg-4">               
                <!-- left task -->
                <div class="left-task">
                   <h6 class="h6 small text-uppercase fbold">Draft Tasks</h6>
                    <!-- request col -->
                    <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Proof Reading</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 3 December, 2019</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker01.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2 expired-request">
                        <h2 class="d-flex justify-content-between">
                            <span>Elk Stack Designer</span>
                            <span class="fbold fred price">$ 25</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 10 Feb</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker02.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 fbold">
                        <span class="small fbold">Closed</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>I need drafting</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Sat, 15 Feb</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker03.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>I need help writing a management report</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Sun, 2 Feb</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker04.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                    <h6 class="h6 small text-uppercase fbold py-2">Completed Tasks</h6>

                     <!-- request col -->
                     <div class="request-col mb-2 expired-request">
                        <h2 class="d-flex justify-content-between">
                            <span>Need to broadcast a post to FB and Insta</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 3 December, 2019</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker05.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fred small fbold">Closed</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>I need lawn mowing services</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Thu, 6 Feb</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker06.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Deck sanded</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Sun, 1 Mar</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker07.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Remove strobing from mov files</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 3 December, 2019</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker08.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fblue small fbold">Assigned .</span>     
                            <span class="small">11 Offers</span>                      
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Graphic Designer - Logo</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 3 December, 2019</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker09.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Proof Reading</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 3 December, 2019</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker10.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fblue small fbold">Assigned .</span>     
                            <span class="small">11 Offers</span>                      
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Airtable - Hlep and databse Build</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 3 December, 2019</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker01.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Proof Reading</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 3 December, 2019</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker02.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                    <!-- div buttons -->
                    <div class="pb-4">
                        <p class="text-center pb-3">To See more taks</p>
                        <p class="text-center">
                            <a href="javascript:void(0)" class="bluebtnlg">Join Laratasker</a>
                            <span>Or</span>
                            <a href="javascript:void(0)" class="pinkbtnlg">Log in</a>
                        </p>
                    </div>
                    <!--/ div buttons -->

                </div>
                <!--/ left task -->
            </div>
            <!--/ left col -->

            <!-- right col -->
            <div class="col-lg-8">
                <div class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d15220.162064645885!2d78.41834594999999!3d17.50557925!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1572786330386!5m2!1sen!2sin" width="100%"; frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>
            </div>
            <!--/ right col -->
        </div>
        <!--/ row -->
    </div>
    <!--/ user container -->
  </main>
  <!--/ main -->

  <?php include 'scripts.php' ?> 
</body>
</html>