<!-- footer -->
  <footer>
    <!-- container -->
    <div class="container">
      <!-- row -->
      <div class="row">
        <!-- col -->
        <div class="col-lg-3">
          <h5 class="h5">Discover</h5>
          <ul>
            <li><a href="javascript:void(0)">How it works</a></li>
            <li><a href="javascript:void(0)">Laratasker for business</a></li>
            <li><a href="javascript:void(0)">Earn money</a></li>
            <li><a href="javascript:void(0)">New users FAQ</a></li>
            <li><a href="javascript:void(0)">Find work</a></li>
          </ul>
        </div>
        <!--/ col -->

        <!-- col -->
        <div class="col-lg-3">
          <h5 class="h5">Company</h5>
          <ul>
            <li><a href="javascript:void(0)">About us</a></li>
            <li><a href="javascript:void(0)">Careers</a></li>
            <li><a href="javascript:void(0)">Community guidelines</a></li>
            <li><a href="javascript:void(0)">Terms & conditions</a></li>
            <li><a href="javascript:void(0)">Blog</a></li>
            <li><a href="javascript:void(0)">Contact us</a></li>
            <li><a href="javascript:void(0)">Privacy Policy</a></li>
          </ul>
        </div>
        <!--/ col -->

        <!-- col -->
        <div class="col-lg-3">
          <h5 class="h5">Existing Members</h5>
          <ul>
            <li><a href="javascript:void(0)">Post a task</a></li>
            <li><a href="javascript:void(0)">Browse tasks</a></li>
            <li><a href="javascript:void(0)">Login</a></li>
            <li><a href="javascript:void(0)">Support centre</a></li>
            <li><a href="javascript:void(0)">Merchandise</a></li>
          </ul>
        </div>
        <!--/ col -->

        <!-- col -->
        <div class="col-lg-3">
          <h5 class="h5">Popular Categories</h5>
          <ul>
            <li><a href="javascript:void(0)">Handyman Services</a></li>
            <li><a href="javascript:void(0)">Cleaning Services</a></li>
            <li><a href="javascript:void(0)">Delivery Services</a></li>
            <li><a href="javascript:void(0)">Removalists</a></li>
            <li><a href="javascript:void(0)">Gardening Services</a></li>
            <li><a href="javascript:void(0)">Automotive</a></li>
            <li><a href="javascript:void(0)">Assembly Services</a></li>
            <li><a href="javascript:void(0)">All Services</a></li>
          </ul>
        </div>
        <!--/ col -->
      </div>
      <!--/ row -->
    </div>
    <!--/ container -->

    <!-- footer bottom -->
    <div class="bottom-footer">
      <!-- container -->
      <div class="container">
        <!-- row -->
        <div class="row">
          <!-- col -->
          <div class="col-lg-6 socialcol">
            <a href="javascript:void(0)">
              <span class="icon-facebook icomoon"></span>
            </a>
            <a href="javascript:void(0)">
              <span class="icon-twitter icomoon"></span>
            </a>
            <a href="javascript:void(0)">
              <span class="icon-linkedin icomoon"></span>
            </a>
            <a href="javascript:void(0)">
              <span class="icon-instagram icomoon"></span>
            </a>
          </div>
          <!--/ col -->
          <!-- col -->
          <div class="col-lg-6 rightcol align-self-center">
            <p><span class="icon-logo icomoon"></span> Pty. Ltd 2011-2020©, All rights reserved</p>
          </div>
          <!--/ col -->
        </div>
        <!--/ row -->
      </div>
      <!--/ container -->
    </div>
    <!--/ footer bottom -->

    <a href="javascript:void(0)" id="movetop" class="movetop">
      <span class="icon-arrow-up icomoon"></span>
    </a>


  </footer>
  <!--/ footer -->

  