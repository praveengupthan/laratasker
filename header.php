  <!-- header -->
  
  <header class="fixed-top">
    <!-- container -->
    <div class="container">
      <!-- nbav bar -->
      <nav class="navbar navbar-expand-lg main_navbar">
        <a class="navbar-brand" href="index.php"><span class="icon-logo"></span></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="icon-align-right icomoon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link pinkbtn" href="#" data-toggle="modal" data-target="#posttask">Post a Task </a>
            </li>

            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                Categories
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <span class="dropdown-menu-arrow"></span>

                    <div class="options-categories">
                      <h5 class="text-center">What are you looking for?</h5>    
                      <p class="text-center">Pick a type of task.</p>

                      <div class="d-flex justify-content-between p-3">
                          <div class="text-center">
                            <p class="small pb-0">I am Looking for work in....</p>
                            <p><button onclick="location.href='category-asatasker.php'" class="pinkbtnlg select-btn" href="javascript:void(0)">As a Tasker</button></p>
                          </div>

                          <div class="text-center">
                            <p class="small pb-0">I Need to hire some for ....</p>
                            <p><button onclick="location.href='category-asaposter.php'" class="bluebtnlg" href="javascript:void(0)">As a Poster</button></p>
                          </div>
                      </div>
                    </div>
                    <div class="drop-in">                      
                        <a class="dropdown-item" href="#">Accountant</a>
                        <a class="dropdown-item" href="#">Admin Assistant</a>
                        <a class="dropdown-item" href="#">Aircon Installation</a>
                        <a class="dropdown-item" href="#">Appliance Repairer</a>
                        <a class="dropdown-item" href="#">Asbestos Removal</a>
                        <a class="dropdown-item" href="#">Assembler</a>
                        <a class="dropdown-item" href="#">Bathroom Renovations</a>
                        <a class="dropdown-item" href="#">Bricklayer</a>
                        <a class="dropdown-item" href="#">Building Services</a>
                        <a class="dropdown-item" href="#">Carpenter</a>
                        <a class="dropdown-item" href="#">Carpet Cleaner</a>
                        <a class="dropdown-item" href="#">Caterer</a>
                        <a class="dropdown-item" href="#">Car Detailing</a>
                        <a class="dropdown-item" href="#">Car Washing</a>
                        <a class="dropdown-item" href="#">Commercial Cleaner</a>
                        <a class="dropdown-item" href="#">Computer Repair</a>
                        <a class="dropdown-item" href="#">Concreter</a>
                        <a class="dropdown-item" href="#">Decking</a>
                        <a class="dropdown-item" href="#">Deliverer</a>
                        <a class="dropdown-item" href="#">Door Installation</a>
                        <a class="dropdown-item" href="#">Drafter</a>
                        <a class="dropdown-item fpink" href="#">View All</a>
                    </div>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="tasks-list.php">Browse a Task</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="howit-works.php">How it works</a>
            </li>
          </ul>

          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" data-toggle="modal" data-target="#signup" href="javascript:void(0)">Sign up</a>
            </li>

            <li class="nav-item">
              <a class="nav-link" data-toggle="modal" data-target="#login" href="javascript:void(0)">Log in</a>
            </li>

            <li class="nav-item">
              <a class="nav-link brdlink " href="#">Become a Tasker</a>
            </li>

          </ul>
        </div>
      </nav>
      <!--/ nav bar -->
    </div>
    <!--/ container -->
  </header>
  <!--/ header -->

  <!-- modal popup for login -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-md modal-dialog-centered" role="document">
    <div class="modal-content  w-75">
      <div class="modal-header">
        <h5 class="modal-title text-center d-block w-100" id="exampleModalLabel">Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body custom-form">
          <!-- form -->
          <form>
              <!-- form group -->
              <div class="form-group">
                  <label>Email</label>
                  <div class="input-group">
                    <input type="text" placeholder="Email" class="form-control">
                  </div>
              </div>
              <!--/ form group -->
              <!-- form group -->
              <div class="form-group">
                  <label>Password</label>
                  <div class="input-group">
                    <input type="password" placeholder="Password" class="form-control">
                  </div>
              </div>
              <!--/ form group -->
              <p class="text-right pb-2">
                <a href="javascript:void(0)" id="forgotpwlink" class="fblack">Forgot Password?</a>
              </p>
              <input type="submit" value="Login" class="w-100 pinkbtn">
          </form>
          <!--/ form -->

          <p class="text-center or py-2"><span>OR</span></p>
          <p class="text-center">Signin with your Social Networks</p>

          <div class="d-flex justify-content-between py-3">
              <a class="socialbtn fbbtn" href="javascript:void(0)">
                  <span class="icon-facebook icomoon"></span> 
                  Facebook
              </a>
              <a class="socialbtn googlesocial" href="javascript:void(0)">
                  <span class="icon-google-plus icomoon"></span> 
                  Google Plus
              </a>
          </div>
      </div>
      <div class="modal-footer d-flex justify-content-between">
          <span>Don't have an account ?</span>
          <a href="javascript:void(0)" id="signuplink" class="fpink">Sign up</a>
      </div>
    </div>
  </div>
</div>
  <!--/ modal popup for login -->

  <!-- modal popup for forgotpassword -->
<div class="modal fade" id="forgotpassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-md modal-dialog-centered" role="document">
    <div class="modal-content  w-75">
      <div class="modal-header">
        <h5 class="modal-title text-center d-block w-100" id="exampleModalLabel">Forgot Password?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body custom-form">
          <!-- form -->
          <form>
            <p>Enter your email below and we will send you instructions on how to reset your password</p>
              <!-- form group -->
              <div class="form-group">
                  <label>Email</label>
                  <div class="input-group">
                    <input type="text" placeholder="Email" class="form-control">
                  </div>
              </div>
              <!--/ form group -->             
             
              <input type="submit" value="Send" class="w-100 pinkbtn">
          </form>
          <!--/ form -->  
      </div>      
    </div>
  </div>
</div>
  <!--/ modal popup for forgot Password -->

   <!-- modal popup for signup -->
<div class="modal fade" id="signup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-md modal-dialog-centered" role="document">
    <div class="modal-content  w-75">
      <div class="modal-header">
        <h5 class="modal-title text-center d-block w-100" id="exampleModalLabel">Join us</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body custom-form">
          <!-- form -->
          <form>
              <!-- form group -->
              <div class="form-group">
                  <label>Email</label>
                  <div class="input-group">
                    <input type="text" placeholder="Email" class="form-control">
                  </div>
              </div>
              <!--/ form group -->
              <!-- form group -->
              <div class="form-group">
                  <label>Password</label>
                  <div class="input-group">
                    <input type="password" placeholder="Password" class="form-control">
                  </div>
              </div>
              <!--/ form group -->
              
              <input type="submit" value="Join Laratasker" class="w-100 pinkbtn">
          </form>
          <!--/ form -->

          <p class="text-center or py-2"><span>OR</span></p>
          <p class="text-center">Signin with your Social Networks</p>

          <div class="d-flex justify-content-between py-3">
              <a class="socialbtn fbbtn" href="javascript:void(0)">
                  <span class="icon-facebook icomoon"></span> 
                  Facebook
              </a>
              <a class="socialbtn googlesocial" href="javascript:void(0)">
                  <span class="icon-google-plus icomoon"></span> 
                  Google Plus
              </a>
          </div>
          <p>
          <span><input type="checkbox"></span>  
          Please don't send me tips or marketing via email or sms.
        </p>
        <p class="py-2">By signing up, I agree to Airtasker's 
          <a href="javascript:void(0)" class="fpink">Terms &amp; Conditions, </a>
          <a href="javascript:void(0)" class="fpink">Community Guidelines, </a>
          <a href="javascript:void(0)" class="fpink">Privacy Policy</a>
        </p>
      </div>
      <div class="modal-footer d-flex justify-content-between">
          <span>Already have an account ?</span>
          <a href="javascript:void(0)" id="loginlink" class="fpink">Log in</a>
      </div>
    </div>
  </div>
</div>
  <!--/ modal popup for signup -->


  <!-- modal popup for Post task -->
<div class="modal fade post-task" id="posttask" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-md modal-dialog-centered" role="document">
    <div class="modal-content ">
      <!--- header -->
      <div class="modal-header">
        <h5 class="modal-title text-center d-block w-100" id="exampleModalLabel">&nbsp;</h5>
        <button id="btn-confirm-exit-postatask" type="button" class="close"  aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!--/ header -->

      <!-- body -->
      <div class="modal-body stepbody">
        <!-- div id wizard-->
        <div id="postTask">
            <!-- SECTION 1 -->
            <h4></h4>
            <section>
                <h5 class="text-center">Start getting offers in no time</h5>                    
                <div class="text-center">
                  <img class="my-4" src="img/onboard.png" alt="" width="150">
                  <p class="text-center">We're just going to ask a few questions to help you find the right Tasker - it'll only take a few minutes!</p>  
                </div>                                   
            </section>
            <!--/ SECTION 1 -->
              
            <!-- SECTION 2 -->
            <h4></h4>
            <section>
                <h5>Tell us what you need done?</h5>

                <div class="form-row mt-2">                  
                  <div class="col-12">     
                    <label>What do you need done?</label>                
                      <input class="multisteps-form__input form-control" type="text" placeholder="e.g. Help move my sofa"/>
                      <small> This'll be the title of your task -</small>
                  </div>
                </div>
                <div class="form-row mt-2">
                  <div class="col-12">
                    <label>What are the details?</label>       
                    <textarea class="form-control" type="text" placeholder="Be as specific as you can about what needs doing" style="height:100px;"></textarea>
                  </div>
                </div>
                
            </section>
            <!-- / SECTION 2-->

            <!-- SECTION 3 -->
            <h4></h4>
            <section>
                <h5>Say where & when</h5>

                <div class="row inpersonrow">
                  <div class="col-12 col-md-6 mt-2">
                    <div class="card shadow-sm">
                      <div class="card-body">
                        <label>
                            <input type="radio">
                              Inperson
                        </label>                         
                        <p class="card-text">Select this if you need the Tasker physically there.</p>
                        <p  class="pt-3"><span class="icon-location h3"></span></p>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6 mt-2">
                    <div class="card shadow-sm">
                      <div class="card-body">
                      <label>
                            <input type="radio">
                              Online
                        </label>   
                        <p class="card-text">Select this if the Tasker can do it from home</p>
                        <p class="pt-3"><span class="icon-mobile h3"></span></p>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-row mt-4">   

                  <div class="col-12">     
                    <label>Enter Suburb</label>                
                      <input class="multisteps-form__input form-control" type="text" placeholder="Enter Suburb"/> 
                  </div>
                  <div class="col-12 pt-3">     
                    <label>When do you need it done?</label>                
                      <input id="datepicker" class="form-control date" type="text" placeholder="Select Date"/> 
                  </div>
                </div>                     
            </section>
            <!--/ Section 3-->

            <!-- SECTION 4 -->
            <h4></h4>
            <section>   
              <h5>Waht is your Budget</h5>     
              <p>Please enter the price you're comfortable to pay to get your task done. Taskers will use this as a guide for how much to offer.</p>
              <p class="py-2">
                <label><input type="radio">Total</label>
                <label><input type="radio">Hourly Rate</label>

                <div class="form-row mt-4">   
                  <div class="col-6">     
                    <label>Enter Value in Rupees</label>                
                      <div class="input-group d-flex">
                        <input class="multisteps-form__input form-control" type="text" placeholder="Rs:150"/> 
                        <span class="d-inline-block p-2"> X </span>
                        <input class="multisteps-form__input form-control" type="text" placeholder="2 Hours"/> 
                      </div>
                  </div>                   
              </div> 

              <div class="form-row mt-4">   
                  <div class="col-12">
                    <div class="bluebg p-3 d-flex justify-content-between">
                        <div>
                          <h5>ESTIMATED BUDGET</h5>
                          <p>Final payment will be agreed later</p>
                        </div>
                        <div class="align-self-center">
                            <h3 class="h3"><span class="icon-inr"></span> 250</h3>
                        </div>
                    </div>
                  </div>
              </div>
              </p>
              
          </section>
          <!-- Section 4-->

            <!-- SECTION 5 -->
            <h4></h4>
            <section>                    
                <div class="text-center">
                <img src="img/mail-success.png" alt="" width="150">
                  <h4 class="h4 py-3">Pick an offer</h4>
                  <p>Now Taskers can ask questions and make offers to do your task - make sure you check back on it regularly!</p> 
                </div>                  
            </section>
            <!-- Section 5-->
          </div>
          <!--/ div id wizard -->
     
      </div>
      <!--/ body -->          
    </div>
  </div>
</div>
  <!--/ modal popup for Post task -->



  <!-- modal popup for exit confirmation of post a task -->
<div class="modal fade" id="task-close-confirmbox" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-md modal-dialog-centered" role="document">
    <div class="modal-content w-75 mx-auto">
      <div class="modal-header">
        <h5 class="modal-title text-center d-block w-100" id="exampleModalLabel">Sorry to see you go...</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body ">         
          <p >Are you sure? You're almost done and it's free to post a task...</p>        
      </div>
      <div class="modal-footer d-flex justify-content-between">
          <a href="javascript:void(0)" data-dismiss="modal"  id="signuplink" class="pinkbtnlg">Continue Task</a>
          <a href="javascript:void(0)" id="discardexitlink" class="bluebtnlg">Discard & Exit</a>
      </div>
    </div>
  </div>
</div>
  <!-- modal popup for exit confirmation of post a task -->




  



