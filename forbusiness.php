<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Lara Tasker for Business</title>

  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header.php' ?>

  <!-- main -->
  <main class="subpage">

      <!-- for business banner -->
      <div class="for-businessheader">
          <!-- container -->
          <div class="container">
              <!-- row -->
              <div class="row">
                  <!-- col-6-->
                  <div class="col-lg-6 align-self-center">
                      <h1 class="h1">Focus on the big picture</h1>
                      <h6 class="h6 pb-3">Get your instant access to a flexible, on demand workforce across Australia with Airtasker for Business.</h6>
                      <a class="pinkbtnlg" href="javascript:void(0)">Post a Task</a>
                  </div>
                  <!-- /col-6-->

                   <!-- col-6-->
                   <div class="col-lg-6 text-center">
                     <img src="img/ceo-transparent.png" alt="" class="img-fluid">
                  </div>
                  <!-- /col-6-->  
              </div>
              <!--/ row -->
          </div>
          <!--/ container -->
      </div>
      <!--/ for busines sbanner -->

      <!---- features -->
    <div class="features home-section">
      <!-- container -->
      <div class="container">
        <!-- row -->
        <div class="row justify-content-lg-center">
          <!-- col -->
          <div class="col-lg-8 text-center">
            <h2 class="pb-2 ">Access to the right people when you need them</h2>            
          </div>
          <!--/ col -->
        </div>
        <!--/ row -->

        <!-- row features -->
        <div class="row pt-5">
          <!-- col -->
          <div class="col-lg-4 text-center feature-col">
            <img src="img/securepayment-icon.jpg" alt="">
            <h5 class="h5">Scalable Workforce</h5>
            <p class="text-center">Whether you need one or 1,000, you can hire as many Taskers as you need, to meet your business demands.</p>
          </div>
          <!--/ col -->
          <!-- col -->
          <div class="col-lg-4 text-center feature-col">
            <img src="img/features-insurance.jpg" alt="">
            <h5 class="h5">Always On</h5>
            <p class="text-center">Need to hire outside of the 9-5? Airtasker provides the extra help when and where you need it."</p>
          </div>
          <!--/ col -->
          <!-- col -->
          <div class="col-lg-4 text-center feature-col">
            <img src="img/verfieid-badges.jpg" alt="">
            <h5 class="h5">Available Australia-wide</h5>
            <p>Access an extensive Tasker network across the country in an instant."</p>
          </div>
          <!--/ col -->
        </div>
        <!--/ row features -->
      </div>
      <!--/ container -->
    </div>
    <!--/ features -->


     <!-- post yur task -->
     <div class="container posturtask-blocks">

        <!-- row -->
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <h1 class="h3">Help with anything you need done</h1>               
            </div>
        </div>
        <!--/ row -->

        <!-- row -->
        <div class="row taskers-category-row">
            <!-- col -3 -->
            <div class="col-lg-3">
                <div class="tasker-cat-col">
                    <a href="javascript:void(0)">
                        <img src="img/data/home-cooking.jpg" alt="" class="img-fluid">
                        <span class="title-cat">Cooking</span>
                    </a>
                </div>
            </div>
            <!--/ col -3 -->
            <!-- col 6-->
            <div class="col-lg-6">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6">
                        <div class="tasker-cat-col">
                            <a href="javascript:void(0)">
                                <img src="img/data/it-comp.jpg" alt="" class="img-fluid">
                                <span class="title-cat">Computer and IT</span>
                            </a>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6">
                        <div class="tasker-cat-col">
                            <a href="javascript:void(0)">
                                <img src="img/data/event.jpg" alt="" class="img-fluid">
                                <span class="title-cat">Photography</span>
                            </a>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!-- /row -->

                <!-- row -->
                <div class="row mt-4">
                    <!-- col -->
                    <div class="col-lg-12">
                        <div class="tasker-cat-col">
                            <a href="javascript:void(0)">
                                <img src="img/data/fun-quirky.jpg" alt="" class="img-fluid">
                                <span class="title-cat">Handy Man</span>
                            </a>
                        </div>
                    </div>
                    <!--/ col -->                    
                </div>
                <!-- /row -->

            </div>
            <!-- /col 6-->

            <!-- col -3 -->
            <div class="col-lg-3">
                <div class="tasker-cat-col">
                    <a href="javascript:void(0)">
                        <img src="img/data/delivery-removal.jpg" alt="" class="img-fluid">
                        <span class="title-cat">Removals</span>
                    </a>
                </div>
            </div>
            <!--/ col -3 -->
        </div>
        <!--/ row -->       

        </div>
        <!-- /post yur task -->


        <!---- features -->
    <div class="features home-section">
      <!-- container -->
      <div class="container">
        <!-- row -->
        <div class="row justify-content-lg-center">
          <!-- col -->
          <div class="col-lg-8 text-center">
            <h2 class="pb-2 ">Ensuring your needs are supported</h2>            
          </div>
          <!--/ col -->
        </div>
        <!--/ row -->

        <!-- row features -->
        <div class="row pt-5">
          <!-- col -->
          <div class="col-lg-4 text-center feature-col">
            <span class="icon-tag icomoon h1"></span>
            <h5 class="h5">Top rated insurance</h5>
            <p class="text-center">Airtasker Insurance provided by CGU covers the Tasker for their liability to third parties for personal injury or property damage whilst performing certain task activities.</p>
          </div>
          <!--/ col -->
          <!-- col -->
          <div class="col-lg-4 text-center feature-col">
            <span class="icon-unlock icomoon h1"></span>
            <h5 class="h5">Secure payments</h5>
            <p class="text-center">Payments are escrowed via the Airtasker platform, so your Tasker can start right away knowing their the task payment is secure.</p>
          </div>
          <!--/ col -->
          <!-- col -->
          <div class="col-lg-4 text-center feature-col">
            <span class="icon-wechat icomoon h1"></span>
            <h5 class="h5">Customer Support</h5>
            <p>Got a question? Simply search our comprehensive Help Centre for your answer. If you’re still stuck then feel free to reach out to our expert Customer Support Team.</p>
          </div>
          <!--/ col -->
        </div>
        <!--/ row features -->
      </div>
      <!--/ container -->
    </div>
    <!--/ features -->
  

   

   



   
    
  </main>
  <!--/ main -->
  <?php include 'footer.php' ?>
  <?php include 'scripts.php' ?> 

</body>
</html>