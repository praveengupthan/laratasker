<!DOCTYPE html>
<html class="taskhtml" lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Lara Tasker Tasks List</title>

  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
    <?php include 'header.php' ?>

  <!-- main -->
  <main class="subpage">
    <?php include 'tasknav.php'?>
    <!-- tasker body -->
    <div class="container">
        <!-- row -->
        <div class="row taskrow">
            <!-- left col -->
            <div class="col-lg-4 left-taskcol ">
                <button class="btn pinkbtn w-100 text-center mb-3">6 New tasks</button>
                <!-- left task -->
                <div class="left-task sticky-top">
                    <!-- request col -->
                    <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Proof Reading</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 3 December, 2019</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker01.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2 expired-request">
                        <h2 class="d-flex justify-content-between">
                            <span>Elk Stack Designer</span>
                            <span class="fbold fred price">$ 25</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 10 Feb</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker02.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 fbold">
                        <span class="small fbold">Closed</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>I need drafting</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Sat, 15 Feb</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker03.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>I need help writing a management report</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Sun, 2 Feb</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker04.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Need to broadcast a post to FB and Insta</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 3 December, 2019</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker05.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>I need lawn mowing services</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Thu, 6 Feb</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker06.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Deck sanded</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Sun, 1 Mar</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker07.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Remove strobing from mov files</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 3 December, 2019</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker08.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fblue small fbold">Assigned .</span>     
                            <span class="small">11 Offers</span>                      
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Graphic Designer - Logo</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 3 December, 2019</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker09.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Proof Reading</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 3 December, 2019</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker10.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fblue small fbold">Assigned .</span>     
                            <span class="small">11 Offers</span>                      
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Airtable - Hlep and databse Build</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 3 December, 2019</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker01.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                     <!-- request col -->
                     <div class="request-col mb-2">
                        <h2 class="d-flex justify-content-between">
                            <span>Proof Reading</span>
                            <span class="fbold fred price">$ 45</span>
                        </h2>
                        <div class="row address-block py-2">
                            <div class="col-lg-8 align-self-center">
                                <p>
                                    <span class="icon-globe icomoon"></span>
                                    <span>Remote</span>
                                </p>
                                <p>
                                <span class="icon-calendar icomoon"></span>
                                    <span>Mon, 3 December, 2019</span>
                                </p>
                            </div>
                            <div class="col-lg-4 align-self-center">
                                <img src="img/data/tasker02.jpg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="border-top pt-1 ">
                            <span class="fgreen small fbold">Open</span>                           
                        </div>
                    </div>
                    <!--/ request col -->

                    <!-- div buttons -->
                    <div class="pb-4">
                        <p class="text-center pb-3">To See more taks</p>
                        <p class="text-center">
                            <a href="javascript:void(0)" class="bluebtnlg">Join Laratasker</a>
                            <span>Or</span>
                            <a href="javascript:void(0)" class="pinkbtnlg">Log in</a>
                        </p>
                    </div>
                    <!--/ div buttons -->

                </div>
                <!--/ left task -->
            </div>
            <!--/ left col -->

            <!-- right col -->
            <div class="col-lg-8 task-detail">
                <!-- primary details row -->
                <div class="row">
                    <!-- primarty details left col -->
                    <div class="col-lg-8">
                        <p class="status">
                            <span class="active">Open</span>
                            <span>Assigned</span>
                            <span>Completed</span>
                        </p>
                        <h1 class="h4 task-title">
                            MS WORD formatting (layout) help!
                        </h1>
                        <ul class="list-group">
                            <li class="list-group-item d-flex justify-content-between">
                                <div class="d-flex">
                                    <img src="img/data/tasker01.jpg" class="sm-thumb" alt="">
                                    <div class="pl-3 align-items-center">
                                        <span class="text-uppercase flgray small">Posted By </span>
                                        <p class="fblue ">Andrew B.</p>
                                    </div>
                                </div>
                                <span class="small d-inline-block align-self-center"> 29 mins ago </span>                            
                            </li>
                            <li class="list-group-item">
                                <div class="d-flex">
                                    <span class="icon-pin icomoon align-self-center"></span>
                                    <div class="pl-3 align-items-center">
                                        <span class="text-uppercase flgray small">LOCATION </span>
                                        <p class="">Remote</p>
                                    </div>
                                </div>                                                 
                            </li>
                            <li class="list-group-item">
                                <div class="d-flex">
                                    <span class="icon-calendar icomoon align-self-center"></span>
                                    <div class="pl-3 align-items-center">
                                        <span class="text-uppercase flgray small">Due Date </span>
                                        <p class="">Monday, 3rd Feb 2020</p>
                                    </div>
                                </div>                                                 
                            </li>
                        </ul>
                    </div>
                    <!--/ primarty details left col -->

                    <!-- primary details right col -->
                    <div class="col-lg-4 primaryrt">
                        <div class="primary-details-rt text-center">
                            <p class="text-uppercase fgray text-center">TASK BUDGET</p>
                            <p class="pt-3 text-center pb-0"><span class="small">Approx. 3hrs</span></p>
                            <h2 class="h2">$25</h2>
                            <button class="w-100" data-toggle="modal" data-target="#makeoffer-pop">Make an Offer</button>
                        </div>
                        <div class="dropdown my-2 ">
                            <button class="btn btn-outline-dark btn-sm dropdown-toggle w-100 border" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               More Options
                            </button>
                            <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Post Similar Task</a>
                                <a class="dropdown-item" href="#">Set up Alerts</a>                                
                            </div>
                        </div>
                        <p class="text-center">Share</p>
                        <ul class="share-task nav text-center justify-content-center border">
                           
                            <li class="nav-item">    
                                <a class="nav-link" href="javascript:void(0)" target="_blank">
                                    <span class="icon-facebook-official icomoon"></span>
                                </a>
                            </li>
                            <li class="nav-item">    
                                <a class="nav-link" href="javascript:void(0)" target="_blank">
                                    <span class="icon-twitter-square icomoon"></span>
                                </a>
                            </li>
                            <li class="nav-item">    
                                <a class="nav-link" href="javascript:void(0)" target="_blank">
                                    <span class="icon-linkedin-square icomoon"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- /primary details right col -->
                </div>
                <!--/ primary details row -->

                <!-- secondary row details -->
                <div class="row py-3">
                    <div class="col-lg-12">
                        <h6 class="fblue h6 text-uppercase">Details</h6>
                        <div class="description-task">
                        <p>I have been teaching for years and am doing my Masters Education to become a teacher librarian. I dont have much experience in libraries but have loads in teaching and in middle leadership positions in education. I will be applying to International Schools.</p>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus imperdiet, nulla et dictum interdum, nisi lorem egestas vitae scel<span id="dots">...</span><span id="more">erisque enim ligula venenatis dolor. Maecenas nisl est, ultrices nec congue eget, auctor vitae massa. Fusce luctus vestibulum augue ut aliquet. Nunc sagittis dictum nisi, sed ullamcorper ipsum dignissim ac. In at libero sed nunc venenatis imperdiet sed ornare turpis. Donec vitae dui eget tellus gravida venenatis. Integer fringilla congue eros non fermentum. Sed dapibus pulvinar nibh tempor porta.</span></p>

                            <a class="fpink" href="javascript:void(0)" onclick="myFunction()" id="showBtn">Read more</a>

                        </div>
                    </div>
                </div>
                <!--/ seconeary row details -->

                <!-- row offers -->
                <div class="row-offers row">
                    <div class="col-lg-12">
                        <h6 class="fblue h6 text-uppercase">Offers (5)</h6>

                        <!-- offer item -->
                       <div class="offer-list-item">
                           <!--tasker-->
                            <div class="d-flex py-3">
                                <img class="md-thumb" src="img/data/tasker02.jpg" alt="">
                                <article class="align-self-center pl-3">
                                <h6 class="h6 fblue">Melissa P.</h6>
                                <ul class="rating-tasker mb-0">
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star-half icomoon"></span>
                                    </li>
                                    <li class="pl-2">
                                        <span class="small">4.5 (50)</span>
                                    </li>
                                </ul>
                                <p>98% Completion rate 
                                    <a href="javascript:void(0)" data-toggle="tooltip" data-placement="right" title="The Percentage of Tasker has completed the Tasks"><span class="icon-info-circle flgray icomoon"></span></a>
                                </p>
                                </article>
                            </div>
                            <!--/ taskder -->

                            <!-- description-->
                            <article class="tasker-desc">
                                <p>I’d be happy to help you with this.</p>
                                <p>I am a degree qualified communications adviser and recruitment consultant with over 12 years of experience.</p>
                                <p>My specialties include resume writing and editing, tailored cover letters, LinkedIn profile optimisation, and ATS compliant applications.</p>
                                <p>I work with my clients to create resumes and cover letters that capture the most pertinent aspects of their experience relevant to the roles for which they’re applying.</p>
                                <p>My offer includes an initial consultation and several revisions until you’re happy. It also includes editable documents for your ongoing use.</p>
                            </article>
                            <!-- /description-->
                       </div>
                       <!-- offer item -->

                        <!-- offer item -->
                        <div class="offer-list-item">
                           <!--tasker-->
                            <div class="d-flex py-3">
                                <img class="md-thumb" src="img/data/tasker03.jpg" alt="">
                                <article class="align-self-center pl-3">
                                <h6 class="h6 fblue">Melissa P.</h6>
                                <ul class="rating-tasker mb-0">
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star-half icomoon"></span>
                                    </li>
                                    <li class="pl-2">
                                        <span class="small">4.5 (50)</span>
                                    </li>
                                </ul>
                                <p>98% Completion rate 
                                    <a href="javascript:void(0)" data-toggle="tooltip" data-placement="right" title="The Percentage of Tasker has completed the Tasks"><span class="icon-info-circle flgray icomoon"></span></a>
                                </p>
                                </article>
                            </div>
                            <!--/ taskder -->

                            <!-- description-->
                            <article class="tasker-desc">
                                <p>I am strategic , goal oriented, and always work with an end goal in mind. I have been creating unique, interactive, and beautiful resumes and cover letters for over 3 years and have worked for clients and companies around the world. Additionally, my proven interview-landing resume services will illuminate your professionalism and fast-track your application through HR and ATS processes with
outstanding credentials-based content that leaves lasting impressions.</p>
                                <p>Let me help you get that job you want; all I need is your current resume or draft. If you don't have a current resume, I can help you build a new one from scratch.</p>
                            </article>
                            <!-- /description-->
                       </div>
                       <!-- offer item -->


                        <!-- offer item -->
                        <div class="offer-list-item">
                           <!--tasker-->
                            <div class="d-flex py-3">
                                <img class="md-thumb" src="img/data/tasker05.jpg" alt="">
                                <article class="align-self-center pl-3">
                                <h6 class="h6 fblue">Melissa P.</h6>
                                <ul class="rating-tasker mb-0">
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star icomoon"></span>
                                    </li>
                                    <li>
                                        <span class="icon-star-half icomoon"></span>
                                    </li>
                                    <li class="pl-2">
                                        <span class="small">4.5 (50)</span>
                                    </li>
                                </ul>
                                <p>98% Completion rate 
                                    <a href="javascript:void(0)" data-toggle="tooltip" data-placement="right" title="The Percentage of Tasker has completed the Tasks"><span class="icon-info-circle flgray icomoon"></span></a>
                                </p>
                                </article>
                            </div>
                            <!--/ taskder -->

                            <!-- description-->
                            <article class="tasker-desc">
                                <p>I’d be happy to help you with this.</p>
                                <p>I am a degree qualified communications adviser and recruitment consultant with over 12 years of experience.</p>
                                <p>My specialties include resume writing and editing, tailored cover letters, LinkedIn profile optimisation, and ATS compliant applications.</p>                               
                                
                            </article>
                            <!-- /description-->
                       </div>
                       <!-- offer item -->
                    </div>                    
                </div>
                <!--/ row offers -->

                <!-- row questiions -->
                 <div class="row py-3">
                    <div class="col-lg-12">
                        <h6 class="fblue h6 text-uppercase">Questions (10)</h6>
                       <p>Please don't share personal info – insurance won't apply to tasks not done through Airtasker!</p>

                         <!-- ask questions -->
                         <div class="ask-question">
                             <!-- row -->
                             <div class="row">
                                 <!-- col -->
                                 <div class="col-lg-1">
                                    <img src="img/data/tasker01.jpg" class="sm-thumb" alt="">
                                 </div>
                                 <!--/ col -->
                                  <!-- col -->
                                  <div class="col-lg-11">
                                    <!-- div questiion section -->
                                    <div class="question-text-area">
                                        <textarea placeholder="Ask Questiion to Praveen"></textarea>

                                        <div class="question-bottomrow">
                                            <!-- image upload -->
                                            <div class="image-upload">
                                                <label for="file-input" class="fileinput">
                                                    <span class="icon-paperclip icomoon"></span>
                                                </label>
                                                <input id="file-input" type="file"/>
                                            </div>
                                            <!--/ image upload -->
                                            <div class="qtn-bottom-rt">
                                                <button class="sendbtn">Send</button>
                                            </div>
                                        </div>
                                    </div>
                                     <!--/ div questiion section -->
                                  </div>
                                 <!--/ col -->
                             </div>
                             <!--/row --> 
                         </div>
                        <!--/ ask questions -->

                       <!-- div buttons -->
                        <div class="pb-4">
                            <p class="text-center pb-3 h5">To join the conversation</p>
                            <p class="text-center">
                                <a href="javascript:void(0)" class="bluebtnlg">Join Laratasker</a>
                                <span>Or</span>
                                <a href="javascript:void(0)" class="pinkbtnlg">Log in</a>
                            </p>
                        </div>
                        <!--/ div buttons -->

                      

                        <!-- questioin -->
                        <div class="question">
                            <div class="d-flex">
                                <img src="img/data/tasker01.jpg" class="sm-thumb" alt="">
                                <div class="pl-3 align-items-center">                               
                                    <a href="javascript:void(0)" class="fblue h6">Andrew B.</a>
                                    <p>Hi Please see my offer and assign me for this task. I will create you a resume and a cover letter that will highlight your skills, experience, and education. So feel free to assign me.</p>

                                    <p>
                                        <span class="small fgray">4 hours ago</span>
                                        <span class="small fblue d-inline-block pl-3">
                                        <a href="javascript:void(0)" class="fblue" id="replybtn">
                                            <span class="icon-mail-reply icomoon"></span> Reply
                                        </a>
                                        </span>
                                    </p>
                                    <!-- ask questions -->
                                    <div class="ask-question" id="askqst">
                                        <!-- row -->
                                        <div class="row">
                                            <!-- col -->
                                            <div class="col-lg-1">
                                                <img src="img/data/tasker01.jpg" class="sm-thumb" alt="">
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-11">
                                                <!-- div questiion section -->
                                                <div class="question-text-area">
                                                    <textarea placeholder="Reply to Andrew B"></textarea>

                                                    <div class="question-bottomrow">
                                                        <!-- image upload -->
                                                        <div class="image-upload">
                                                            <label for="file-input" class="fileinput">
                                                                <span class="icon-paperclip icomoon"></span>
                                                            </label>
                                                            <input id="file-input" type="file"/>
                                                        </div>
                                                        <!--/ image upload -->
                                                        <div class="qtn-bottom-rt">
                                                            <button class="sendbtn">Send</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/ div questiion section -->
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/row --> 
                                    </div>
                                    <!--/ ask questions -->
                                </div>
                            </div>
                        </div>
                        <!--/ question -->

                        <!-- questioin -->
                        <div class="question">
                            <div class="d-flex">
                                <img src="img/data/tasker02.jpg" class="sm-thumb" alt="">
                                <div class="pl-3 align-items-center">                               
                                    <a href="javascript:void(0)" class="fblue h6">Faith T.</a>
                                    <p>Hi Please see my offer and assign me for this task. I will create you a resume and a cover letter that will highlight your skills, experience, and education. So feel free to assign me.</p>

                                    <p>
                                        <span class="small fgray">4 hours ago</span>
                                        <span class="small fblue d-inline-block pl-3">
                                        <a href="javascript:void(0)" class="fblue">
                                            <span class="icon-mail-reply icomoon"></span> Reply
                                        </a>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--/ question -->

                          <!-- questioin -->
                          <div class="question">
                            <div class="d-flex">
                                <img src="img/data/tasker04.jpg" class="sm-thumb" alt="">
                                <div class="pl-3 align-items-center">                               
                                    <a href="javascript:void(0)" class="fblue h6">Faith T.</a>
                                    <p>Hi Please see my offer and assign me for this task. I will create you a resume and a cover letter that will highlight your skills, experience, and education. So feel free to assign me.</p>

                                    <p>
                                        <span class="small fgray">4 hours ago</span>
                                        <span class="small fblue d-inline-block pl-3">
                                        <a href="javascript:void(0)" class="fblue">
                                            <span class="icon-mail-reply icomoon"></span> Reply
                                        </a>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--/ question -->
                    </div>
                </div> 
                <!--/ row questions -->

                <!-- private messages -->
                <div class="private-chat-messages">
                    <h6 class="fblue h6 text-uppercase">Private Messages</h6>
                    <p>Private messages here are only seen by you and Monalisa.J</p>
                    <p class="text-center">
                        <button class="whitebtn mx-auto">Load Earlier Messages</button>
                    </p>

                     <!-- user chat row secondary -->
                     <div class="row userchat-row chat-reply-row justify-content-start">
                        <!-- thumb col -->
                        <div class="col-lg-1 order-lg-last">
                            <img src="img/data/tasker04.jpg" class="sm-thumb" alt="">
                        </div>
                        <!--/ thumb col -->
                        <!-- message col -->
                        <div class="col-lg-10">
                            <div class="message-div">
                                <p class="d-flex chat-name justify-content-between">
                                    <span class="small">Chaintanya</span>
                                    <span class="small">3rd Feb 2020 - 10-15</span>
                                </p>
                                <p class="msg-desc">
                                    Hello Brother How are you?
                                </p>
                            </div>
                        </div>
                        <!--/ message col -->
                    </div>
                    <!-- user chat row secondary -->

                    <!-- user chat row -->
                    <div class="row userchat-row justify-content-end">
                        <!-- thumb col -->
                        <div class="col-lg-1">
                            <img src="img/data/tasker04.jpg" class="sm-thumb" alt="">
                        </div>
                        <!--/ thumb col -->
                        <!-- message col -->
                        <div class="col-lg-10">
                            <div class="message-div">
                                <p class="d-flex chat-name justify-content-between">
                                    <span class="small">Monalisa J. (Job Poster)</span>
                                    <span class="small">3rd Feb 2020 - 10-15</span>
                                </p>
                                <p class="msg-desc">
                                    I am good, Thank you, How about you.
                                </p>
                            </div>
                        </div>
                        <!--/ message col -->
                    </div>
                    <!-- user chat row -->

                    
                     <!-- user chat row secondary -->
                     <div class="row userchat-row chat-reply-row justify-content-start">
                        <!-- thumb col -->
                        <div class="col-lg-1 order-lg-last">
                            <img src="img/data/tasker04.jpg" class="sm-thumb" alt="">
                        </div>
                        <!--/ thumb col -->
                        <!-- message col -->
                        <div class="col-lg-10">
                            <div class="message-div">
                                <p class="d-flex chat-name justify-content-between">
                                    <span class="small">Chaintanya</span>
                                    <span class="small">3rd Feb 2020 - 10-15</span>
                                </p>
                                <p class="msg-desc">
                                    Hello Brother How are you?
                                </p>
                            </div>
                        </div>
                        <!--/ message col -->
                    </div>
                    <!-- user chat row secondary -->

                    <!-- user chat row -->
                    <div class="row userchat-row justify-content-end">
                        <!-- thumb col -->
                        <div class="col-lg-1">
                            <img src="img/data/tasker04.jpg" class="sm-thumb" alt="">
                        </div>
                        <!--/ thumb col -->
                        <!-- message col -->
                        <div class="col-lg-10">
                            <div class="message-div">
                                <p class="d-flex chat-name justify-content-between">
                                    <span class="small">Monalisa J. (Job Poster)</span>
                                    <span class="small">3rd Feb 2020 - 10-15</span>
                                </p>
                                <p class="msg-desc">
                                    I am good, Thank you, How about you.
                                </p>
                            </div>
                        </div>
                        <!--/ message col -->
                    </div>
                    <!-- user chat row -->

                      <!-- ask questions -->
                      <div class="ask-question mt-3">
                             <!-- row -->
                             <div class="row">
                                 <!-- col -->
                                 <div class="col-lg-1">
                                    <img src="img/data/tasker01.jpg" class="sm-thumb" alt="">
                                 </div>
                                 <!--/ col -->
                                  <!-- col -->
                                  <div class="col-lg-11">
                                    <!-- div questiion section -->
                                    <div class="question-text-area">
                                        <textarea placeholder="Ask Questiion to Praveen"></textarea>

                                        <div class="question-bottomrow">
                                            <!-- image upload -->
                                            <div class="image-upload">
                                                <label for="file-input" class="fileinput">
                                                    <span class="icon-paperclip icomoon"></span>
                                                </label>
                                                <input id="file-input" type="file"/>
                                            </div>
                                            <!--/ image upload -->
                                            <div class="qtn-bottom-rt">
                                                <button class="sendbtn">Send</button>
                                            </div>
                                        </div>
                                    </div>
                                     <!--/ div questiion section -->
                                  </div>
                                 <!--/ col -->
                             </div>
                             <!--/row --> 
                         </div>
                        <!--/ ask questions -->
                </div>
                <!--/ private messages -->
            </div>
            <!--/ right col -->
        </div>
        <!--/ row -->
    </div>
    <!--/ tasker body -->        
  </main>
  <!--/ main -->


  <?php include 'scripts.php' ?>

  
<script>
    var slider = document.getElementById("myRange1");
    var output = document.getElementById("demo1");
    output.innerHTML = slider.value;

    slider.oninput = function() {
    output.innerHTML = this.value;
    }
</script>
   
<script>
    var slider = document.getElementById("myRange");
    var output = document.getElementById("demo");
    output.innerHTML = slider.value;

    slider.oninput = function() {
    output.innerHTML = this.value;
    }
</script>

<!-- Modal for make an offer -->
<div class="modal fade " id="makeoffer-pop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header border-0">
       
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
       <!-- body -->
       <div class="modal-body stepbody">
         <!-- div id wizard-->
         <div id="makeOffer" class="foursteps">
              <!-- SECTION 1 -->
              <h4></h4>
              <section>
                  <h5>To start making money</h5>
                  
                  <!-- sectiion body -->
                  <div class="section-body">
                      <!-- div update data -->
                      <div class="update-data">
                          <p class="label">Upload a Profile Picture</p>
                          <div class="status d-flex" data-name='item_1'>
                              <span class="icon-check-circle icomoon"></span>
                              <p>Profile Photo Set</p>
                          </div>

                          <!-- dynamic data-->
                          <div class="makeoffer-datashow item_1" >
                                <p class="text-right">
                                    <a href="javascript:void(0)" class="back" data-name='item_1'>Back</a>
                                </p>
                                <div class="d-flex">
                                    <img src="img/data/tasker07.jpg" alt="" class="md-thumb">

                                    <div class="inputfile pl-3 align-self-center">
                                        <input type="file" title="Upload Photo" class="pinkbtn">
                                    </div>
                                </div>
                          </div>
                          <!-- /dynamic data-->
                      </div>
                      <!-- /div update data -->                      
                   
                      <!-- div update data -->
                      <div class="update-data">
                          <p class="label">Provide your bank account details</p>
                          <div class="status d-flex" data-name='item_2'>
                              <span class="icon-check-circle icomoon select"></span>
                              <p>Enter your Bank Details</p>
                          </div>

                           <!-- dynamic data-->
                           <div class="makeoffer-datashow item_2">
                                <p class="text-right">
                                    <a href="javascript:void(0)" class="back" data-name='item_2'>Back</a>
                                </p>
                               <p>Please provide your bank details so you can get paid. We don't take any money from your account.</p>

                               <form class="cust-form py-2">
                                    <!-- form group -->
                                    <div class="form-group">
                                        <label>Account holder name</label>
                                        <div class="input-group">
                                            <input type="text" placeholder="Name of Account holder" class="form-control">
                                        </div>
                                    </div>
                                     <!-- /form group -->

                                     <!-- form group -->
                                    <div class="form-group">
                                        <label>Account number</label>
                                        <div class="input-group">
                                            <input type="text" placeholder="Account number" class="form-control">
                                        </div>
                                    </div>
                                     <!-- /form group -->

                                     <!-- form group -->
                                    <div class="form-group mb-0">
                                        <label>BSB</label>
                                        <div class="input-group">
                                            <input type="text" placeholder="000-000" class="form-control">
                                        </div>
                                    </div>
                                     <!-- /form group -->                                   
                                    <button class="pinkbtnlg">Add Bank Account</button>
                               </form>
                          </div>
                          <!-- /dynamic data-->
                      </div>
                      <!-- /div update data -->

                       <!-- div update data -->
                       <div class="update-data">
                          <p class="label">Provide a Billing Address</p>
                          <div class="status d-flex" data-name="item_3">
                              <span class="icon-check-circle icomoon"></span>
                              <p>Enter your Billing Address</p>
                          </div>

                           <!-- dynamic data-->
                           <div class="makeoffer-datashow item_3">
                                <p class="text-right">
                                    <a href="javascript:void(0)" class="back" data-name='item_3'>Back</a>
                                </p>
                               <p>Your billing address will be verified before you can receive payments.</p>

                               <form class="cust-form py-2">
                                    <!-- form group -->
                                    <div class="form-group">
                                        <label>Address Line 1</label>
                                        <div class="input-group">
                                            <input type="text" placeholder="Address Line 1" class="form-control">
                                        </div>
                                    </div>
                                     <!-- /form group -->

                                     <!-- form group -->
                                    <div class="form-group">
                                        <label>Address Line 2 (optional)</label>
                                        <div class="input-group">
                                            <input type="text" placeholder="Address Line 2 (optional)" class="form-control">
                                        </div>
                                    </div>
                                     <!-- /form group -->

                                     <!-- form group -->
                                    <div class="form-group mb-0">
                                        <label>Suburb</label>
                                        <div class="input-group">
                                            <input type="text" placeholder="Suburb" class="form-control">
                                        </div>
                                    </div>
                                     <!-- /form group -->     
                                     
                                      <!-- form group -->
                                    <div class="form-group mb-0">
                                        <label>State</label>
                                        <div class="input-group">
                                            <input type="text" placeholder="State" class="form-control">
                                        </div>
                                    </div>
                                     <!-- /form group --> 

                                      <!-- form group -->
                                    <div class="form-group mb-0">
                                        <label>Postcode</label>
                                        <div class="input-group">
                                            <input type="text" placeholder="Postcode" class="form-control">
                                        </div>
                                    </div>
                                     <!-- /form group --> 

                                     <!-- form group -->
                                    <div class="form-group mb-0">
                                        <label>Country</label>
                                        <div class="input-group">
                                            <input type="text" placeholder="Country" class="form-control">
                                        </div>
                                    </div>
                                     <!-- /form group --> 


                                    <button class="pinkbtnlg">Add Billing Address</button>
                                    <p class="py-2">
                                        <small>Your address will never been shown publicly, it is only used for account verification purposes.</small>
                                    </p>
                               </form>
                          </div>
                          <!-- /dynamic data-->
                      </div>
                      <!-- /div update data -->

                       <!-- div update data -->
                       <div class="update-data">
                          <p class="label">Provide a Date of Birth</p>
                          <div class="status d-flex" data-name='item_4'>
                              <span class="icon-check-circle icomoon"></span>
                              <p>DD/MM/YYYY</p>
                          </div>

                            <!-- dynamic data-->
                            <div class="makeoffer-datashow item_4">
                                <p class="text-right">
                                    <a href="javascript:void(0)" class="back" data-name='item_4'>Back</a>
                                </p>

                               <form class="cust-form py-2">
                                    <!-- form group -->
                                    <div class="form-group">
                                        <label>Date of Birth</label>
                                        <div class="input-group d-flex">
                                            <input type="text" placeholder="DD" class="form-control w-25">
                                            <input type="text" placeholder="MM" class="form-control w-25">
                                            <input type="text" placeholder="YYYY" class="form-control w-25">
                                        </div>
                                    </div>
                                     <!-- /form group -->
                                    <button class="pinkbtnlg">Save Date of Birth</button>
                               </form>
                          </div>
                          <!-- /dynamic data-->
                      </div>
                      <!-- /div update data -->

                       <!-- div update data -->
                       <div class="update-data">
                          <p class="label">Provide a Mobile Number</p>
                          <div class="status d-flex" data-name='item_5'>
                              <span class="icon-check-circle icomoon"></span>
                              <p>Enter your Mobile Number</p>
                          </div>                          
                            <!-- dynamic data-->
                            <div class="makeoffer-datashow item_5">
                                <p class="text-right">
                                    <a href="javascript:void(0)" class="back" data-name='item_5'>Back</a>
                                </p>

                               <form class="cust-form py-2">
                                   <p>Let's have those digits! We'll keep you up to date about the latest happenings on your tasks by SMS.</p>
                                   <p class="py-2">Mobile number</p>
                                   <p>We will send you a verification code</p>
                                    <!-- form group -->
                                    <div class="form-group">                                       
                                        <div class="input-group d-flex">
                                            <input type="text" placeholder="+91 9642123254" class="form-control">                                           
                                        </div>
                                    </div>
                                     <!-- /form group -->
                                     <p class="small">Verifying your mobile number helps us know you're a genuine human! We won't show it to anyone or sell it on to any 3rd party, it's just for us to send you some good stuff.</p>
                                    <button class="pinkbtnlg">Send</button>
                               </form>
                          </div>
                          <!-- /dynamic data-->
                      </div>
                      <!-- /div update data -->
                  </div>
                  <!--/ sectiion body -->
              </section>

              <!--/ SECTION 1 -->
              
              <!-- SECTION 2 -->
              <h4></h4>
              <section>
                  <h5>Make an Offer</h5>
                  <div class="w-50 mx-auto">
                        <p>Your Offer</p>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">$</span>
                            </div>
                            <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
                            <div class="input-group-append">
                                <span class="input-group-text">.00</span>
                            </div>
                            </div>
                        </div>    
                        
                        <table class="table-bordered pb-3 table">
                            <tr>
                                <td><small class="fgray">Silver service fee</small></td>
                                <td><small class="fgray">-$216</small></td>
                            </tr>
                            <tr>
                                <td><h6 class="h6">You'll Receive</h6></td>
                                <td><h6 class="h6">$984</h6></td>
                            </tr>
                            <tr>
                                <td>Yes, I want to offer afterpay</td>
                                <td>
                                    <input type="radio">
                                </td>
                            </tr>
                            <tr>
                                <td><small class="fgray">Afterpay fee</small></td>
                                <td><small class="fgray">-$70</small></td>
                            </tr>
                            <tr>
                                <td><h6 class="h6">With Afterpay You'll Receive</h6></td>
                                <td><h6 class="h6">$913</h6></td>
                            </tr>
                        </table>

                        <p>
                            <small>This fee will only apply if the poster chooses to pay with Afterpay</small>
                        </p>                
              </section>
              <!-- / SECTION 2-->

              <!-- SECTION 3 -->
              <h4></h4>
              <section>
                  <h5>Make an Offer</h5>
                  <div class="form-group">
                      <p class="pb-0">Why are you the best person for this task?</p>
                      <p>
                          <small>for your safety, please do not share personal information, eg: email, phone or address</small>
                      </p>
                      <div class="input-group">
                         <textarea class="form-control" style="height:150px" placeholder="eg. I will be great for this task.  I have the necessary experience, skills and equipment required to get this done."></textarea>
                         
                      </div>                    
                  </div>
                                         
              </section>
              <!-- Section 3-->

                <!-- SECTION 4 -->
                <h4></h4>
              <section>   
                  <h5>Preview Offer</h5>    
                  <div class="graybox text-center">
                        <h6>Your Offer</h6>
                        <h1 class="h1">$1,200</h1>
                  </div>
                  
                  <table class="table-bordered pb-3 table">
                        <tr>
                            <td><small class="fgray">Silver service fee</small></td>
                            <td><small class="fgray">-$216</small></td>
                        </tr>
                        <tr>
                            <td><h6 class="h6">You'll Receive</h6></td>
                            <td><h6 class="h6">$984</h6></td>
                        </tr>
                       
                        <tr>
                            <td><small class="fgray">Afterpay fee</small></td>
                            <td><small class="fgray">-$70</small></td>
                        </tr>
                        <tr>
                            <td><h6 class="h6">With Afterpay You'll Receive</h6></td>
                            <td><h6 class="h6">$913</h6></td>
                        </tr>
                    </table>
                    <small>This fee will only apply if the Poster chooses to pay with afterpay </small>
                  
                </section>

                <script>
               
                    $(".status").click(function () {
                    var item = $(this).data("name");
                    $("."+ item).show();
                    });

                    $(".back").click(function () {                      
                        var item = $(this).data("name");
                        $("." + item).hide();
                    });
                
                </script>
              <!-- Section 4-->
          </div>
          <!--/ div id wizard -->
      </div>
      <!--/ body -->      
    </div>
  </div>
</div>
 <!-- /Modal for make an offer -->
</body>

</html>