<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Community Guidelines</title>

  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header.php' ?>

  <!-- main -->
  <main class="subpage">

      <!-- for business banner -->
      <div class="communityguide-header">
          <!-- container -->
          <div class="container">
              <!-- row -->
              <div class="row">
                  <!-- col-6-->
                  <div class="col-lg-6 align-self-center">
                      <h1 class="h1">Community Guidelines</h1>
                      <p>We’re passionate about connecting people ready to work with people who need work done. As Airtasker continues to grow it’s important that the community follows guidelines that reflect our values and standards of behaviour.</p>

                      <a href="javascript:void(0)" class="fwhite pr-3 fbold">Privacy Policy</a>
                      <a href="javascript:void(0)" class="fwhite fbold">Terms & Conditions</a>
                  </div>
                  <!-- /col-6-->

                   <!-- col-6-->
                   <div class="col-lg-6 text-center">
                     <img src="img/community-guidelines.webp" alt="" class="img-fluid">
                  </div>
                  <!-- /col-6-->  
              </div>
              <!--/ row -->
          </div>
          <!--/ container -->
      </div>
      <!--/ for busines sbanner -->
      <!-- container -->
      <div class="container">
            <!-- row -->
            <div class="row justify-content-center py-md-5">
                <!-- col -->
                <div class="col-lg-6 text-center">
                    <h1 class="h4 title-page">Policies for our community members</h1>
                    <p class="text-center">Creating a community where all members can enjoy a safe and rewarding Airtasker experience requires trust. Being responsible and respectful to others are the building blocks that form our marketplace integrity.</p>
                </div>
                <!--/ col --> 

                <!-- col 12 -->
                <div class="col-lg-12 justify-content-center text-center">
                    <a class="policy-card" href="javascript:void(0)">
                        <div class="policy-container d-flex">
                            <div class="policy-icon">
                                <span class="icon-user-circle-o icomoon"></span>
                            </div>
                            <div class="policy-desc text-left">
                                <h6 class="h5">Taskers</h6>
                                <p class="pb-0">A member that has made an offer on a task</p>
                            </div>
                        </div>
                    </a>
                    <a class="policy-card" href="javascript:void(0)">
                        <div class="policy-container d-flex">
                            <div class="policy-icon">
                                <span class="icon-user-o icomoon"></span>
                            </div>
                            <div class="policy-desc text-left">
                                <h6 class="h5">Posters</h6>
                                <p class="pb-0">A member that has Posted a task</p>
                            </div>
                        </div>
                    </a>
                </div>
                <!--/ col 12 -->
            </div>
            <!--/ row -->

            <!-- row -->
            <div class="row justify-content-center pb-4">
                 <!-- col -->
                 <div class="col-lg-4 text-center">
                    <h1 class="h4 title-page">Community values</h1>
                    <p class="text-center">Our values are crucial to maintain our high standards of marketplace integrity.</p>

                    <a class="bluebtnlg" href="javascript:void(0)">
                        View Brand Values
                    </a>
                </div>
                <!--/ col --> 
            </div>
            <!--/ row -->
      </div>
      <!--/ container -->

  </main>
  <!--/ main -->
  <?php include 'footer.php' ?>
  <?php include 'scripts.php' ?> 

</body>
</html>