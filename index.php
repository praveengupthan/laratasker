<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Lara Tasker</title>

  <?php include 'styles.php'?>
</head>

<body>
    <?php include 'header.php' ?>

  <!-- main -->
  <main>
    <!-- main slider -->
    <div class="homeslider d-flex justify-content-center align-items-center">
      <!-- container -->
      <div class="container">
        <!-- row -->
        <div class="row">
          <!-- col 8-->
          <div class="col-lg-8">
            <h1 class="h1 fbold">The best person for the job isn't always who you think</h1>
            <p>Find the people with the skills you need on Airtasker</p>
            <a href="javascript:void(0)">Get Started Now</a>
          </div>
          <!--/ col 8-->
        </div>
        <!--/ row -->
      </div>
      <!--/ container -->
    </div>
    <!--/ main slider -->

    <!-- categories -->
    <div class="home-categories home-section">
      <!-- container -->
      <div class="container">
        <h2 class="pb-2 text-center">What do you need done?</h2>

        <div class="categories-in d-flex justify-content-lg-center">
          <!-- col -->
          <a class="cat-col-home" href="javascript:void(0)">
            <span class="icon-bookmark icomoon"></span>
            <span class="text">Home cleaning</span>
          </a>
          <!--/ col -->
          <!-- col -->
          <a class="cat-col-home" href="javascript:void(0)">
            <span class="icon-taxi icomoon"></span>
            <span class="text">Full House Removals</span>
          </a>
          <!--/ col -->
          <!-- col -->
          <a class="cat-col-home" href="javascript:void(0)">
            <span class="icon-tag icomoon"></span>
            <span class="text">Few Items Removals</span>
          </a>
          <!--/ col -->
          <!-- col -->
          <a class="cat-col-home" href="javascript:void(0)">
            <span class="icon-tow-truck icomoon"></span>
            <span class="text">Furniture Assembly</span>
          </a>
          <!--/ col -->
          <!-- col -->
          <a class="cat-col-home" href="javascript:void(0)">
            <span class="icon-tag icomoon"></span>
            <span class="text">Handy Man</span>
          </a>
          <!--/ col -->
          <!-- col -->
          <a class="cat-col-home" href="javascript:void(0)">
            <span class="icon-telephone icomoon"></span>
            <span class="text">Marketing &amp; Design</span>
          </a>
          <!--/ col -->
          <!-- col -->
          <a class="cat-col-home" href="javascript:void(0)">
            <span class="icon-tyre icomoon"></span>
            <span class="text">Home &amp; Gardening</span>
          </a>
          <!--/ col -->
          <!-- col -->
          <a class="cat-col-home" href="javascript:void(0)">
            <span class="icon-search icomoon"></span>
            <span class="text">More</span>
          </a>
          <!--/ col -->
        </div>

      </div>
      <!--/ container -->
    </div>
    <!--/ categories -->

    <!-- see others get done -->
    <div class="other-getdone graybox">
      <!-- container -->
      <div class="container">
        <!-- row -->
        <div class="row">
          <div class="col-lg-12">
            <h2 class="pb-2">See what others are getting done</h2>
            <p>Got a few boxes to shift, an apartment or entire house? Get your home moved just the way you want, by
              whom you want, when you want. Let Airtasker shoulder the load.</p>
          </div>
        </div>
        <!-- row -->

        <!-- row -->
        <div class="row">
          <!-- col -->
          <div class="col-lg-12">
            <!-- tab -->
            <div class="parentHorizontalTab customtab">
              <ul class="resp-tabs-list hor_1 nav justify-content-center">
                <li>Moving Home</li>
                <li>Starting a business</li>
                <li>Fixing stuff</li>
                <li>Hosting a party</li>
                <li>Something different</li>
              </ul>
              <div class="resp-tabs-container hor_1">
                <!-- Moving Home -->
                <div>
                  <!-- card row -->
                  <div class="row">

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">House Delivery</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Move Queen size mattress from Southbank</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$250</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Delivery</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Moving washing machine</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$120</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Delivery</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Move Boxes and Pushbike from Kew to Sydney</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$100</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Assembly</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Disassemble/move bed frame and mattress</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$150</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">House Delivery</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Move Queen size mattress from Southbank</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$250</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Delivery</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Moving washing machine</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$120</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Delivery</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Move Boxes and Pushbike from Kew to Sydney</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$100</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Assembly</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Disassemble/move bed frame and mattress</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$150</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col 12 -->
                    <div class="col-lg-12">
                      <p class="text-center d-flex justify-content-center">
                        <a href="javascript:void(0)" class="pinkbtnlg mr-2">More Tasks</a>
                        <a href="javascript:void(0)" class="bluebtnlg">Get Start Now</a>
                      </p>
                    </div>
                    <!--/ col 12 -->

                  </div>
                  <!--/ card row -->
                </div>
                <!--/ Moving Home -->


                <!-- Starting a business-->
                <div>
                  <!-- card row -->
                  <div class="row">

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Graphic Design</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Create a Game of Thrones Trivia package</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$165</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Carpentary</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Make a throne out of old Skis</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$140</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Event staff</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Sing "Love is in the Air" by JP Young to my Mum</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$100</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Photography and Video</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Produce a video of yourself jumping into a puddle</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$1150</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Chef</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>MTeach me your family pasta sauce recipe</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$250</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Delivery</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Moving washing machine</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$120</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Delivery</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Move Boxes and Pushbike from Kew to Sydney</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$100</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Assembly</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Disassemble/move bed frame and mattress</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$150</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col 12 -->
                    <div class="col-lg-12">
                      <p class="text-center d-flex justify-content-center">
                        <a href="javascript:void(0)" class="pinkbtnlg mr-2">More Tasks</a>
                        <a href="javascript:void(0)" class="bluebtnlg">Get Start Now</a>
                      </p>
                    </div>
                    <!--/ col 12 -->

                  </div>
                  <!--/ card row -->
                </div>
                <!--/ Starting a business -->
                <!-- Fixing stuff -->
                <div>
                  <!-- card row -->
                  <div class="row">

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Graphic Design</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Create a Game of Thrones Trivia package</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$165</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Carpentary</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Make a throne out of old Skis</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$140</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Event staff</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Sing "Love is in the Air" by JP Young to my Mum</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$100</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->



                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Chef</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>MTeach me your family pasta sauce recipe</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$250</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Delivery</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Moving washing machine</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$120</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Delivery</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Move Boxes and Pushbike from Kew to Sydney</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$100</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Assembly</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Disassemble/move bed frame and mattress</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$150</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col 12 -->
                    <div class="col-lg-12">
                      <p class="text-center d-flex justify-content-center">
                        <a href="javascript:void(0)" class="pinkbtnlg mr-2">More Tasks</a>
                        <a href="javascript:void(0)" class="bluebtnlg">Get Start Now</a>
                      </p>
                    </div>
                    <!--/ col 12 -->

                  </div>
                  <!--/ card row -->
                </div>
                <!--/ Fixing stuff-->
                <!--Hosting a party -->
                <div>
                  <!-- card row -->
                  <div class="row">

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Graphic Design</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Create a Game of Thrones Trivia package</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$165</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Cleaning</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Clean up for children’s party</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$140</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Catering</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Sing "Love is in the Air" by JP Young to my Mum</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$100</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Gardening</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>DJ for 50th Birthday Partys</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$1150</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Cooking & Baking</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Deliver a surprise birthday cake to a restaurant</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$250</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Cooking & Baking</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Moving washing machine</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$120</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Delivery</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Move Boxes and Pushbike from Kew to Sydney</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$100</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Assembly</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Disassemble/move bed frame and mattress</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$150</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col 12 -->
                    <div class="col-lg-12">
                      <p class="text-center d-flex justify-content-center">
                        <a href="javascript:void(0)" class="pinkbtnlg mr-2">More Tasks</a>
                        <a href="javascript:void(0)" class="bluebtnlg">Get Start Now</a>
                      </p>
                    </div>
                    <!--/ col 12 -->

                  </div>
                  <!--/ card row -->
                </div>
                <!--/ Hosting a party -->
                <!-- Something different -->
                <div>
                  <!-- card row -->
                  <div class="row">

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Pickup & Delivery</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Make a throne out of old Skis</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$65</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Models</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Fun job! Need 2 people to wear a costume for 30min</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$140</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Event Staff</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Someone to sing Happy Birthday</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$100</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Gardening</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>DJ for 50th Birthday Partys</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$1150</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Cooking & Baking</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Deliver a surprise birthday cake to a restaurant</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$250</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Cooking & Baking</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Moving washing machine</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$120</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Delivery</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Move Boxes and Pushbike from Kew to Sydney</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$100</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                      <!-- card -->
                      <a href="javascript:void(0)">
                        <div class="card">
                          <!-- card header -->
                          <div class="card-header">
                            <h4 class="h6">Assembly</h4>
                          </div>
                          <!--/ card header -->
                          <!-- card body -->
                          <div class="card-body">
                            <div class="d-flex thumb">
                              <img src="img/thumbnail.png" alt="">
                              <p>Disassemble/move bed frame and mattress</p>
                            </div>
                          </div>
                          <!--/card card body -->
                          <!-- card footer -->
                          <div class="card-footer d-flex justify-content-between">
                            <span class="h6">$150</span>
                            <span><span class="icon-star fpink icomoon"></span> 5 Stars</span>
                          </div>
                          <!--/ card footer -->
                        </div>
                        <!--/ card -->
                      </a>
                    </div>
                    <!--/ col -->

                    <!-- col 12 -->
                    <div class="col-lg-12">
                      <p class="text-center d-flex justify-content-center">
                        <a href="javascript:void(0)" class="pinkbtnlg mr-2">More Tasks</a>
                        <a href="javascript:void(0)" class="bluebtnlg">Get Start Now</a>
                      </p>
                    </div>
                    <!--/ col 12 -->

                  </div>
                  <!--/ card row -->
                </div>
                <!--/ Something different-->
              </div>
            </div>
            <!-- tab -->
          </div>
          <!--/ col -->
        </div>
        <!--/ row -->
      </div>
      <!--/ container -->
    </div>
    <!--/ see others get  done -->

    <!-- how it works -->
    <div class="howitworks home-section">
      <!-- how is it works -->
      <div class="container">
        <!-- row -->
        <div class="row justify-content-center pb-3">
          <!-- col -->
          <div class="col-lg-6">
            <article class="text-center">
              <h2 class="pb-2 text-center">How does Laratasker work?</h2>
              <p>Check out the video below to see exactly how Airtasker can help you get those to-dos done once and for
                all.</p>
            </article>
          </div>
          <!--/ col -->
        </div>
        <!--/ row -->
        <!-- row -->
        <div class="row justify-content-center ">
          <!-- col -->
          <div class="col-lg-10">
            <!-- video -->
            <div class="videoimg position-relative">
              <img src="img/home-video-player.jpg" alt="" class="img-fluid">
              <a href="javascript:void(0)" class="position-absolute playicon">
                <span class="icon-play icomoon"></span>
              </a cl>
            </div>
            <!--/ video-->
            <!-- row -->
            <div class="row py-3 border-bottom">
              <!-- col -->
              <div class="col-lg-4 d-flex howitcol">
                <img src="img/howitworks01.svg" alt="">
                <article class="align-self-center">
                  <h6>Post your Task</h6>
                  <p>Tell us what you need. It's FREE to post</p>
                </article c>
              </div>
              <!-- /col -->
              <!-- col -->
              <div class="col-lg-4 d-flex howitcol">
                <img src="img/howitworks02.svg" alt="">
                <article class="align-self-center">
                  <h6>Review offers</h6>
                  <p>Get offers from trusted Taskers and view profiles.</p>
                </article c>
              </div>
              <!-- /col -->
              <!-- col -->
              <div class="col-lg-4 d-flex howitcol">
                <img src="img/howitworks03.svg" alt="">
                <article class="align-self-center">
                  <h6>Get it done</h6>
                  <p>Choose the right person for your task and get it done.</p>
                </article c>
              </div>
              <!-- /col -->
            </div>
            <!--/ row -->
            <!-- row -->
            <div class="row justify-content-center py-5">
              <div class="col-lg-6 text-center">
                <h5>
                  Ready to get that to-do list done? Start by posting a task today!
                </h5>
                <p class="text-center pt-3">
                  <a href="javascript:void(0)" class="pinkbtnlg">Get Started Now</a>
                </p>
              </div>
            </div>
            <!--/ row -->
          </div>
          <!--/ col -->
        </div>
        <!--/ row -->
        </diva>
        <!--/ how is it works -->
      </div>
    </div>
    <!--/ how it works -->

    <!-- list taskers -->
    <div class="graybox taskers-home">
      <!-- container -->
      <div class="container">
        <!-- row -->
        <div class="row justify-content-center">
          <!-- col -->
          <div class="col-lg-6 text-center">
            <h2 class="pb-2 ">Meet some Taskers!</h2>
            <p>Discover the story behind the people that are making the Airtasker community great, how and why they do
              what they do.</p>
          </div>
          <!--/ col -->
          <!-- col 12 -->
          <div class="col-lg-12">
            <p class="text-center d-flex justify-content-center">
              <a href="javascript:void(0)" class="pinkbtnlg mr-2">Become a Tasker?</a>
            </p>
          </div>
          <!--/ col 12 -->
        </div>
        <!--/ row -->
      </div>
      <!--/ container -->

      <!-- container fluid -->
      <div class="container-fluid">
        <!-- slider -->
        <div class="custom-slick taskers">
          <!-- slide -->
          <div class="slide">
            <!-- card -->
            <div class="card tasker-item">
              <!-- card body -->
              <div class="card-body text-center">
                <p class="small">Newyork, USA</p>
                <a href="javascript:void(0)"><img class="tasker-fig" src="img/data/tasker01.jpg" alt=""></a>
                <h6 class="h5 pt-2"><a class="fblue" href="javascript:void(0)">Samantha</a></h6>
                <p class="pb-2"><i class="small fgray">Specialities: assembly, pet care</i></p>
                <p class="small">Returning to the workforce as a single mum, Sam had to find something that could
                  be
                  flexible and cover the cost of childcare.</p>
              </div>
              <!--/ card body -->
              <!-- card footer -->
              <div class="card-footer text-center">
                <p>
                  <a href="javascript:void(0)" class="fblue">4.9 stars from 185 reviews</a>
                </p>
              </div>
              <!--/ card footer -->
            </div>
            <!--/ card -->
          </div>
          <!--/ slide -->

          <!-- slide -->
          <div class="slide">
            <!-- card -->
            <div class="card tasker-item">
              <!-- card body -->
              <div class="card-body text-center">
                <p class="small">Newyork, USA</p>
                <a href="javascript:void(0)"><img class="tasker-fig" src="img/data/tasker02.jpg" alt=""></a>
                <h6 class="h5 pt-2"><a class="fblue" href="javascript:void(0)">Emily</a></h6>
                <p class="pb-2"><i class="small fgray">Specialities: assembly, pet care</i></p>
                <p class="small">Returning to the workforce as a single mum, Sam had to find something that could
                  be
                  flexible and cover the cost of childcare.</p>
              </div>
              <!--/ card body -->
              <!-- card footer -->
              <div class="card-footer text-center">
                <p>
                  <a href="javascript:void(0)" class="fblue">4.9 stars from 185 reviews</a>
                </p>
              </div>
              <!--/ card footer -->
            </div>
            <!--/ card -->
          </div>
          <!--/ slide -->

          <!-- slide -->
          <div class="slide">
            <!-- card -->
            <div class="card tasker-item">
              <!-- card body -->
              <div class="card-body text-center">
                <p class="small">Newyork, USA</p>
                <a href="javascript:void(0)"><img class="tasker-fig" src="img/data/tasker03.jpg" alt=""></a>
                <h6 class="h5 pt-2"><a class="fblue" href="javascript:void(0)">Samantha</a></h6>
                <p class="pb-2"><i class="small fgray">Specialities: assembly, pet care</i></p>
                <p class="small">Returning to the workforce as a single mum, Sam had to find something that could
                  be
                  flexible and cover the cost of childcare.</p>
              </div>
              <!--/ card body -->
              <!-- card footer -->
              <div class="card-footer text-center">
                <p>
                  <a href="javascript:void(0)" class="fblue">4.9 stars from 185 reviews</a>
                </p>
              </div>
              <!--/ card footer -->
            </div>
            <!--/ card -->
          </div>
          <!--/ slide -->

          <!-- slide -->
          <div class="slide">
            <!-- card -->
            <div class="card tasker-item">
              <!-- card body -->
              <div class="card-body text-center">
                <p class="small">Newyork, USA</p>
                <a href="javascript:void(0)"><img class="tasker-fig" src="img/data/tasker04.jpg" alt=""></a>
                <h6 class="h5 pt-2"><a class="fblue" href="javascript:void(0)">Brendan</a></h6>
                <p class="pb-2"><i class="small fgray">Specialities: assembly, pet care</i></p>
                <p class="small">Returning to the workforce as a single mum, Sam had to find something that could
                  be
                  flexible and cover the cost of childcare.</p>
              </div>
              <!--/ card body -->
              <!-- card footer -->
              <div class="card-footer text-center">
                <p>
                  <a href="javascript:void(0)" class="fblue">4.9 stars from 185 reviews</a>
                </p>
              </div>
              <!--/ card footer -->
            </div>
            <!--/ card -->
          </div>
          <!--/ slide -->

          <!-- slide -->
          <div class="slide">
            <!-- card -->
            <div class="card tasker-item">
              <!-- card body -->
              <div class="card-body text-center">
                <p class="small">Newyork, USA</p>
                <a href="javascript:void(0)"><img class="tasker-fig" src="img/data/tasker05.jpg" alt=""></a>
                <h6 class="h5 pt-2"><a class="fblue" href="javascript:void(0)">Praveen Guptha</a></h6>
                <p class="pb-2"><i class="small fgray">Specialities: assembly, pet care</i></p>
                <p class="small">Returning to the workforce as a single mum, Sam had to find something that could
                  be
                  flexible and cover the cost of childcare.</p>
              </div>
              <!--/ card body -->
              <!-- card footer -->
              <div class="card-footer text-center">
                <p>
                  <a href="javascript:void(0)" class="fblue">4.9 stars from 185 reviews</a>
                </p>
              </div>
              <!--/ card footer -->
            </div>
            <!--/ card -->
          </div>
          <!--/ slide -->

          <!-- slide -->
          <div class="slide">
            <!-- card -->
            <div class="card tasker-item">
              <!-- card body -->
              <div class="card-body text-center">
                <p class="small">Newyork, USA</p>
                <a href="javascript:void(0)"><img class="tasker-fig" src="img/data/tasker06.jpg" alt=""></a>
                <h6 class="h5 pt-2"><a class="fblue" href="javascript:void(0)">Prabhakar</a></h6>
                <p class="pb-2"><i class="small fgray">Specialities: assembly, pet care</i></p>
                <p class="small">Returning to the workforce as a single mum, Sam had to find something that could
                  be
                  flexible and cover the cost of childcare.</p>
              </div>
              <!--/ card body -->
              <!-- card footer -->
              <div class="card-footer text-center">
                <p>
                  <a href="javascript:void(0)" class="fblue">4.9 stars from 185 reviews</a>
                </p>
              </div>
              <!--/ card footer -->
            </div>
            <!--/ card -->
          </div>
          <!--/ slide -->

          <!-- slide -->
          <div class="slide">
            <!-- card -->
            <div class="card tasker-item">
              <!-- card body -->
              <div class="card-body text-center">
                <p class="small">Newyork, USA</p>
                <a href="javascript:void(0)"><img class="tasker-fig" src="img/data/tasker07.jpg" alt=""></a>
                <h6 class="h5 pt-2"><a class="fblue" href="javascript:void(0)">John Peter</a></h6>
                <p class="pb-2"><i class="small fgray">Specialities: assembly, pet care</i></p>
                <p class="small">Returning to the workforce as a single mum, Sam had to find something that could
                  be
                  flexible and cover the cost of childcare.</p>
              </div>
              <!--/ card body -->
              <!-- card footer -->
              <div class="card-footer text-center">
                <p>
                  <a href="javascript:void(0)" class="fblue">4.9 stars from 185 reviews</a>
                </p>
              </div>
              <!--/ card footer -->
            </div>
            <!--/ card -->
          </div>
          <!--/ slide -->

          <!-- slide -->
          <div class="slide">
            <!-- card -->
            <div class="card tasker-item">
              <!-- card body -->
              <div class="card-body text-center">
                <p class="small">Newyork, USA</p>
                <a href="javascript:void(0)"><img class="tasker-fig" src="img/data/tasker08.jpg" alt=""></a>
                <h6 class="h5 pt-2"><a class="fblue" href="javascript:void(0)">Raja Gonda</a></h6>
                <p class="pb-2"><i class="small fgray">Specialities: assembly, pet care</i></p>
                <p class="small">Returning to the workforce as a single mum, Sam had to find something that could
                  be
                  flexible and cover the cost of childcare.</p>
              </div>
              <!--/ card body -->
              <!-- card footer -->
              <div class="card-footer text-center">
                <p>
                  <a href="javascript:void(0)" class="fblue">4.9 stars from 185 reviews</a>
                </p>
              </div>
              <!--/ card footer -->
            </div>
            <!--/ card -->
          </div>
          <!--/ slide -->

          <!-- slide -->
          <div class="slide">
            <!-- card -->
            <div class="card tasker-item">
              <!-- card body -->
              <div class="card-body text-center">
                <p class="small">Newyork, USA</p>
                <a href="javascript:void(0)"><img class="tasker-fig" src="img/data/tasker09.jpg" alt=""></a>
                <h6 class="h5 pt-2"><a class="fblue" href="javascript:void(0)">Suresh</a></h6>
                <p class="pb-2"><i class="small fgray">Specialities: assembly, pet care</i></p>
                <p class="small">Returning to the workforce as a single mum, Sam had to find something that could
                  be
                  flexible and cover the cost of childcare.</p>
              </div>
              <!--/ card body -->
              <!-- card footer -->
              <div class="card-footer text-center">
                <p>
                  <a href="javascript:void(0)" class="fblue">4.9 stars from 185 reviews</a>
                </p>
              </div>
              <!--/ card footer -->
            </div>
            <!--/ card -->
          </div>
          <!--/ slide -->

          <!-- slide -->
          <div class="slide">
            <!-- card -->
            <div class="card tasker-item">
              <!-- card body -->
              <div class="card-body text-center">
                <p class="small">Newyork, USA</p>
                <a href="javascript:void(0)"><img class="tasker-fig" src="img/data/tasker10.jpg" alt=""></a>
                <h6 class="h5 pt-2"><a class="fblue" href="javascript:void(0)">Shiva Kumar</a></h6>
                <p class="pb-2"><i class="small fgray">Specialities: assembly, pet care</i></p>
                <p class="small">Returning to the workforce as a single mum, Sam had to find something that could
                  be
                  flexible and cover the cost of childcare.</p>
              </div>
              <!--/ card body -->
              <!-- card footer -->
              <div class="card-footer text-center">
                <p>
                  <a href="javascript:void(0)" class="fblue">4.9 stars from 185 reviews</a>
                </p>
              </div>
              <!--/ card footer -->
            </div>
            <!--/ card -->
          </div>
          <!--/ slide -->

        </div>
        <!--/ slider -->
      </div>
      <!--/ container fluid -->
    </div>
    <!--/ list taskers -->

    <!---- features -->
    <div class="features home-section">
      <!-- container -->
      <div class="container">
        <!-- row -->
        <div class="row justify-content-lg-center">
          <!-- col -->
          <div class="col-lg-8 text-center">
            <h2 class="pb-2 ">Things you might also want to know</h2>
            <p>Whether you’re getting work done or doing tasks on Airtasker, know that we’ve got your back every step of
              the way.</p>
          </div>
          <!--/ col -->
        </div>
        <!--/ row -->

        <!-- row features -->
        <div class="row pt-5">
          <!-- col -->
          <div class="col-lg-4 text-center feature-col">
            <img src="img/securepayment-icon.jpg" alt="">
            <h5 class="h5">Secure Payment</h5>
            <p>We hold task payments secure with our PCI-DSS compliant Airtasker Pay so tasks can be completed knowing
              payment is there when you're done.</p>
          </div>
          <!--/ col -->
          <!-- col -->
          <div class="col-lg-4 text-center feature-col">
            <img src="img/features-insurance.jpg" alt="">
            <h5 class="h5">Top rated insurance</h5>
            <p>Insurance is there to ease any worries - making sure the Tasker has liability insurance from CGU while
              performing most task activities. T&C's apply.</p>
          </div>
          <!--/ col -->
          <!-- col -->
          <div class="col-lg-4 text-center feature-col">
            <img src="img/verfieid-badges.jpg" alt="">
            <h5 class="h5">Verified badges</h5>
            <p>Badges give members a bit more verified info when deciding who to work with on a task. Each badge has
              certain requirements that must be met and verified before they’re shown on the member's profile.</p>
          </div>
          <!--/ col -->
        </div>
        <!--/ row features -->
      </div>
      <!--/ container -->
    </div>
    <!--/ features -->

    <!-- blog -->
    <div class="blog-section home-section graybox">
      <!--- container -->
      <div class="container">
        <!-- row -->
        <div class="row justify-content-lg-center">
          <!-- col -->
          <div class="col-lg-8 text-center">
            <h2 class="pb-2 ">Articles, stories and more</h2>
            <p>Whether you’re getting work done or doing tasks on Airtasker, know that we’ve got your back every step of
              the way.</p>
            <p class="text-center d-flex justify-content-center">
              <a href="javascript:void(0)" class="pinkbtnlg mr-2">View Our All Blog</a>
            </p>
          </div>
          <!--/ col -->
        </div>
        <!--/ row -->

        <!-- row -->
        <div class="row pt-4">
          <!-- col -->
          <div class="col-lg-4">
            <div class="card">
              <img class="card-img-top" src="img/data/blog01.jpg" alt="Card image cap" class="img-fluid">
              <div class="card-body">
                <h5 class="card-title">30 Elegant art deco bathrooms</h5>
                <p class="card-text pb-4">Create a space that exudes elegance and gives a nod to the old world glamour
                  of
                  yesteryear. </p>
                <a href="#" class="bluebtnlg">Read More</a>
              </div>
            </div>
          </div>
          <!--/ col -->
          <!-- col -->
          <div class="col-lg-4">
            <div class="card">
              <img class="card-img-top" src="img/data/blog02.jpg" alt="Card image cap" class="img-fluid">
              <div class="card-body">
                <h5 class="card-title">40 Unbelievable U-shaped kitchen designs</h5>
                <p class="card-text pb-4">A practical solution that maximises bench space and storage! </p>
                <a href="#" class="bluebtnlg">Read More</a>
              </div>
            </div>
          </div>
          <!--/ col -->
          <!-- col -->
          <div class="col-lg-4">
            <div class="card">
              <img class="card-img-top" src="img/data/blog03.jpg" alt="Card image cap" class="img-fluid">
              <div class="card-body">
                <h5 class="card-title">The best bathroom vanity ideas for your home</h5>
                <p class="card-text pb-4">35 Functional and beautiful bathroom vanities that you will want in your home
                </p>
                <a href="#" class="bluebtnlg">Read More</a>
              </div>
            </div>
          </div>
          <!--/ col -->
        </div>
        <!--/ row -->
      </div>
      <!--/ container -->
    </div>
    <!--/ blog -->

  </main>
  <!--/ main -->

  <?php include 'footer.php' ?>
  <?php include 'scripts.php' ?>
  

</body>

</html>