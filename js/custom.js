//post task popup steps

$(function () {
    $("#postTask").steps({
        headerTag: "h4",
        bodyTag: "section",
        transitionEffect: "fade",
        enableAllSteps: true,
        onStepChanging: function (event, currentIndex, newIndex) {
            if (newIndex === 1) {
                $('.wizard > .steps ul').addClass('step-2');
            } else {
                $('.wizard > .steps ul').removeClass('step-2');
            }

            if (newIndex === 2) {
                $('.wizard > .steps ul').addClass('step-3');
            } else {
                $('.wizard > .steps ul').removeClass('step-3');
            }

            if (newIndex === 3) {
                $('.wizard > .steps ul').addClass('step-4');
            } else {
                $('.wizard > .steps ul').removeClass('step-4');
            }

            if (newIndex === 4) {
                $('.wizard > .steps ul').addClass('step-5');
            } else {
                $('.wizard > .steps ul').removeClass('step-5');
            }

            return true;
        },
        labels: {
            finish: "Submit",
            next: "Continue",
            previous: "Back"
        }
    });
    // Custom Button Jquery Steps
    $('.forward').click(function () {
        $("#wizard").steps('next');
    })
    $('.backward').click(function () {
        $("#wizard").steps('previous');
    })
})
//post task popup steps ends


//Make Offer popup steps

$(function () {
    $("#makeOffer").steps({
        headerTag: "h4",
        bodyTag: "section",
        transitionEffect: "fade",
        enableAllSteps: true,
        onStepChanging: function (event, currentIndex, newIndex) {
            if (newIndex === 1) {
                $('.wizard > .steps ul').addClass('step-2');
            } else {
                $('.wizard > .steps ul').removeClass('step-2');
            }

            if (newIndex === 2) {
                $('.wizard > .steps ul').addClass('step-3');
            } else {
                $('.wizard > .steps ul').removeClass('step-3');
            }

            if (newIndex === 3) {
                $('.wizard > .steps ul').addClass('step-4');
            } else {
                $('.wizard > .steps ul').removeClass('step-4');
            }

            return true;
        },
        labels: {
            finish: "Submit",
            next: "Continue",
            previous: "Back"
        }
    });
    // Custom Button Jquery Steps
    $('.forward').click(function () {
        $("#wizard").steps('next');
    })
    $('.backward').click(function () {
        $("#wizard").steps('previous');
    })
})
//Make Offer popup steps

//hide and show reply section
$('#askqst').css("display", "none");

$("#replybtn").click(function () {
    $('#askqst').css("display", "block");
});
//slick
//home categories slick
$('.taskers').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
    centerMode: false,
    responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
                dots: false
            }
        },
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 766,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 574,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});

//click event to move top

$(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
        $('#movetop').fadeIn();
    } else {
        $('#movetop').fadeOut();
    }
});

//Click event to scroll to top
$('#movetop').click(function () {
    $('html, body').animate({
        scrollTop: 0
    }, 800);
    return false;
});


//on scroll add class to header 

$(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
        $('.fixed-top').addClass('fixed-theme');
    } else {
        $('.fixed-top').removeClass('fixed-theme');
    }
});


$('[data-toggle="popover"]').popover();

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

$(document).ready(function () {
    //Horizontal Tab
    $('.parentHorizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function (event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });
    // Child Tab
    $('.ChildVerticalTab_1').easyResponsiveTabs({
        type: 'vertical',
        width: 'auto',
        fit: true,
        tabidentify: 'ver_1', // The tab groups identifier
        activetab_bg: '#fff', // background color for active tabs in this group
        inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
        active_border_color: '#c1c1c1', // border color for active tabs heads in this group
        active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
    });
    //Vertical Tab
    $('.parentVerticalTab').easyResponsiveTabs({
        type: 'vertical', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        closed: 'accordion', // Start closed if in accordion view
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function (event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo2');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });
});

//login and singup section hide and show 
$(document).ready(function () {

    $("#signuplink").click(function () {
        $("#signup").modal('show');
        $("#login").modal('hide');
    });

    $("#loginlink").click(function () {
        $("#login").modal('show');
        $("#signup").modal('hide');
    });

    $("#forgotpwlink").click(function () {
        $("#login").modal('hide');
        $("#forgotpassword").modal('show');
    });
});

//confirmatin box for post task popup
$(document).ready(function () {
    $("#btn-confirm-exit-postatask").click(function () {
        $("#task-close-confirmbox").modal('show');
    });

    $("#discardexitlink").click(function () {
        $("#task-close-confirmbox").modal('hide');
        $("#posttask").modal('hide');
    });
});

// With JQuery slider value
$(document).ready(function () {
    $('#ex1').slider({
        formatter: function (value) {
            return 'Current value: ' + value;
        }
    });
});

///show and less content 
function myFunction() {
    var dots = document.getElementById("dots");
    var moreText = document.getElementById("more");
    var btnText = document.getElementById("showBtn");

    if (dots.style.display === "none") {
        dots.style.display = "inline";
        btnText.innerHTML = "Show More";
        moreText.style.display = "none";
    } else {
        dots.style.display = "none";
        btnText.innerHTML = "Show Less";
        moreText.style.display = "inline";
    }
}

//accordian script
// Hiding the panel content. If JS is inactive, content will be displayed
$('.panel-content').hide();

// Preparing the DOM

// -- Update the markup of accordion container 
$('.accordion').attr({
    role: 'tablist',
    multiselectable: 'true'
});

// -- Adding ID, aria-labelled-by, role and aria-labelledby attributes to panel content
$('.panel-content').attr('id', function (IDcount) {
    return 'panel-' + IDcount;
});
$('.panel-content').attr('aria-labelledby', function (IDcount) {
    return 'control-panel-' + IDcount;
});
$('.panel-content').attr('aria-hidden', 'true');
// ---- Only for accordion, add role tabpanel
$('.accordion .panel-content').attr('role', 'tabpanel');

// -- Wrapping panel title content with a <a href="">
$('.panel-title').each(function (i) {

    // ---- Need to identify the target, easy it's the immediate brother
    $target = $(this).next('.panel-content')[0].id;

    // ---- Creating the link with aria and link it to the panel content
    $link = $('<a>', {
        'href': '#' + $target,
        'aria-expanded': 'false',
        'aria-controls': $target,
        'id': 'control-' + $target
    });

    // ---- Output the link
    $(this).wrapInner($link);

});

// Optional : include an icon. Better in JS because without JS it have non-sense.
$('.panel-title a').append('<span class="icon">+</span>');

// Now we can play with it
$('.panel-title a').click(function () {

    if ($(this).attr('aria-expanded') == 'false') { //If aria expanded is false then it's not opened and we want it opened !

        // -- Only for accordion effect (2 options) : comment or uncomment the one you want

        // ---- Option 1 : close only opened panel in the same accordion
        //      search through the current Accordion container for opened panel and close it, remove class and change aria expanded value
        $(this).parents('.accordion').find('[aria-expanded=true]').attr('aria-expanded', false).removeClass('active').parent().next('.panel-content').slideUp(200).attr('aria-hidden', 'true');

        // Option 2 : close all opened panels in all accordion container
        //$('.accordion .panel-title > a').attr('aria-expanded', false).removeClass('active').parent().next('.panel-content').slideUp(200);

        // Finally we open the panel, set class active for styling purpos on a and aria-expanded to "true"
        $(this).attr('aria-expanded', true).addClass('active').parent().next('.panel-content').slideDown(200).attr('aria-hidden', 'false');

    } else { // The current panel is opened and we want to close it

        $(this).attr('aria-expanded', false).removeClass('active').parent().next('.panel-content').slideUp(200).attr('aria-hidden', 'true');;

    }
    // No Boing Boing
    return false;
});

//profile edit and saved components 
$(document).ready(function () {
    $("#edit-profile-banner-icon").click(function () {
        $("#article-userprimary").hide();
        $("#edit-profile-basic").show();
        $("#edit-profile-banner-icon").hide();
        $("#edit-profile-basic").show();
        $(".profile-banner .image-upload").show();
        $(".profile-thumb .image-upload").show();
    });

    $("#saveprofile-basic-data").click(function () {
        $("#article-userprimary").show();
        $("#edit-profile-basic").hide();
        $("#edit-profile-banner-icon").show();
        $("#edit-profile-basic").hide();
        $(".profile-banner .image-upload").hide();
        $(".profile-thumb .image-upload").hide();
    });
});

document.getElementById("describe-user").value = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";

// about edit and saved components 
$(document).ready(function () {
    $("#edit-profile-icon").click(function () {
        $("#static-data-about").hide();
        $("#edit-description").show();
        $("#edit-profile-icon").hide();
    });

    $("#saveprofile-about-data").click(function () {
        $("#static-data-about").show();
        $("#edit-description").hide();
        $("#edit-profile-icon").show();
    });
});

// portfolio of profile edit and saved components 
$(document).ready(function () {
    $("#edit-portfolio-icon").click(function () {
        $("#static-data-portfolio").hide();
        $("#dynamic-data-portfolio").show();
        $("#edit-portfolio-icon").hide();
    });

    $("#save-portfolio-btn").click(function () {
        $("#static-data-portfolio").show();
        $("#dynamic-data-portfolio").hide();
        $("#edit-portfolio-icon").show();
    });
});

// skills of profile edit and saved components 
$(document).ready(function () {
    $("#edit-skills-btn").click(function () {
        $("#static-data-skills").hide();
        $("#edit-skills-dynamic").show();
        $("#edit-skills-btn").hide();
    });

    $("#save-skills-btn").click(function () {
        $("#static-data-skills").show();
        $("#edit-skills-dynamic").hide();
        $("#edit-skills-btn").show();
    });
});


//jobs page 
$(".scrollTo").on('click', function (e) {
    e.preventDefault();
    var target = $(this).attr('href');
    $('#movejobs').animate({
        scrollTop: ($(target).offset().top)
    }, 20);
});