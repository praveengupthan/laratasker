<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Account Dashboard</title>
  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header-postlogin.php' ?>

  <!-- main -->
  <main class="subpage usersubpage">
    <!--user container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- left navigation -->
            <div class="col-lg-3 leftnavigation">
              <?php include 'user-leftnav.php' ?>
            </div>
            <!--/ left navigatin -->

            <!-- right profile -->
            <div class="col-lg-9">
                <!-- right user panel-->
                <div class="right-user-panel">
                    <h1 class="h5 title-page">Payment History</h1>

                     <!-- tab -->
                    <div class="custom-tab">
                    
                          <ul class="nav nav-pills" id="myTab" role="tablist">
                              <li class="nav-item">
                                  <a class="nav-link active" id="Earned-tab" data-toggle="tab" href="#earned" role="tab" aria-controls="home" aria-selected="true">Earned</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" id="outgoing-tab" data-toggle="tab" href="#outgoing" role="tab" aria-controls="profile" aria-selected="false">Out Going</a>
                              </li>                           
                          </ul>

                          <div class="tab-content pt-3" id="myTabContent">
                              <!-- earned -->
                              <div class="tab-pane fade show active" id="earned" role="tabpanel" aria-labelledby="Earned-tab">
                                <!-- row -->
                                <div class="row pt-3 border-top">
                                    <!-- col -->
                                    <div class="col-lg-8">
                                        <div class="d-flex">
                                            <div class="form-group">
                                                <label>Showing</label>
                                                <div class="ingput-group">
                                                    <select class="form-control">
                                                        <option>All</option>
                                                        <option>Ranges</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- ranges -->
                                            <div class="d-flex">
                                                <div class="form-group datepickergroup px-3">
                                                    <label>From</label>
                                                    <div class="input-group">
                                                        <input placeholder="Select Date" class="form-control datepicker">
                                                    </div>
                                                </div>
                                                <div class="form-group datepickergroup">
                                                    <label>To</label>
                                                    <div class="input-group">
                                                        <input placeholder="Select Date" class="form-control datepicker">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /ranges -->
                                        </div>
                                    </div>
                                    <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-4 text-right align-self-center">
                                        <h6 class="h6">Net Earned</h6>
                                        <h5 class="h5 fblue">$0.00</h6>
                                    </div>
                                    <!--/ col -->
                                </div>
                                <!--/ row -->

                                <!-- row -->
                                <div class="row">
                                    <div class="col-lg-12">
                                      <p>
                                        <small><i>0 transactions for 10th Feb 2020 - 16th Feb 2020</i></small>
                                      </p>
                                    </div>
                                </div>
                                <!--/ row -->

                                  <!-- row -->
                                  <div class="row justify-content-center py-4">
                                    <div class="col-lg-6 text-center">
                                        <img src="img/payments_earned.png" alt="" class="img-fluid w-75">
                                        <p class="text-center pt-4">You haven't earned from any tasks yet. Yet to find the right task?</p>
                                        <p class="text-center">
                                            <a href="javascript:void(0)" class="fblue">Browse tasks</a>
                                        </p>
                                    </div>
                                </div>
                                <!--/ row -->
                              </div>
                              <!--/ earned -->

                              <!-- out going -->
                              <div class="tab-pane fade" id="outgoing" role="tabpanel" aria-labelledby="outgoing-tab">
                                <!-- row -->
                                <div class="row pt-3 border-top">
                                    <!-- col -->
                                    <div class="col-lg-8">
                                        <div class="d-flex">
                                            <div class="form-group">
                                                <label>Showing</label>
                                                <div class="ingput-group">
                                                    <select class="form-control">
                                                        <option>All</option>
                                                        <option>Ranges</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- ranges -->
                                            <div class="d-flex">
                                                <div class="form-group datepickergroup px-3">
                                                    <label>From</label>
                                                    <div class="input-group">
                                                        <input placeholder="Select Date" class="form-control datepicker">
                                                    </div>
                                                </div>
                                                <div class="form-group datepickergroup">
                                                    <label>To</label>
                                                    <div class="input-group">
                                                        <input placeholder="Select Date" class="form-control datepicker">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /ranges -->
                                        </div>
                                    </div>
                                    <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-4 text-right align-self-center">
                                        <h6 class="h6">Net Outgoing</h6>
                                        <h5 class="h5 fblue">$0.00</h6>
                                    </div>
                                    <!--/ col -->
                                </div>
                                <!--/ row -->

                                
                                <!-- row -->
                                <div class="row">
                                    <div class="col-lg-12">
                                      <p>
                                        <small><i>0 transactions for 10th Feb 2020 - 16th Feb 2020</i></small>
                                      </p>
                                    </div>
                                </div>
                                <!--/ row -->

                                  <!-- row -->
                                  <div class="row justify-content-center py-4">
                                    <div class="col-lg-6 text-center">
                                        <img src="img/payments_earned.png" alt="" class="img-fluid w-75">
                                        <p class="text-center pt-4">You haven't paid for any tasks yet. But let's change that!</p>
                                        <p class="text-center">
                                            <a href="javascript:void(0)" class="pinkbtnlg">Post a Task</a>
                                        </p>
                                    </div>
                                </div>
                                <!--/ row -->


                              </div>
                              <!--/ out going -->
                          </div>

                      </div>

                      <!--/ tab -->

                   


                 


                </div>
                <!--/ right user panel -->
            </div>
            <!--/ right profile -->
        </div>
        <!--/ row -->
    </div>
    <!--/ user container -->
  </main>
  <!--/ main -->

  <?php include 'scripts.php' ?> 
</body>
</html>