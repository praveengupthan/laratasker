<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Category as a Poster</title>

  <?php include 'styles.php'?>
</head>

<body class="subpage-body innerheader">
 <?php include 'header.php' ?>

  <!-- main -->
  <main class="subpage">

  <!-- poster header -->
  <div class="poster-header">
  <!-- container -->
  <div class="container">
      <!-- row -->
      <div class="row justify-content-center">
            <!-- col -->
            <div class="col-lg-7 text-center">
                <div class="category-headerin">
                    <h1 class="h1 fbold">657 Accountants Near You</h1>
                    <p class="text-center fgray px-5">Receive no-obligation quotes from reviewed, rated & trusted Accountants in minutes.</p>

                    <p>
                        <small>I need</small>
                        <div class="form-group">
                            <select class="form-control">
                                <option>Accounting Services</option>
                                <option>BASS Accounting </option>
                                <option>Book Keeping</option>
                                <option>Budgeting Help</option>
                                <option>Financial Modeling</option>
                                <option>Financial Planning</option>
                                <option> Something Else </option>
                            </select>
                        </div>
                        <button class="pinkbtnlg w-100">Get Free Quote</button>
                        <p class="small fgray text-center pt-3 pb-0">Free quotes in minutes</p>
                    </p>
                </div>
            </div>
            <!--. col -->
      </div>
      <!--/ row -->
  </div>
  <!--/ container -->
  </div>
  <!--/ poster header -->
     

   <!-- bested reated taskers-->
   <div class="taskers-best title-row">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="col-lg-12">
                <h2 class="h3">Best Rated Accountants</h2>
            </div>
            <!--/ row -->

            <!-- taskers row -->
            <div class="row">

                <!-- col -->
                <div class="col-lg-3">
                    <div class="taskercol">
                        <!-- card -->
                        <div class="card">
                            <!-- header -->
                            <div class="card-header d-flex">
                                <a href="javascript:void(0)">
                                    <img src="img/data/tasker01.jpg" class="md-thumb">
                                </a>
                                <article class="pl-2 align-self-center">
                                    <a href="javascript:void(0)">
                                        <h6 class="mb-0">Jason J</h6>
                                        <p class="small fgray pb-0">Hornsby NSW</p>
                                    </a>
                                    <p>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span>(25)</span>
                                    </p>
                                </article>
                            </div>
                            <!--/ header -->
                            <!-- body -->
                            <div class="card-body">
                                <h6 class="fgray fbold text-uppercase h6">LATEST REVIEW</h6>
                                <p class="rev">"Super helpful session, and a Quickbooks Pro!"</p>
                                <h6 class="fgray fbold text-uppercase h6">VERIFIED BADGES</h6>                               
                                <a href="javascript:void(0)" class="badge" data-toggle="tooltip" data-placement="top" title="Phone Verfied">
                                    <span class="icon-phone icomoon"></span>
                                 </a>
                            </div>
                            <!--/ body -->

                            <!-- footer -->
                            <div class="card-footer text-center">
                                <a href="javascript:void(0)" class="bluebtnlg">Request a Quote</a>
                            </div>
                            <!--/ footer -->
                        </div>
                        <!--/ card -->
                    </div>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-3">
                    <div class="taskercol">
                        <!-- card -->
                        <div class="card">
                            <!-- header -->
                            <div class="card-header d-flex">
                                <a href="javascript:void(0)">
                                    <img src="img/data/tasker02.jpg" class="md-thumb">
                                </a>
                                <article class="pl-2 align-self-center">
                                    <a href="javascript:void(0)">
                                        <h6 class="mb-0">Fiona M</h6>
                                        <p class="small fgray pb-0">Varsity Lakes QLD</p>
                                    </a>
                                    <p>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span>(52)</span>
                                    </p>
                                </article>
                            </div>
                            <!--/ header -->
                            <!-- body -->
                            <div class="card-body">
                                <h6 class="fgray fbold text-uppercase h6">LATEST REVIEW</h6>
                                <p class="rev">"I needed xero set up for me as a some trader and i had no idea how to do it ye tf tried and made a mess. Fiona sorted out mh mess, explained it all an..."</p>
                                <h6 class="fgray fbold text-uppercase h6">VERIFIED BADGES</h6>  

                                <a href="javascript:void(0)" class="badge" data-toggle="tooltip" data-placement="top" title="Payment Method Verfied">
                                    <span class="icon-credit-card icomoon"></span>
                                 </a>

                                 <a href="javascript:void(0)" class="badge" data-toggle="tooltip" data-placement="top" title="Phone Verfied">
                                    <span class="icon-phone icomoon"></span>
                                 </a>

                                 <a href="javascript:void(0)" class="badge" data-toggle="tooltip" data-placement="top" title="Facebook Verified">
                                    <span class="icon-facebook icomoon"></span>
                                 </a>
                            </div>
                            <!--/ body -->

                             <!-- footer -->
                             <div class="card-footer text-center">
                                <a href="javascript:void(0)" class="bluebtnlg">Request a Quote</a>
                            </div>
                            <!--/ footer -->

                        </div>
                        <!--/ card -->
                    </div>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-3">
                    <div class="taskercol">
                        <!-- card -->
                        <div class="card">
                            <!-- header -->
                            <div class="card-header d-flex">
                                <a href="javascript:void(0)">
                                    <img src="img/data/tasker03.jpg" class="md-thumb">
                                </a>
                                <article class="pl-2 align-self-center">
                                    <a href="javascript:void(0)">
                                        <h6 class="mb-0">Hamza A</h6>
                                        <p class="small fgray pb-0">Perth WA</p>
                                    </a>
                                    <p>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span>(31)</span>
                                    </p>
                                </article>
                            </div>
                            <!--/ header -->
                            <!-- body -->
                            <div class="card-body">
                                <h6 class="fgray fbold text-uppercase h6">LATEST REVIEW</h6>
                                <p class="rev">"Very helpful and professional. 5*"</p>
                                <h6 class="fgray fbold text-uppercase h6">VERIFIED BADGES</h6>

                                 <a href="javascript:void(0)" class="badge" data-toggle="tooltip" data-placement="top" title="Phone Verfied">
                                    <span class="icon-phone icomoon"></span>
                                 </a>
                               
                            </div>
                            <!--/ body -->

                             <!-- footer -->
                             <div class="card-footer text-center">
                                <a href="javascript:void(0)" class="bluebtnlg">Request a Quote</a>
                            </div>
                            <!--/ footer -->

                        </div>
                        <!--/ card -->
                    </div>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-3">
                    <div class="taskercol">
                        <!-- card -->
                        <div class="card">
                            <!-- header -->
                            <div class="card-header d-flex">
                                <a href="javascript:void(0)">
                                    <img src="img/data/tasker04.jpg" class="md-thumb">
                                </a>
                                <article class="pl-2 align-self-center">
                                    <a href="javascript:void(0)">
                                        <h6 class="mb-0">Shariff K</h6>
                                        <p class="small fgray pb-0">Plumpton NSW</p>
                                    </a>
                                    <p>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span>(16)</span>
                                    </p>
                                </article>
                            </div>
                            <!--/ header -->
                            <!-- body -->
                            <div class="card-body">
                                <h6 class="fgray fbold text-uppercase h6">LATEST REVIEW</h6>
                                <p class="rev">"Great guy! Efficient and reliable and most importantly super knowledgeable in his field. I was looking for someone just to help me with this one singl..."</p>
                                <h6 class="fgray fbold text-uppercase h6">VERIFIED BADGES</h6>

                                <a href="javascript:void(0)" class="badge" data-toggle="tooltip" data-placement="top" title="Payment Method Verfied">
                                    <span class="icon-credit-card icomoon"></span>
                                 </a>

                                 <a href="javascript:void(0)" class="badge" data-toggle="tooltip" data-placement="top" title="Phone Verfied">
                                    <span class="icon-phone icomoon"></span>
                                 </a>

                                 <a href="javascript:void(0)" class="badge" data-toggle="tooltip" data-placement="top" title="Facebook Verified">
                                    <span class="icon-facebook icomoon"></span>
                                 </a>
                               
                            </div>
                            <!--/ body -->

                             <!-- footer -->
                             <div class="card-footer text-center">
                                <a href="javascript:void(0)" class="bluebtnlg">Request a Quote</a>
                            </div>
                            <!--/ footer -->

                        </div>
                        <!--/ card -->
                    </div>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-3">
                    <div class="taskercol">
                        <!-- card -->
                        <div class="card">
                            <!-- header -->
                            <div class="card-header d-flex">
                                <a href="javascript:void(0)">
                                    <img src="img/data/tasker05.jpg" class="md-thumb">
                                </a>
                                <article class="pl-2 align-self-center">
                                    <a href="javascript:void(0)">
                                        <h6 class="mb-0">Tess K</h6>
                                        <p class="small fgray pb-0">Blenheim Road NSW</p>
                                    </a>
                                    <p>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span>(16)</span>
                                    </p>
                                </article>
                            </div>
                            <!--/ header -->
                            <!-- body -->
                            <div class="card-body">
                                <h6 class="fgray fbold text-uppercase h6">LATEST REVIEW</h6>
                                <p class="rev">"Tess was most helpful in organising my items for moving to 2 separate locations, as well as being flexible re moving dates/companies. Tess made the mo..."</p>
                                <h6 class="fgray fbold text-uppercase h6">VERIFIED BADGES</h6>                               

                                 <a href="javascript:void(0)" class="badge" data-toggle="tooltip" data-placement="top" title="Phone Verfied">
                                    <span class="icon-phone icomoon"></span>
                                 </a>

                                 <a href="javascript:void(0)" class="badge" data-toggle="tooltip" data-placement="top" title="Facebook Verified">
                                    <span class="icon-facebook icomoon"></span>
                                 </a>
                               
                            </div>
                            <!--/ body -->

                             <!-- footer -->
                             <div class="card-footer text-center">
                                <a href="javascript:void(0)" class="bluebtnlg">Request a Quote</a>
                            </div>
                            <!--/ footer -->

                        </div>
                        <!--/ card -->
                    </div>
                </div>
                <!--/ col -->

                
                 <!-- col -->
                 <div class="col-lg-3">
                    <div class="taskercol">
                        <!-- card -->
                        <div class="card">
                            <!-- header -->
                            <div class="card-header d-flex">
                                <a href="javascript:void(0)">
                                    <img src="img/data/tasker06.jpg" class="md-thumb">
                                </a>
                                <article class="pl-2 align-self-center">
                                    <a href="javascript:void(0)">
                                        <h6 class="mb-0">David L</h6>
                                        <p class="small fgray pb-0">Springvale VIC</p>
                                    </a>
                                    <p>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span>(13)</span>
                                    </p>
                                </article>
                            </div>
                            <!--/ header -->
                            <!-- body -->
                            <div class="card-body">
                                <h6 class="fgray fbold text-uppercase h6">LATEST REVIEW</h6>
                                <p class="rev">"Great work David"</p>
                                <h6 class="fgray fbold text-uppercase h6">VERIFIED BADGES</h6>                               

                                 <a href="javascript:void(0)" class="badge" data-toggle="tooltip" data-placement="top" title="Phone Verfied">
                                    <span class="icon-phone icomoon"></span>
                                 </a>

                                 <a href="javascript:void(0)" class="badge" data-toggle="tooltip" data-placement="top" title="Facebook Verified">
                                    <span class="icon-facebook icomoon"></span>
                                 </a>
                               
                            </div>
                            <!--/ body -->

                             <!-- footer -->
                             <div class="card-footer text-center">
                                <a href="javascript:void(0)" class="bluebtnlg">Request a Quote</a>
                            </div>
                            <!--/ footer -->

                        </div>
                        <!--/ card -->
                    </div>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-3">
                    <div class="taskercol">
                        <!-- card -->
                        <div class="card">
                            <!-- header -->
                            <div class="card-header d-flex">
                                <a href="javascript:void(0)">
                                    <img src="img/data/tasker07.jpg" class="md-thumb">
                                </a>
                                <article class="pl-2 align-self-center">
                                    <a href="javascript:void(0)">
                                        <h6 class="mb-0">Maple I</h6>
                                        <p class="small fgray pb-0">Kew VIC</p>
                                    </a>
                                    <p>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span>(12)</span>
                                    </p>
                                </article>
                            </div>
                            <!--/ header -->
                            <!-- body -->
                            <div class="card-body">
                                <h6 class="fgray fbold text-uppercase h6">LATEST REVIEW</h6>
                                <p class="rev">"Maple was great very helpful Thanks Maple"</p>
                                <h6 class="fgray fbold text-uppercase h6">VERIFIED BADGES</h6>                               

                                 <a href="javascript:void(0)" class="badge" data-toggle="tooltip" data-placement="top" title="Phone Verfied">
                                    <span class="icon-phone icomoon"></span>
                                 </a>

                                 <a href="javascript:void(0)" class="badge" data-toggle="tooltip" data-placement="top" title="Facebook Verified">
                                    <span class="icon-facebook icomoon"></span>
                                 </a>
                               
                            </div>
                            <!--/ body -->

                             <!-- footer -->
                             <div class="card-footer text-center">
                                <a href="javascript:void(0)" class="bluebtnlg">Request a Quote</a>
                            </div>
                            <!--/ footer -->

                        </div>
                        <!--/ card -->
                    </div>
                </div>
                <!--/ col -->

                
                 <!-- col -->
                 <div class="col-lg-3">
                    <div class="taskercol">
                        <!-- card -->
                        <div class="card">
                            <!-- header -->
                            <div class="card-header d-flex">
                                <a href="javascript:void(0)">
                                    <img src="img/data/tasker08.jpg" class="md-thumb">
                                </a>
                                <article class="pl-2 align-self-center">
                                    <a href="javascript:void(0)">
                                        <h6 class="mb-0">Taimoor S</h6>
                                        <p class="small fgray pb-0">Glenroy VIC</p>
                                    </a>
                                    <p>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span><span class="icon-star icomoon"></span></span>
                                        <span>(23)</span>
                                    </p>
                                </article>
                            </div>
                            <!--/ header -->
                            <!-- body -->
                            <div class="card-body">
                                <h6 class="fgray fbold text-uppercase h6">LATEST REVIEW</h6>
                                <p class="rev">"Taimoor assisted me to understand corporate financial principals and theory and he was very patient thanks again."</p>
                                <h6 class="fgray fbold text-uppercase h6">VERIFIED BADGES</h6>                               

                                 <a href="javascript:void(0)" class="badge" data-toggle="tooltip" data-placement="top" title="Phone Verfied">
                                    <span class="icon-phone icomoon"></span>
                                 </a>

                            </div>
                            <!--/ body -->

                             <!-- footer -->
                             <div class="card-footer text-center">
                                <a href="javascript:void(0)" class="bluebtnlg">Request a Quote</a>
                            </div>
                            <!--/ footer -->

                        </div>
                        <!--/ card -->
                    </div>
                </div>
                <!--/ col -->

            </div>
            <!--/ taskers row -->
        </div>
        <!--/ container -->
   </div>
   <!--/ bested reated taskers-->

   <!-- recent  reviews -->
   <div class="recent-reviews title-row">
        <!-- container -->
        <div class="container">
             <!-- row -->
             <div class="row">
                 <!-- col -->
                <div class="col-lg-12">
                    <h2 class="h3">Recent Accounting reviews</h2>
                </div>
                <!--/ col -->
                <!-- left col -->
                <div class="col-lg-9"></div>
                <!-- left col -->

                 <!-- right col -->
                 <div class="col-lg-3"></div>
                <!-- right col -->
             </div>
            <!--/ row -->

            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-9">
                    <!-- card -->
                    <div class="card mb-2">
                        <!-- body -->
                        <div class="card-body d-flex">
                            <img src="img/data/tasker01.jpg" class="md-thumb">
                            <article class="pl-4">
                                <ul class="rating-tasker mb-0">
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                </ul>
                                <p>"Fabulous and prompt communication and replies to my questions."</p>
                            </article>
                        </div>
                        <!--/ body -->
                        <!-- footer -->
                        <div class="card-footer d-flex justify-content-between bgflgray">
                            <span>Bookkeeper</span>
                            <h6>$25</h6>
                        </div>
                        <!--/ footer -->
                    </div>
                    <!--/ card -->

                      <!-- card -->
                      <div class="card mb-2">
                        <!-- body -->
                        <div class="card-body d-flex">
                            <img src="img/data/tasker02.jpg" class="md-thumb">
                            <article class="pl-4">
                                <ul class="rating-tasker mb-0">
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                </ul>
                                <p>"Timely, responsive and high quality. A+"</p>
                            </article>
                        </div>
                        <!--/ body -->
                        <!-- footer -->
                        <div class="card-footer d-flex justify-content-between bgflgray">
                            <span>Develop a database of SME lawyers and accountants</span>
                            <h6>$25</h6>
                        </div>
                        <!--/ footer -->
                    </div>
                    <!--/ card -->

                      <!-- card -->
                      <div class="card mb-2">
                        <!-- body -->
                        <div class="card-body d-flex">
                            <img src="img/data/tasker01.jpg" class="md-thumb">
                            <article class="pl-4">
                                <ul class="rating-tasker mb-0">
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                </ul>
                                <p>"Michael exceeded our expectations! We needed a Book Keeper on short notice. He was punctual, polite, diligent, and knowledgeable. We highly recommend him and will definitely book him again. Thank you Michael! "</p>
                            </article>
                        </div>
                        <!--/ body -->
                        <!-- footer -->
                        <div class="card-footer d-flex justify-content-between bgflgray">
                            <span>Book keeper with MYOB needed twice a fortnight</span>
                            <h6>$66</h6>
                        </div>
                        <!--/ footer -->
                    </div>
                    <!--/ card -->

                      <!-- card -->
                      <div class="card mb-2">
                        <!-- body -->
                        <div class="card-body d-flex">
                            <img src="img/data/tasker04.jpg" class="md-thumb">
                            <article class="pl-4">
                                <ul class="rating-tasker mb-0">
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                </ul>
                                <p>"Michael was great. He did my bookkeeping n reconciliation in very good time, he went outside his task improving the set up of my company n of the accounts. I would definitely work again with him n I would highly recommend his work "</p>
                            </article>
                        </div>
                        <!--/ body -->
                        <!-- footer -->
                        <div class="card-footer d-flex justify-content-between bgflgray">
                            <span>MYOB entries</span>
                            <h6>$100</h6>
                        </div>
                        <!--/ footer -->
                    </div>
                    <!--/ card -->

                      <!-- card -->
                      <div class="card mb-2">
                        <!-- body -->
                        <div class="card-body d-flex">
                            <img src="img/data/tasker01.jpg" class="md-thumb">
                            <article class="pl-4">
                                <ul class="rating-tasker mb-0">
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                </ul>
                                <p>"Michael is an expert in his field. His help was amazing. Gave us all the support, advice and assistance we could have possibly asked for. Such a genuine, nice and helpful guy. Couldnt recommend him highly enough."</p>
                            </article>
                        </div>
                        <!--/ body -->
                        <!-- footer -->
                        <div class="card-footer d-flex justify-content-between bgflgray">
                            <span>Reckon Accounts Business Setup </span>
                            <h6>$70</h6>
                        </div>
                        <!--/ footer -->
                    </div>
                    <!--/ card -->

                      <!-- card -->
                      <div class="card mb-2">
                        <!-- body -->
                        <div class="card-body d-flex">
                            <img src="img/data/tasker01.jpg" class="md-thumb">
                            <article class="pl-4">
                                <ul class="rating-tasker mb-0">
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                    <li><span class="icon-star icomoon"></span></li>
                                </ul>
                                <p>"Very good job, on time and task competed perfectly "</p>
                            </article>
                        </div>
                        <!--/ body -->
                        <!-- footer -->
                        <div class="card-footer d-flex justify-content-between bgflgray">
                            <span>Upload my receipt amounts into XERO</span>
                            <h6>$150</h6>
                        </div>
                        <!--/ footer -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ col -->
                  <!-- col -->
                  <div class="col-lg-3">
                      <img src="img/ad01.jpg" class="img-fluid" alt="" title="">
                  </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
   </div>
   <!--/ recent reviews -->

   <!-- what is laratasker-->
   <div class="title-row">
       <!-- container -->
       <div class="container">
            <!-- row -->
            <div class="row">
                 <!-- col -->
                <div class="col-lg-12 text-center">
                    <h2 class="h3">What is Airtasker?</h2>
                </div>
                <!--/ col -->
             </div>
            <!--/ row -->

             <!-- row -->
             <div class="row py-3">
                <!-- col -->
                <div class="col-lg-4 text-center">
                    <img src="img/modal_what-is-airtasker-one.png" class="img-fluid">
                    <h6 class="h6">Post your task</h6>
                    <p class="text-center">Tell us what you need, it's FREE to post.</p>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-4 text-center">
                    <img src="img/modal_what-is-airtasker-two.png" class="img-fluid">
                    <h6 class="h6">Review offers</h6>
                    <p class="text-center">Get offers from trusted Taskers and view profiles.</p>
                </div>
                <!--/ col -->

                  <!-- col -->
                  <div class="col-lg-4 text-center">
                    <img src="img/modal_what-is-airtasker-three.png" class="img-fluid">
                    <h6 class="h6">Get it done</h6>
                    <p class="text-center">Choose the right person for your task and get it done.</p>
                </div>
                <!--/ col -->
             </div>
            <!--/ row -->

            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-4">
                    <div class="bgflgray text-center py-5">
                        <h1 class="h1">1796</h1>
                        <p class="text-center pb-0 fgray">Tasks successfully complete</p>
                    </div>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <div class="bgflgray text-center py-5">
                        <h1 class="h1">3</h1>
                        <p class="text-center pb-0 fgray">Avg. quotes per task</p>
                    </div>
                </div>
                <!--/ col -->

                 <!-- col -->
                 <div class="col-lg-4">
                    <div class="bgflgray text-center py-5">
                        <h1 class="h1">5hrs</h1>
                        <p class="text-center pb-0 fgray">Avg. time to receive offer</p>
                    </div>
                </div>
                <!--/ col -->

            </div>
            <!-- /row -->

            <!-- row -->
            <div class="row title-row">
                <!-- left col -->
                <div class="col-lg-6">
                    <h3 class="h5">Why book an accountant through Laratasker?</h3>
                    <p>Nothing is certain but death and taxes, and thankfully you’ll only have to concern yourself with one of those two certainties for most of your life. But why concern yourself with taxes at all?</p>

                    <p>Accounting is one of those rare services that can actually save you cash if you choose to utilise it. You spend money to make money. A good accountant can highlight mistakes and inefficiencies that might be costing you money, and offer ways in which you could save more money. Not only that, you’ll also save yourself an inordinate amount of time when the tax year ends, claiming the maximum amount possible to either pump up your tax refund or minimise your tax debt.</p>

                    <p>All you need to do find a quality accountant. And luckily they may only be a couple of clicks away! With a wealth of expert accountants now at your fingertips, hiring the perfect one has never been easier. Simply describe the taxes services that you might require, suggest a fair rate, and wait for the offers to roll in.</p>

                    <p>There’s no better time to sort your finances out than now. Get a free, no obligation quote, and find your perfect accountant today.</p>
                </div>
                <!--/ left col -->

                <!-- right col -->
                <div class="col-lg-6">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-6 text-center">
                            <img width="150" src="img/offers-in-no-time.png" class="img-fluid">
                            <h6 class="h6">Offers in no time</h6>
                            <p class="text-center">Start getting offers to do your task ASAP!</p>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-6 text-center">
                            <img width="150" src="img/flexible-pricing.png" class="img-fluid">
                            <h6 class="h6">Flexible pricing</h6>
                            <p class="text-center">Choose the offer that’s right for you.</p>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-6 text-center">
                            <img width="150" src="img/real-reviews.png" class="img-fluid">
                            <h6 class="h6">Real reviews</h6>
                            <p class="text-center">Judge for yourself – every task gets a review.</p>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-6 text-center">
                            <img width="150" src="img/local-skilled-pros.png" class="img-fluid">
                            <h6 class="h6">Local, skilled pros</h6>
                            <p class="text-center">Take your pick of skilled Accountants nearby.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ right col -->
            </div>
            <!--/ row -->

       </div>
       <!--/ container -->

       <!-- faq-->
       <div class="title-row bgflgray">
           <!-- container -->
           <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-12 text-center">
                        <h2 class="h3">Top Accounting related questions</h2>
                    </div>
                    <!--/ col -->

                    <!-- col 8-->
                    <div class="col-lg-8">
                        <!-- accordion -->
                        <div class="cust-accord">
                            <div class="accordion">
                                <h3 class="panel-title h6">What does an accountant do?</h3>
                                <div class="panel-content">
                                    <p>Your accountant might specialise in one area, or they might offer a range of services, like personal tax, financial planning, business structures, BAS accounting, bookkeeping, budgeting, and reporting. Accounts usually offer a mixture of advice and implementation to help you get on top of the numbers in your business or personal finances.</p>
                                </div>
                                <h3 class="panel-title h6">How do I find a good accountant or tax accountant?</h3>
                                <div class="panel-content">
                                    <p>The best way to find a good accountant near you is to find candidates whose experience and skills match what you need. Then check their reviews to make sure they have a history of delivering on time and providing a great service.</p>
                                </div>

                                <h3 class="panel-title h6">How can I get a tax return near me?</h3>
                                <div class="panel-content">
                                    <p>Finding a local tax accountant can sometimes be tricky, depending on where you live. Our platform lists accountants from all over Australia, so if you post a task, it’s likely a local accountant will reach out. But it could also be worth considering a virtual or remote accountant so you’re not limited to your local area.</p>
                                </div>

                                <h3 class="panel-title h6">What do I need to bring to my tax appointment?</h3>
                                <div class="panel-content">
                                    <p>If it has anything to do with your income or expenses, it’s a good idea to bring it to your appointment. This includes ID, last year’s notice of assessment, bank statements, property/investment info, your expense receipts, and payment summaries.</p>
                                </div>

                                <h3 class="panel-title h6">How much does it cost to get an accountant to do your tax return or small business accounting?</h3>
                                <div class="panel-content">
                                    <p>For a simple tax return or straightforward BAS submission, it may cost as little as $50. For more time consuming or specialised services such as compliance, auditing, or setup, you can expect to pay closer to $500+.</p>
                                </div>

                                <h3 class="panel-title h6">How do I find a self-employed accountant? </h3>
                                <div class="panel-content">
                                    <p>Finding a self-employed, independent accountant often means you’ll get better value for money because they have fewer overheads and can charge less. The best place to find your accountant is here on Airtasker, with access to hundreds of accountants near you.</p>
                                </div>

                                <h3 class="panel-title h6">Do I need a specialist licence? </h3>
                                <div class="panel-content">
                                    <p>For certain types of tasks, you may need someone who has a specialist licence and also additional insurance. Please make sure that you confirm that a Tasker has the relevant licence and insurance before accepting an offer.</p>
                                </div>

                            </div>
                        </div>
                        <!--/ accordion -->
                    </div>
                    <!--/ col 8 -->
                </div>
                <!--/ row -->
           </div>
           <!--/ container -->
       </div>
       <!--/ faqs -->

       <!-- recent tasks -->
       <div class="recent-tasks title-row">
           <!-- container -->
           <div class="container">

              <div class="row title-row">
                    <!-- col -->
                    <div class="col-lg-12 text-center">
                        <h2 class="h3">Recent Accounting tasks</h2>
                    </div>
                    <!--/ col -->
              </div>

               <!-- row -->
               <div class="row">
                   <!-- col -->
                   <div class="col-lg-6">

                       <!-- card -->
                       <div class="card mb-2">
                           <!-- header -->
                           <div class="card-header bgflgray d-flex justify-content-between">
                                <div>
                                    <h6 class="h6">Temporary Bookkeeper</h6>
                                    <ul class="nav">
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-pin"></span> Prahran, VIC
                                        </li>
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-calendar"></span> 10th Feb 2020
                                        </li>
                                    </ul>
                                </div>
                                <h6 class="h6 align-self-center">$420</h6>
                           </div>
                           <!--/ header -->
                           <!-- body -->
                           <div class="card-body">
                               <p>We are between staff and need a temp bookkeeper to run small IT company books We use online cloud accounting You would need bookkeeping skills ie reconciliation of accounts , collecting $$ and paying bills, IT knowledge, happy positive attitude, auditing and attention to detail.... work closely with great positive team this on site job based in PRAHRAN in Melbourne Flexible hours and will allow for extra hours to get on top of the work</p>
                           </div>
                           <!--/ body -->
                       </div>
                       <!--/ card -->

                       <!-- card -->
                       <div class="card mb-2">
                           <!-- header -->
                           <div class="card-header bgflgray d-flex justify-content-between">
                                <div>
                                    <h6 class="h6">MYOB Customise Forms </h6>
                                    <ul class="nav">
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-pin"></span> Gladesville NSW, Australia
                                        </li>
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-calendar"></span> 10th Feb 2020
                                        </li>
                                    </ul>
                                </div>
                                <h6 class="h6 align-self-center">$160</h6>
                           </div>
                           <!--/ header -->
                           <!-- body -->
                           <div class="card-body">
                               <p>I need someone to come to my office at Gladesville to spend a few hours to customise my invoices, statements, Quote and purchase forms andhave a full understanding of MYOB overall.</p>
                           </div>
                           <!--/ body -->
                       </div>
                       <!--/ card -->

                        <!-- card -->
                        <div class="card mb-2">
                           <!-- header -->
                           <div class="card-header bgflgray d-flex justify-content-between">
                                <div>
                                    <h6 class="h6">I need bookkeeping  </h6>
                                    <ul class="nav">
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-pin"></span> Gembrook VIC 3783, Australi
                                        </li>
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-calendar"></span> 10th Feb 2020
                                        </li>
                                    </ul>
                                </div>
                                <h6 class="h6 align-self-center">$35</h6>
                           </div>
                           <!--/ header -->
                           <!-- body -->
                           <div class="card-body">
                               <p>complete MYOB accounts for Bas statements and entry and statements for end of year tax,business and sms fund</p>
                           </div>
                           <!--/ body -->
                       </div>
                       <!--/ card -->

                        <!-- card -->
                        <div class="card mb-2">
                           <!-- header -->
                           <div class="card-header bgflgray d-flex justify-content-between">
                                <div>
                                    <h6 class="h6">Accounting consultation+Ongoing relationship after </h6>
                                    <ul class="nav">
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-pin"></span> Melbourne VIC, Australia </li>
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-calendar"></span> 10th Feb 2020
                                        </li>
                                    </ul>
                                </div>
                                <h6 class="h6 align-self-center">$50</h6>
                           </div>
                           <!--/ header -->
                           <!-- body -->
                           <div class="card-body">
                               <p>I have recently open a small delivery business and I need to get the books in order. I have kept all receipts but haven't fill up any log book yet. I have an ABN and GST registered. I earn incomes from 2 sources and I've literally not many invoices. Need someone to teach me the basic, help with claiming my business expenses and understanding law requirements. I DON'T NEED TO LODGE ANYTHING YET. JUST 30-60min FACE TO FACE CONSULTATION, however I'll need to upload my first BAS statement (oct-dec) by 28 Feb (obviously I'll pay extra for it, need it in a couple of weeks). I'm looking for someone to build a long-term relationship with and work together. Someone that can help me to understand how tax works and what need to be done. Not looking for a huge accounting firm or for ridiculous offer. I'm just looking for a Registered Tax Agent-CPA qualified Accountant with a some experience.. I can come to your place or meet you at my place Meeting before before 9th Feb Best Regards Frank</p>
                           </div>
                           <!--/ body -->
                       </div>
                       <!--/ card -->

                       
                        <!-- card -->
                        <div class="card mb-2">
                           <!-- header -->
                           <div class="card-header bgflgray d-flex justify-content-between">
                                <div>
                                    <h6 class="h6">I need basic Xero Training  </h6>
                                    <ul class="nav">
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-pin"></span> Kingston QLD 4114, Australia </li>
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-calendar"></span> 10th Feb 2020
                                        </li>
                                    </ul>
                                </div>
                                <h6 class="h6 align-self-center">$50</h6>
                           </div>
                           <!--/ header -->
                           <!-- body -->
                           <div class="card-body">
                               <p>I have a small online business, and would like face to face Xero training. I have set up bank feeds, but need assistance importing statements into Xero, and ensuring they end up under the correct section in Chart of Accounts. I am not interested in payroll or invoicing. I would think I would require no more than 2hrs of your time, as I have save the bank statement in CSV format to my desktop.</p>
                           </div>
                           <!--/ body -->
                       </div>
                       <!--/ card -->

                         <!-- card -->
                         <div class="card mb-2">
                           <!-- header -->
                           <div class="card-header bgflgray d-flex justify-content-between">
                                <div>
                                    <h6 class="h6">Help with Xero  </h6>
                                    <ul class="nav">
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-pin"></span> Clovelly NSW 2031, Australia </li>
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-calendar"></span> 10th Feb 2020
                                        </li>
                                    </ul>
                                </div>
                                <h6 class="h6 align-self-center">$50</h6>
                           </div>
                           <!--/ header -->
                           <!-- body -->
                           <div class="card-body">
                               <p>Hi there We need help with some book keeping decisions for our new business. We use Xero. 2 key questions 1) treatment of GST - we are not currently registered and want to make sure we are doing things right 2) treatment of customer deposits - we then use the deposit to purchase furniture on clients behalf. We would prefer not to book as revenue but happy to understand the best way. Plus we’d love to know how to use it better around handling payments. And any other tips. We have set up the bank reconciliation with St George Bank so have the feed coming through. Thanks Blake and Leah</p>
                           </div>
                           <!--/ body -->
                       </div>
                       <!--/ card -->

                       
                         <!-- card -->
                         <div class="card mb-2">
                           <!-- header -->
                           <div class="card-header bgflgray d-flex justify-content-between">
                                <div>
                                    <h6 class="h6">Accountant to write letter for loan  </h6>
                                    <ul class="nav">
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-pin"></span> Parkwood QLD 4214, Austra </li>
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-calendar"></span> 10th Feb 2020
                                        </li>
                                    </ul>
                                </div>
                                <h6 class="h6 align-self-center">$80</h6>
                           </div>
                           <!--/ header -->
                           <!-- body -->
                           <div class="card-body">
                               <p>Hi I need a accountant to write letter for a loan here is a example To Whom It May Concern: Re: John Smith & Smith Family Printing Pty Ltd We confirm the following details regarding John Smith and his company Smith Family Printing Pty Ltd: We act as the accountant for both John & Smith Family Printing Pty Ltd. We confirm that to the best of our knowledge Smith Family Printing Pty Ltd is trading at a profit and can pay all liabilities that it has. This information is true to the best of our knowledge and has been confirmed from independent enquiries with our customer and our own records. Should you require any additional information please do not hesitate to contact our office on [Content Moderated] Regards,</p>
                           </div>
                           <!--/ body -->
                       </div>
                       <!--/ card -->

                   </div>
                   <!--/ col -->

                   <!-- col -->
                   <div class="col-lg-6">

                     <!-- card -->
                     <div class="card mb-2">
                           <!-- header -->
                           <div class="card-header bgflgray d-flex justify-content-between">
                                <div>
                                    <h6 class="h6">I need XERO training  </h6>
                                    <ul class="nav">
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-pin"></span> Paddington NSW, Australia </li>
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-calendar"></span> 10th Feb 2020
                                        </li>
                                    </ul>
                                </div>
                                <h6 class="h6 align-self-center">$80</h6>
                           </div>
                           <!--/ header -->
                           <!-- body -->
                           <div class="card-body">
                               <p>Need help with Xero. Already proficient with Quicken software and have basic understanding of Xero but would like to get a deeper understanding and learn a few tips and tricks.</p>
                           </div>
                           <!--/ body -->
                       </div>
                       <!--/ card -->

                        <!-- card -->
                     <div class="card mb-2">
                           <!-- header -->
                           <div class="card-header bgflgray d-flex justify-content-between">
                                <div>
                                    <h6 class="h6">Help set up XERO payroll  </h6>
                                    <ul class="nav">
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-pin"></span> Pelican Waters QLD, Australia

</li>
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-calendar"></span> 10th Feb 2020
                                        </li>
                                    </ul>
                                </div>
                                <h6 class="h6 align-self-center">$55</h6>
                           </div>
                           <!--/ header -->
                           <!-- body -->
                           <div class="card-body">
                               <p>Help set up XERO payroll and a quick run through at my office pelican waters</p>
                           </div>
                           <!--/ body -->
                       </div>
                       <!--/ card -->

                        <!-- card -->
                        <div class="card mb-2">
                           <!-- header -->
                           <div class="card-header bgflgray d-flex justify-content-between">
                                <div>
                                    <h6 class="h6">Tax return for the last 5 years  </h6>
                                    <ul class="nav">
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-pin"></span> Redland Bay QLD, Australia</li>
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-calendar"></span> 4th Feb 2020
                                        </li>
                                    </ul>
                                </div>
                                <h6 class="h6 align-self-center">$5</h6>
                           </div>
                           <!--/ header -->
                           <!-- body -->
                           <div class="card-body">
                               <p>I need a good tax accountant to do my tax returns for the last 5 years, I have only worked FIFO and for employers, no contract work, no bass statements, I have most receipts, and bank records for work related expenses.</p>
                           </div>
                           <!--/ body -->
                       </div>
                       <!--/ card -->

                        <!-- card -->
                        <div class="card mb-2">
                           <!-- header -->
                           <div class="card-header bgflgray d-flex justify-content-between">
                                <div>
                                    <h6 class="h6">MYOB short course  </h6>
                                    <ul class="nav">
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-pin"></span> Elwood VIC, Australia </li>
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-calendar"></span> 4th Feb 2020
                                        </li>
                                    </ul>
                                </div>
                                <h6 class="h6 align-self-center">$5</h6>
                           </div>
                           <!--/ header -->
                           <!-- body -->
                           <div class="card-body">
                               <p>I’m looking to purchase and setup MYOB for a small business- If there’s someone out there that knows exactly How to use the product and is willing to sit down over 1 or 2 days and explain- set me up I’d be willing to pay for this privilege. Experience essential, preferably trades services</p>
                           </div>
                           <!--/ body -->
                       </div>
                       <!--/ card -->

                       <!-- card -->
                       <div class="card mb-2">
                           <!-- header -->
                           <div class="card-header bgflgray d-flex justify-content-between">
                                <div>
                                    <h6 class="h6">Chat about Bookkeeping  </h6>
                                    <ul class="nav">
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-pin"></span> Chermside QLD, Australia </li>
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-calendar"></span> 4th Feb 2020
                                        </li>
                                    </ul>
                                </div>
                                <h6 class="h6 align-self-center">$5</h6>
                           </div>
                           <!--/ header -->
                           <!-- body -->
                           <div class="card-body">
                               <p>To talk about Bookkeeping Payroll and BAS. Ongoing role.</p>
                           </div>
                           <!--/ body -->
                       </div>
                       <!--/ card -->

                        <!-- card -->
                        <div class="card mb-2">
                           <!-- header -->
                           <div class="card-header bgflgray d-flex justify-content-between">
                                <div>
                                    <h6 class="h6">I need XERO training  </h6>
                                    <ul class="nav">
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-pin"></span> Ringwood East VIC, Australia </li>
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-calendar"></span> 14th Jan 2020
                                        </li>
                                    </ul>
                                </div>
                                <h6 class="h6 align-self-center">$5</h6>
                           </div>
                           <!--/ header -->
                           <!-- body -->
                           <div class="card-body">
                               <p>I need to know how to use Xero for doing bookkeeping for my cafe business. currently using it for invoicing, payroll. Need training on how to organize receipts, and reconcile items. Further down More advanced training on bas statement and financial yearly report. Would like to have 1 on 1 training. let me know your rates. I live in Ringwood, but after mid Feb I will be living in city. Ideally I would like an intensive 1 day training with you for how to use Xero to cover all basics.</p>
                           </div>
                           <!--/ body -->
                       </div>
                       <!--/ card -->

                         <!-- card -->
                         <div class="card mb-2">
                           <!-- header -->
                           <div class="card-header bgflgray d-flex justify-content-between">
                                <div>
                                    <h6 class="h6">Tax lodgement  </h6>
                                    <ul class="nav">
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-pin"></span>Clarkson WA 6030, Australia </li>
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-calendar"></span> 14th Jan 2020
                                        </li>
                                    </ul>
                                </div>
                                <h6 class="h6 align-self-center">$5</h6>
                           </div>
                           <!--/ header -->
                           <!-- body -->
                           <div class="card-body">
                               <p>Very simple tax lodgement, normally takes 20/30 mins with tax agent I can come to you or meet at a cafe</p>
                           </div>
                           <!--/ body -->
                       </div>
                       <!--/ card -->

                        <!-- card -->
                        <div class="card mb-2">
                           <!-- header -->
                           <div class="card-header bgflgray d-flex justify-content-between">
                                <div>
                                    <h6 class="h6">Bookkeeping admin social media  </h6>
                                    <ul class="nav">
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-pin"></span>Padstow Heights NSW, Austra </li>
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-calendar"></span> 14th Jan 2020
                                        </li>
                                    </ul>
                                </div>
                                <h6 class="h6 align-self-center">$20</h6>
                           </div>
                           <!--/ header -->
                           <!-- body -->
                           <div class="card-body">
                               <p>VHi! Locals only apply I need basic bookkeeping, lodging form applications, social media management and posting, responding to my emails, speak fluent english, help with errands, live locally to me., basic bookkeeping. My office is from home.</p>
                           </div>
                           <!--/ body -->
                       </div>
                       <!--/ card -->

                        <!-- card -->
                        <div class="card mb-2">
                           <!-- header -->
                           <div class="card-header bgflgray d-flex justify-content-between">
                                <div>
                                    <h6 class="h6">MYOB Training  </h6>
                                    <ul class="nav">
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-pin"></span>Prahran VIC, Australia </li>
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-calendar"></span> 14th Jan 2020
                                        </li>
                                    </ul>
                                </div>
                                <h6 class="h6 align-self-center">$20</h6>
                           </div>
                           <!--/ header -->
                           <!-- body -->
                           <div class="card-body">
                               <p>Payroll & Data Entry 1-3 Hours Training (not sure of how long we need)</p>
                           </div>
                           <!--/ body -->
                       </div>
                       <!--/ card -->

                        <!-- card -->
                        <div class="card mb-2">
                           <!-- header -->
                           <div class="card-header bgflgray d-flex justify-content-between">
                                <div>
                                    <h6 class="h6">Personal Tax Returns  </h6>
                                    <ul class="nav">
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-pin"></span>Wheelers Hill VIC, Australia </li>
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-calendar"></span> 14th Jan 2020
                                        </li>
                                    </ul>
                                </div>
                                <h6 class="h6 align-self-center">$150</h6>
                           </div>
                           <!--/ header -->
                           <!-- body -->
                           <div class="card-body">
                               <p>I need my last 4 personal tax returns done. Must be qualified and licensed. Please offer me a package one price deal. Please also let me know why I should hire you and how you can maximise my refunds from ATO. Cheers</p>
                           </div>
                           <!--/ body -->
                       </div>
                       <!--/ card -->

                        <!-- card -->
                        <div class="card mb-2">
                           <!-- header -->
                           <div class="card-header bgflgray d-flex justify-content-between">
                                <div>
                                    <h6 class="h6">Tax return 18/19 for sole trader   </h6>
                                    <ul class="nav">
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-pin"></span>Hamersley WA 6022, Australia </li>
                                        <li class="nav-item small px-2 fgray">
                                            <span class="icon-calendar"></span> 15th Jan 2020
                                        </li>
                                    </ul>
                                </div>
                                <h6 class="h6 align-self-center">$100</h6>
                           </div>
                           <!--/ header -->
                           <!-- body -->
                           <div class="card-body">
                               <p>Looking for someone to assist in submitting my tax return for year 2018/2019. Receipts and bank statements available to maximize my returns.</p>
                           </div>
                           <!--/ body -->
                       </div>
                       <!--/ card -->


                   </div>
                   <!--/ col -->
               </div>
               <!--/ row -->
           </div>
           <!--/ container -->
       </div>
       <!--/ recent rasks -->


   </div>
   <!--/ what is larataskder-->


     
    
      

    
  </main>
  <!--/ main -->
  <?php include 'footer.php' ?>
  <?php include 'scripts.php' ?> 

</body>
</html>