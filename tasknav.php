<!-- tasker nav-->
    <div class="task-nav">
        <!-- container -->
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light">               
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">                   
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        50 km Nehru Nagar & remotely
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <span class="dropdown-menu-arrow"></span>
                            <!-- task custom -->
                            <div class="task-custom p-2">
                                <p>
                                    <span class="small-title">To be Done</span>
                                </p>
                                <div>
                                    <button class="pinkbtn btn">In Person</button>
                                    <button class="pinkbtn btn">Remotely</button>
                                    <button class="pinkbtn btn">All</button>
                                </div>
                                <div class="form-group pt-4">
                                    <p>
                                        <span class="small-title">Suburb</span>
                                    </p>
                                    <input class="form-control my-2" type="text" placeholder="Nehru Nagar, Telangana">
                                    <p>
                                        <span class="small-title">Distance</span>
                                    </p>
                                    <div class="slidecontainer">
                                        <p class="text-right small">Distance: <span id="demo"></span> km</p>
                                        <input type="range" min="1" max="100" value="50" class="slider" id="myRange">                                        
                                    </div>
                                    <div class="button-row d-flex mt-4">
                                       <a href="javascript:void(0)" class="fblack mt-4">Cancel</a>
                                        <button class="pinkbtn w-25 ml-auto" type="button" title="Next">Apply</button>
                                    </div>
                                </div>
                            </div>
                            <!-- task custom -->
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Any Prices
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <span class="dropdown-menu-arrow"></span>
                           <!-- task custom -->
                           <div class="task-custom p-2">
                                <p>
                                    <span class="small-title">TASK PRICE</span>
                                </p>
                               
                                <div class="form-group pt-4">                                   
                                    <div class="slidecontainer">
                                        <p class="text-right small">$<span id="demo1"></span> </p>
                                        <input type="range" min="20" max="1000" value="60" class="slider" id="myRange1">                                        
                                    </div>
                                    <div class="button-row d-flex mt-4">
                                       <a href="javascript:void(0)" class="fblack mt-4">Cancel</a>
                                        <button class="pinkbtn w-25 ml-auto" type="button" title="Next">Apply</button>
                                    </div>
                                </div>
                            </div>
                            <!-- task custom -->
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           Task Type
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <span class="dropdown-menu-arrow"></span>
                            <!-- task custom -->
                            <div class="task-custom p-2">
                                <p>
                                    <span class="small-title">AVAILABLE TASKS ONLY</span>
                                </p>
                               
                                <div class="form-group pt-4">                                   
                                    <div class="slidecontainer">
                                        <label>
                                            <input type="checkbox">       
                                            Hide tasks that are already assigned 
                                        </label>                                     
                                                                        
                                    </div>
                                    <div class="button-row d-flex mt-4">
                                        <a href="javascript:void(0)" class="fblack mt-4">Cancel</a>
                                        <button class="pinkbtn w-25 ml-auto" type="button" title="Next">Apply</button>
                                    </div>
                                </div>
                            </div>
                            <!-- task custom -->
                        </div>
                    </li>
                   
                    </ul>
                    <form class="form-inline my-2 my-lg-0 search-tasker">
                        <input class="form-control mr-sm-2 round-input" type="search" placeholder="Search a Task" aria-label="Search">
                        <button class="btn my-2 my-sm-0" type="submit"><span class="icon-search icomoon"></span></button>
                    </form>
                </div>
            </nav>
        </div>
        <!--/ container -->
    </div>
    <!--/ tasker nav -->

    
